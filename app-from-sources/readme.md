This code isn't associated with Codebrew Software in any fashion. All rights to the original Gravity Defied, it's name, logotype, brand and all that stuff belongs to Codebrew Software.

## Gavity defied is awesome

Original jar consist of 63 616 bytes. All graphics and code.

It's a masterpiece. It is working on old primitive smartphones even without support of float numbers!
To better understanding of ways how app was done one can try restore source code.

## Decompiling
Fernflower is a [part](https://github.com/JetBrains/intellij-community/tree/master/plugins/java-decompiler/engine) of IntelliJ IDEA.
Oiginal repo is huge: more than 1Gb. You can use [mirror](https://github.com/fesh0r/fernflower) instead, it contains only decompiler.

* clone repo
* ```./gradlew jar``` to make fernflower.jar
* ```mkdir decompiled && java -jar fernflower.jar -ren=1 GravityDefied_java decompiled```

-ren=1 renames fields, because some of them has crazy names like 'if' and 'for'.

## Let's compile it back!

I use gradle. Original MIDP2 classes aren't exist in PC java. I wrote proxy classes emulating them.
Some methods and classes was renamed to achieve better readability.
