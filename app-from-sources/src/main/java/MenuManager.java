import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreNotOpenException;

// $FF: renamed from: m
public class MenuManager implements IMenuManager, Runnable {
    // $FF: renamed from: i byte[]
    private byte[] field_278;
    // $FF: renamed from: R Micro
    private Micro micro;
    // $FF: renamed from: z a
    private RecordManager recordManager;
    // $FF: renamed from: au e
    public GameMenu currentGameMenu;
    // $FF: renamed from: m javax.microedition.lcdui.Command
    private Command commandOk;
    // $FF: renamed from: C javax.microedition.lcdui.Command
    private Command commandBack;
    // $FF: renamed from: F e
    private GameMenu gameMenuMain;
    // $FF: renamed from: p e
    private GameMenu gameMenuPlay;
    // $FF: renamed from: q e
    private GameMenu gameMenuOptions;
    // $FF: renamed from: r e
    private GameMenu gameMenuAbout;
    // $FF: renamed from: s e
    private GameMenu gameMenuHelp;
    // $FF: renamed from: t e
    private GameMenu gameMenuConfirmClear;
    // $FF: renamed from: u e
    private GameMenu gameMenuConfirmReset;
    // $FF: renamed from: v e
    private GameMenu gameMenuFinished;
    // $FF: renamed from: w e
    private GameMenu gameMenuIngame;
    // $FF: renamed from: x n
    private TimerOrMotoPartOrMenuElem taskPlayMenu;
    // $FF: renamed from: J n
    private TimerOrMotoPartOrMenuElem taskOptions;
    // $FF: renamed from: aO n
    private TimerOrMotoPartOrMenuElem taskHelp;
    // $FF: renamed from: Y g
    private SettingsStringRender settingStringLevel;
    // $FF: renamed from: V e
    private GameMenu gameMenuStringLevel;
    // $FF: renamed from: aM g
    private SettingsStringRender settingsStringTrack;
    // $FF: renamed from: y e
    private GameMenu field_299;
    // $FF: renamed from: aZ g
    private SettingsStringRender settingsStringLeague;
    // $FF: renamed from: A e
    private GameMenu gameMenuLeague;
    // $FF: renamed from: al e
    private GameMenu gameMenuHighscore;
    // $FF: renamed from: W n
    private TimerOrMotoPartOrMenuElem gameTimerTaskHighscore;
    // $FF: renamed from: aB g
    private SettingsStringRender taskStart;
    // $FF: renamed from: aL g
    private SettingsStringRender settingStringSoftwareJoystick;
    // $FF: renamed from: bc g
    private SettingsStringRender perspectiveSetting;
    // $FF: renamed from: a7 g
    private SettingsStringRender shadowsSetting;
    // $FF: renamed from: K g
    private SettingsStringRender driverSpriteSetting;
    // $FF: renamed from: aD g
    private SettingsStringRender bikeSpriteSetting;
    // $FF: renamed from: H g
    private SettingsStringRender inputSetting;
    // $FF: renamed from: T g
    private SettingsStringRender lookAheadSetting;
    // $FF: renamed from: aI n
    private TimerOrMotoPartOrMenuElem clearHighscoreSetting;
    // $FF: renamed from: bf n
    private TimerOrMotoPartOrMenuElem field_313;
    // $FF: renamed from: I g
    private SettingsStringRender field_314;
    // $FF: renamed from: ay g
    private SettingsStringRender field_315;
    // $FF: renamed from: ba n
    private TimerOrMotoPartOrMenuElem taskAbout;
    // $FF: renamed from: O e
    private GameMenu field_317;
    // $FF: renamed from: a6 n
    private TimerOrMotoPartOrMenuElem field_318;
    // $FF: renamed from: aX e
    private GameMenu field_319;
    // $FF: renamed from: U n
    private TimerOrMotoPartOrMenuElem field_320;
    // $FF: renamed from: bd e
    private GameMenu field_321;
    // $FF: renamed from: av n
    private TimerOrMotoPartOrMenuElem field_322;
    // $FF: renamed from: aC e
    private GameMenu gameMenuOptionsHighscoreDescription;
    // $FF: renamed from: aw n
    private TimerOrMotoPartOrMenuElem taskHighscore;
    // $FF: renamed from: a1 e
    private GameMenu gameMenuOptions2;
    // $FF: renamed from: aP n
    private TimerOrMotoPartOrMenuElem field_326;
    // $FF: renamed from: bh e
    private GameMenu gameMenuEnterName;
    // $FF: renamed from: aJ g
    private SettingsStringRender settingStringBack;
    // $FF: renamed from: as g
    private SettingsStringRender settingStringPlayMenu;
    // $FF: renamed from: am g
    private SettingsStringRender settingStringContinue;
    // $FF: renamed from: Q g
    private SettingsStringRender settingStringGoToMain;
    // $FF: renamed from: N g
    private SettingsStringRender settingStringExitGame;
    // $FF: renamed from: bg g
    private SettingsStringRender field_333;
    // $FF: renamed from: aU g
    private SettingsStringRender field_334;
    // $FF: renamed from: ad g
    private SettingsStringRender field_335;
    // $FF: renamed from: aa g
    private SettingsStringRender field_336;
    // $FF: renamed from: a9 long
    private long field_337;
    // $FF: renamed from: aj int
    private int field_338;
    // $FF: renamed from: at int
    private int field_339;
    // $FF: renamed from: be java.lang.String
    private String field_340;
    // $FF: renamed from: X byte[]
    private byte[] field_341;
    // $FF: renamed from: Z byte[]
    byte[] field_342 = new byte[3];
    // $FF: renamed from: b byte
    byte availableLeagues = 0;
    // $FF: renamed from: c byte
    byte field_344 = 0;
    // $FF: renamed from: d int[]
    int[] field_345 = new int[]{0, 0, 0};
    // $FF: renamed from: D java.lang.String[][]
    String[][] levelNames;
    // $FF: renamed from: f java.lang.String[]
    String[] leagueNames = new String[3];
    // $FF: renamed from: g java.lang.String[]
    String[] leagueNamesAll4;
    // $FF: renamed from: a5 javax.microedition.rms.RecordStore
    private RecordStore recordStore;
    // $FF: renamed from: af int
    private int recorcStoreRecordId = -1;
    // $FF: renamed from: ap boolean
    private boolean isRecordStoreOpened;
    // $FF: renamed from: a4 javax.microedition.lcdui.Image
    private Image rasterImage;
    // $FF: renamed from: h h
    TextRender textRenderCodeBrewLink;
    // $FF: renamed from: M int
    private int field_354 = 0;
    // $FF: renamed from: E int
    private int field_355 = 0;
    // $FF: renamed from: L boolean
    private boolean field_356 = false;
    // $FF: renamed from: S boolean
    private boolean field_357 = false;
    // $FF: renamed from: aA boolean
    private boolean isHasPointerEvents = false;
    // $FF: renamed from: B java.lang.Object
    private Object field_359;
    // $FF: renamed from: a3 int
    public int field_360 = 0;
    // $FF: renamed from: az java.lang.String[]
    private String[] field_361 = new String[]{"Easy", "Medium", "Pro"};
    // $FF: renamed from: aW long
    private long field_362 = 0L;
    // $FF: renamed from: aY byte
    private byte isDisablePerspective = 0;
    // $FF: renamed from: aE byte
    private byte isDisabledShadows = 0;
    // $FF: renamed from: ab byte
    private byte isDisabledDriverSprite = 0;
    // $FF: renamed from: aR byte
    private byte isDisabledBikeSprite = 0;
    // $FF: renamed from: bi byte
    private byte field_367 = 0;
    // $FF: renamed from: ae byte
    private byte isDisableLookAhead = 0;
    // $FF: renamed from: G byte
    private byte field_369 = 0;
    // $FF: renamed from: ac byte
    private byte field_370 = 0;
    // $FF: renamed from: an byte
    private byte field_371 = 0;
    // $FF: renamed from: aT byte
    private byte field_372 = 0;
    // $FF: renamed from: ar byte
    private byte field_373 = 0;
    // $FF: renamed from: ag java.lang.String[]
    private String[] field_374 = null;
    // $FF: renamed from: aQ java.lang.String[]
    private String[] field_375 = null;
    // $FF: renamed from: P h
    private TextRender field_376 = null;
    // $FF: renamed from: bl boolean
    public boolean field_377 = false;
    // $FF: renamed from: a2 javax.microedition.lcdui.Alert
    private Alert alert = null;

    public MenuManager(Micro var1) {
        this.micro = var1;
        this.field_376 = new TextRender("", var1);
    }

    // $FF: renamed from: byte (int) void
    public void initPart(int var1) {
        int var4;
        switch (var1) {
            case 1:
                this.field_359 = new Object();
                this.isHasPointerEvents = this.micro.gameCanvas.hasPointerEvents();
                this.field_341 = new byte[]{65, 65, 65};
                this.field_374 = new String[]{"On", "Off"};
                this.field_375 = new String[]{"Keyset 1", "Keyset 2", "Keyset 3"};
                this.recordManager = new RecordManager();
                this.field_337 = -1L;
                this.field_338 = -1;
                this.field_339 = -1;
                this.field_340 = null;
                this.isRecordStoreOpened = false;
                this.field_278 = new byte[19];

                for (int var11 = 0; var11 < 19; ++var11) {
                    this.field_278[var11] = -127;
                }

                try {
                    this.recordStore = RecordStore.openRecordStore("GDTRStates", true);
                    this.isRecordStoreOpened = true;
                    return;
                } catch (RecordStoreException var9) {
                    this.isRecordStoreOpened = false;
                    return;
                }
            case 2:
                this.recorcStoreRecordId = -1;

                RecordEnumeration records;
                try {
                    records = this.recordStore.enumerateRecords((RecordFilter) null, (RecordComparator) null, false);
                } catch (RecordStoreNotOpenException var8) {
                    return;
                }

                byte[] var3;
                if (records.numRecords() > 0) {
                    try {
                        var3 = records.nextRecord();
                        records.reset();
                        this.recorcStoreRecordId = records.nextRecordId();
                    } catch (RecordStoreException var7) {
                        return;
                    }

                    if (var3.length <= 19) {
                        System.arraycopy(var3, 0, this.field_278, 0, var3.length);
                    }

                    records.destroy();
                }

                if ((var3 = this.method_216(16, (byte) -1)) != null && var3[0] != -1) {
                    for (var4 = 0; var4 < 3; ++var4) {
                        this.field_341[var4] = var3[var4];
                    }
                }

                if (this.field_341[0] == 82 && this.field_341[1] == 75 && this.field_341[2] == 69) {
                    this.availableLeagues = 3;
                    this.field_344 = 2;
                    this.field_342[0] = (byte) (this.micro.levelLoader.levelNames[0].length - 1);
                    this.field_342[1] = (byte) (this.micro.levelLoader.levelNames[1].length - 1);
                    this.field_342[2] = (byte) (this.micro.levelLoader.levelNames[2].length - 1);
                    return;
                }

                this.availableLeagues = 0;
                this.field_344 = 1;
                this.field_342[0] = 0;
                this.field_342[1] = 0;
                this.field_342[2] = -1;
                return;
            case 3:
                this.isDisablePerspective = this.method_217(0, this.isDisablePerspective);
                this.isDisabledShadows = this.method_217(1, this.isDisabledShadows);
                this.isDisabledDriverSprite = this.method_217(2, this.isDisabledDriverSprite);
                this.isDisabledBikeSprite = this.method_217(3, this.isDisabledBikeSprite);
                this.field_367 = this.method_217(14, this.field_367);
                this.isDisableLookAhead = this.method_217(4, this.isDisableLookAhead);
                this.field_369 = this.method_217(11, this.field_369);
                this.field_370 = this.method_217(10, this.field_370);
                this.field_371 = this.method_217(12, this.field_371);
                this.field_373 = this.method_217(15, this.field_373);
                this.field_354 = this.field_370;
                this.field_355 = this.field_369;
                if (this.isHasPointerEvents) {
                    this.field_372 = this.method_217(13, this.field_372);
                }

                if (this.field_341[0] != 82 || this.field_341[1] != 75 || this.field_341[2] != 69) {
                    this.availableLeagues = this.method_217(5, this.availableLeagues);
                    this.field_344 = this.method_217(6, this.field_344);

                    for (var4 = 0; var4 < 3; ++var4) {
                        this.field_342[var4] = this.method_217(7 + var4, this.field_342[var4]);
                    }
                }

                try {
                    this.field_345[this.field_370] = this.field_369;
                } catch (ArrayIndexOutOfBoundsException var6) {
                    this.field_370 = 0;
                    this.field_369 = 0;
                    this.field_345[this.field_370] = this.field_369;
                }

                LevelLoader var10000 = this.micro.levelLoader;
                LevelLoader.isEnabledPerspective = this.isDisablePerspective == 0;
                LevelLoader.isEnabledShadows = this.isDisabledShadows == 0;
                this.micro.gamePhysics.setEnableLookAhead(this.isDisableLookAhead == 0);
                this.micro.gameCanvas.method_163(this.field_367);
                this.micro.gameCanvas.method_124(this.field_372 == 0);
                String[] leagueNames = new String[]{"100cc", "175cc", "220cc"};
                this.leagueNamesAll4 = new String[]{"100cc", "175cc", "220cc", "325cc"};
                this.levelNames = this.micro.levelLoader.levelNames;
                if (this.availableLeagues < 3) {
                    this.leagueNames = leagueNames;
                } else {
                    this.leagueNames = this.leagueNamesAll4;
                }

                this.field_360 = this.field_371;
                return;
            case 4:
                this.gameMenuMain = new GameMenu("Main", this.micro, (GameMenu) null, (byte[]) null);
                this.gameMenuPlay = new GameMenu("Play", this.micro, this.gameMenuMain, (byte[]) null);
                this.gameMenuOptions = new GameMenu("Options", this.micro, this.gameMenuMain, (byte[]) null);
                this.gameMenuAbout = new GameMenu("About", this.micro, this.gameMenuMain, (byte[]) null);
                this.gameMenuHelp = new GameMenu("Help", this.micro, this.gameMenuMain, (byte[]) null);
                this.settingStringBack = new SettingsStringRender("Back", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.settingStringGoToMain = new SettingsStringRender("Go to Main", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.settingStringContinue = new SettingsStringRender("Continue", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.settingStringPlayMenu = new SettingsStringRender("Play Menu", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                if (this.gameMenuAbout.xPos + Font.getFont(32, 1, 8).stringWidth("http://www.codebrew.se/") >= this.getCanvasWidth()) {
                    this.textRenderCodeBrewLink = new TextRender("www.codebrew.se", this.micro);
                } else {
                    this.textRenderCodeBrewLink = new TextRender("http://www.codebrew.se/", this.micro);
                }

                this.textRenderCodeBrewLink.setFont(Font.getFont(32, 1, 8));
                this.gameMenuHighscore = new GameMenu("Highscore", this.micro, this.gameMenuPlay, (byte[]) null);
                this.gameMenuFinished = new GameMenu("Finished!", this.micro, this.gameMenuPlay, (byte[]) null);
                return;
            case 5:
                this.gameMenuIngame = new GameMenu("Ingame", this.micro, this.gameMenuPlay, (byte[]) null);
                this.gameMenuEnterName = new GameMenu("Enter Name", this.micro, this.gameMenuFinished, this.field_341);
                this.gameMenuConfirmClear = new GameMenu("Confirm Clear", this.micro, this.gameMenuOptions, (byte[]) null);
                this.gameMenuConfirmReset = new GameMenu("Confirm Reset", this.micro, this.gameMenuConfirmClear, (byte[]) null);
                this.taskPlayMenu = new TimerOrMotoPartOrMenuElem("Play Menu", this.gameMenuPlay, this);
                this.taskOptions = new TimerOrMotoPartOrMenuElem("Options", this.gameMenuOptions, this);
                this.taskHelp = new TimerOrMotoPartOrMenuElem("Help", this.gameMenuHelp, this);
                this.taskAbout = new TimerOrMotoPartOrMenuElem("About", this.gameMenuAbout, this);
                this.settingStringExitGame = new SettingsStringRender("Exit Game", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.gameMenuMain.addMenuElement(this.taskPlayMenu);
                this.gameMenuMain.addMenuElement(this.taskOptions);
                this.gameMenuMain.addMenuElement(this.taskHelp);
                this.gameMenuMain.addMenuElement(this.taskAbout);
                this.gameMenuMain.addMenuElement(this.settingStringExitGame);
                this.settingStringLevel = new SettingsStringRender("Level", this.field_370, this, this.field_361, false, this.micro, this.gameMenuPlay, false);
                this.settingsStringTrack = new SettingsStringRender("Track", this.field_345[this.field_370], this, this.levelNames[this.field_370], false, this.micro, this.gameMenuPlay, false);
                this.settingsStringLeague = new SettingsStringRender("League", this.field_371, this, this.leagueNames, false, this.micro, this.gameMenuPlay, false);

                try {
                    this.settingsStringTrack.setAvailableOptions(this.field_342[this.field_370]);
                } catch (ArrayIndexOutOfBoundsException var5) {
                    this.settingsStringTrack.setAvailableOptions(0);
                }

                this.settingStringLevel.setAvailableOptions(this.field_344);
                this.settingsStringLeague.setAvailableOptions(this.availableLeagues);
                this.gameTimerTaskHighscore = new TimerOrMotoPartOrMenuElem("Highscore", this.gameMenuHighscore, this);
                this.gameMenuHighscore.addMenuElement(this.settingStringBack);
                this.taskStart = new SettingsStringRender("Start>", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.gameMenuPlay.addMenuElement(this.taskStart);
                this.gameMenuPlay.addMenuElement(this.settingStringLevel);
                this.gameMenuPlay.addMenuElement(this.settingsStringTrack);
                this.gameMenuPlay.addMenuElement(this.settingsStringLeague);
                this.gameMenuPlay.addMenuElement(this.gameTimerTaskHighscore);
                this.gameMenuPlay.addMenuElement(this.settingStringGoToMain);
                if (this.isHasPointerEvents) {
                    this.settingStringSoftwareJoystick = new SettingsStringRender("Software Joystick", this.field_372, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                }

                this.perspectiveSetting = new SettingsStringRender("Perspective", this.isDisablePerspective, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                this.shadowsSetting = new SettingsStringRender("Shadows", this.isDisabledShadows, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                this.driverSpriteSetting = new SettingsStringRender("Driver sprite", this.isDisabledDriverSprite, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                this.bikeSpriteSetting = new SettingsStringRender("Bike sprite", this.isDisabledBikeSprite, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                this.inputSetting = new SettingsStringRender("Input", this.field_367, this, this.field_375, false, this.micro, this.gameMenuOptions, false);
                this.lookAheadSetting = new SettingsStringRender("Look ahead", this.isDisableLookAhead, this, this.field_374, true, this.micro, this.gameMenuOptions, false);
                this.clearHighscoreSetting = new TimerOrMotoPartOrMenuElem("Clear highscore", this.gameMenuConfirmClear, this);
                return;
            case 6:
                if (this.isHasPointerEvents) {
                    this.gameMenuOptions.addMenuElement(this.settingStringSoftwareJoystick);
                }

                this.gameMenuOptions.addMenuElement(this.perspectiveSetting);
                this.gameMenuOptions.addMenuElement(this.shadowsSetting);
                this.gameMenuOptions.addMenuElement(this.driverSpriteSetting);
                this.gameMenuOptions.addMenuElement(this.bikeSpriteSetting);
                this.gameMenuOptions.addMenuElement(this.inputSetting);
                this.gameMenuOptions.addMenuElement(this.lookAheadSetting);
                this.gameMenuOptions.addMenuElement(this.clearHighscoreSetting);
                this.gameMenuOptions.addMenuElement(this.settingStringBack);
                this.field_315 = new SettingsStringRender("No", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.field_314 = new SettingsStringRender("Yes", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.field_313 = new TimerOrMotoPartOrMenuElem("Full Reset", this.gameMenuConfirmReset, this);
                this.addTextRender(this.gameMenuConfirmClear, "Clearing the highscores can not be undone. It will remove all the registered times on all tracks.");
                this.addTextRender(this.gameMenuConfirmClear, "Would you like to clear the highscores?");
                this.gameMenuConfirmClear.addMenuElement(this.field_315);
                this.gameMenuConfirmClear.addMenuElement(this.field_314);
                this.gameMenuConfirmClear.addMenuElement(this.field_313);
                this.addTextRender(this.gameMenuConfirmReset, "A full reset can not be undone. It will relock all tracks and leagues and clear back all settings to default. A full reset will exit the application.");
                this.addTextRender(this.gameMenuConfirmReset, "Would you like to do a full reset?");
                this.gameMenuConfirmReset.addMenuElement(this.field_315);
                this.gameMenuConfirmReset.addMenuElement(this.field_314);
                this.field_317 = new GameMenu("Objective", this.micro, this.gameMenuHelp, (byte[]) null);
                this.field_318 = new TimerOrMotoPartOrMenuElem("Objective", this.field_317, this);
                this.addTextRender(this.field_317, "Race to the finish line as fast as you can without crashing. By leaning forward and backward you can adjust the rotation of your bike. By landing on both wheels after jumping, your bike won't crash as easily. Beware, the levels tend to get harder and harder...");
                this.field_317.addMenuElement(this.settingStringBack);
                this.gameMenuHelp.addMenuElement(this.field_318);
                this.field_319 = new GameMenu("Keys", this.micro, this.gameMenuHelp, (byte[]) null);
                this.field_320 = new TimerOrMotoPartOrMenuElem("Keys", this.field_319, this);
                this.addTextRender(this.field_319, "- " + this.field_375[0] + " -");
                this.addTextRender(this.field_319, "UP accelerates, DOWN brakes, RIGHT leans forward and LEFT leans backward. 1 accelerates and leans backward. 3 accelerates and leans forward. 7 brakes and leans backward. 9 brakes and leans forward.");
                this.field_319.addMenuElement(this.field_376);
                this.addTextRender(this.field_319, "- " + this.field_375[1] + " -");
                this.addTextRender(this.field_319, "1 accelerates, 4 brakes, 6 leans forward and 5 leans backward.");
                this.field_319.addMenuElement(this.field_376);
                this.addTextRender(this.field_319, "- " + this.field_375[2] + " -");
                this.addTextRender(this.field_319, "3 accelerates, 6 brakes, 5 leans forward and 4 leans backward.");
                this.field_319.addMenuElement(this.settingStringBack);
                this.gameMenuHelp.addMenuElement(this.field_320);
                this.field_321 = new GameMenu("Unlocking", this.micro, this.gameMenuHelp, (byte[]) null);
                this.field_322 = new TimerOrMotoPartOrMenuElem("Unlocking", this.field_321, this);
                this.addTextRender(this.field_321, "By completing the easier levels, new levels will be unlocked. You will also gain access to higher leagues where more advanced bikes with different characteristics are available.");
                this.field_321.addMenuElement(this.settingStringBack);
                this.gameMenuHelp.addMenuElement(this.field_322);
                this.gameMenuOptionsHighscoreDescription = new GameMenu("Highscore", this.micro, this.gameMenuHelp, (byte[]) null);
                this.taskHighscore = new TimerOrMotoPartOrMenuElem("Highscore", this.gameMenuOptionsHighscoreDescription, this);
                this.addTextRender(this.gameMenuOptionsHighscoreDescription, "The three best times on every track are saved for each league. When beating a time on a track you will be asked to enter your name. The highscores can be viewed from the Play Menu. By pressing left and right in the highscore view you can view the highscore for a specific league. The highscore can be cleared from the options menu.");
                this.gameMenuOptionsHighscoreDescription.addMenuElement(this.settingStringBack);
                this.gameMenuHelp.addMenuElement(this.taskHighscore);
                return;
            case 7:
                this.gameMenuOptions2 = new GameMenu("Options", this.micro, this.gameMenuHelp, (byte[]) null);
                this.field_326 = new TimerOrMotoPartOrMenuElem("Options", this.gameMenuOptions2, this);
                if (this.isHasPointerEvents) {
                    this.addTextRender(this.gameMenuOptions2, "Software Joystick: On/Off");
                    this.addTextRender(this.gameMenuOptions2, "Default: <On>. Specifies if you want to use the ingame software joystick (recommended) to steer your bike. Steer the bike by pressing a direction with your stylus.");
                    this.gameMenuOptions2.addMenuElement(this.field_376);
                }

                this.addTextRender(this.gameMenuOptions2, "Perspective: On/Off");
                this.addTextRender(this.gameMenuOptions2, "Default: <On>. Turns on and off the perspective view of the tracks.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Shadows: On/Off");
                this.addTextRender(this.gameMenuOptions2, "Default: <On>. Turns on and off the shadows.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Driver Sprite: On / Off");
                this.addTextRender(this.gameMenuOptions2, "Default: <On>. <On> uses a texture for the driver. <Off> uses line graphics.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Bike Sprite: On / Off");
                this.addTextRender(this.gameMenuOptions2, "Default: <On>. <On> uses a texture for the bike. <Off> uses line graphics.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Input: Keyset 1,2,3 ");
                this.addTextRender(this.gameMenuOptions2, "Default: <1>. Determines which type of input should be used when playing. See \"Keys\" in the help menu for more info.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Look ahead: On/Off");
                this.addTextRender(this.gameMenuOptions2, "Default: <On>. Turns on and off smart camera movement.");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.addTextRender(this.gameMenuOptions2, "Clear highscore");
                this.addTextRender(this.gameMenuOptions2, "Lets you clear the highscores. Here you can also do a \"Full Reset\" which will reset the game to original state (clear settings, highscores, unlocked levels and leagues).");
                this.gameMenuOptions2.addMenuElement(this.field_376);
                this.gameMenuOptions2.addMenuElement(this.settingStringBack);
                this.gameMenuHelp.addMenuElement(this.field_326);
                this.gameMenuHelp.addMenuElement(this.settingStringBack);
                this.addTextRender(this.gameMenuAbout, "\"Gravity Defied - Trial Racing\" v1.0 by Codebrew Software © 2004.");
                this.addTextRender(this.gameMenuAbout, "brought 2 you by pascha.                For information visit:");
                this.gameMenuAbout.addMenuElement(this.textRenderCodeBrewLink);
                this.gameMenuAbout.addMenuElement(this.settingStringBack);
                this.field_334 = new SettingsStringRender("Track: " + this.micro.levelLoader.getName(0, 1), 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.field_333 = new SettingsStringRender("Restart: " + this.micro.levelLoader.getName(0, 0), 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.gameMenuIngame.addMenuElement(this.settingStringContinue);
                this.gameMenuIngame.addMenuElement(this.field_333);
                this.gameMenuIngame.addMenuElement(this.taskOptions);
                this.gameMenuIngame.addMenuElement(this.taskHelp);
                this.gameMenuIngame.addMenuElement(this.settingStringPlayMenu);
                this.field_335 = new SettingsStringRender("Ok", 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.field_336 = new SettingsStringRender("Name - " + new String(this.field_341), 0, this, (String[]) null, false, this.micro, this.gameMenuMain, true);
                this.commandOk = new Command("Ok", 4, 1);
                this.commandBack = new Command("Back", 2, 1);
                this.method_1(this.gameMenuMain, false);

                try {
                    this.rasterImage = Image.createImage("/raster.png");
                    return;
                } catch (IOException var10) {
                    this.rasterImage = Image.createImage(1, 1);
                }
            default:
        }
    }

    // $FF: renamed from: a (e, java.lang.String) void
    private void addTextRender(GameMenu gameMenu, String text) {
        TextRender[] var3 = TextRender.makeMultilineTextRenders(text, this.micro);

        for (int var4 = 0; var4 < var3.length; ++var4) {
            gameMenu.addMenuElement(var3[var4]);
        }
    }

    // $FF: renamed from: g () int
    public int getCurrentLevel() {
        return this.settingStringLevel.getCurrentOptionPos();
    }

    // $FF: renamed from: e () int
    public int getCurrentTrack() {
        return this.settingsStringTrack.getCurrentOptionPos();
    }

    // $FF: renamed from: k () boolean
    public boolean method_196() {
        if (this.field_357) {
            this.field_357 = false;
            return true;
        } else {
            return false;
        }
    }

    // $FF: renamed from: b () void
    private void method_197() {
        this.recordManager.method_17(this.settingsStringLeague.getCurrentOptionPos(), this.field_341, this.field_337);
        this.recordManager.writeRecordInfo();
        this.field_356 = false;
        this.gameMenuFinished.clearVector();
        this.gameMenuFinished.addMenuElement(new TextRender("Time: " + this.field_340, this.micro));
        System.gc();
        String[] var1 = this.recordManager.getRecordDescription(this.settingsStringLeague.getCurrentOptionPos());

        for (int var2 = 0; var2 < var1.length; ++var2) {
            if (var1[var2] != null) {
                this.gameMenuFinished.addMenuElement(new TextRender("" + (var2 + 1) + "." + var1[var2], this.micro));
            }
        }

        this.recordManager.closeRecordStore();
        byte availableLeagues = -1;
        if (this.settingsStringTrack.getMaxAvailableOptionPos() >= this.settingsStringTrack.getCurrentOptionPos()) {
            this.settingsStringTrack.setAvailableOptions(this.settingsStringTrack.getCurrentOptionPos() + 1 < this.field_342[this.settingStringLevel.getCurrentOptionPos()] ? this.field_342[this.settingStringLevel.getCurrentOptionPos()] : this.settingsStringTrack.getCurrentOptionPos() + 1);
            this.field_342[this.settingStringLevel.getCurrentOptionPos()] = (byte) this.settingsStringTrack.getMaxAvailableOptionPos() < this.field_342[this.settingStringLevel.getCurrentOptionPos()] ? this.field_342[this.settingStringLevel.getCurrentOptionPos()] : (byte) this.settingsStringTrack.getMaxAvailableOptionPos();
        }

        if (this.settingsStringTrack.getCurrentOptionPos() == this.settingsStringTrack.getMaxOptionPos()) {
            this.field_356 = true;
            switch (this.settingStringLevel.getCurrentOptionPos()) {
                case 0:
                    if (this.availableLeagues < 1) {
                        availableLeagues = 1;
                        this.availableLeagues = 1;
                        this.settingsStringLeague.setAvailableOptions(this.availableLeagues);
                    }
                    break;
                case 1:
                    if (this.availableLeagues < 2) {
                        availableLeagues = 2;
                        this.availableLeagues = 2;
                        this.settingsStringLeague.setAvailableOptions(this.availableLeagues);
                    }
                    break;
                case 2:
                    if (this.availableLeagues < 3) {
                        availableLeagues = 3;
                        this.availableLeagues = 3;
                        this.settingsStringLeague.setOptionsList(this.leagueNamesAll4);
                        this.leagueNames = this.leagueNamesAll4;
                        this.settingsStringLeague.setAvailableOptions(this.availableLeagues);
                    }
            }

            this.settingStringLevel.setAvailableOptions(this.settingStringLevel.getMaxAvailableOptionPos() + 1);
            if (this.field_342[this.settingStringLevel.getMaxAvailableOptionPos()] == -1) {
                this.field_342[this.settingStringLevel.getMaxAvailableOptionPos()] = 0;
            }
        }

        int var3 = this.getCountOfRecordStoresWithPrefix(this.settingStringLevel.getCurrentOptionPos());
        this.addTextRender(this.gameMenuFinished, var3 + " of " + this.levelNames[this.settingStringLevel.getCurrentOptionPos()].length + " tracks in " + this.field_361[this.settingStringLevel.getCurrentOptionPos()] + " completed.");
        System.gc();
        if (!this.field_356) {
            this.field_333.setText("Restart: " + this.micro.levelLoader.getName(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos()));
            this.field_334.setText("Next: " + this.micro.levelLoader.getName(this.field_354, this.field_355 + 1));
        } else {
            if (this.settingStringLevel.getCurrentOptionPos() < this.settingStringLevel.getMaxOptionPos()) {
                this.settingStringLevel.setCurentOptionPos(this.settingStringLevel.getCurrentOptionPos() + 1);
                this.settingsStringTrack.setCurentOptionPos(0);
                this.settingsStringTrack.setAvailableOptions(this.field_342[this.settingStringLevel.getCurrentOptionPos()]);
            }

            if (availableLeagues != -1) {
                this.addTextRender(this.gameMenuFinished, "Congratultions! You have successfully unlocked a new league: " + this.leagueNames[availableLeagues]);
                if (availableLeagues == 3) {
                    this.gameMenuFinished.addMenuElement(new TextRender("Enjoy...", this.micro));
                }

                this.showAlert("League unlocked", "You have successfully unlocked a new league: " + this.leagueNames[availableLeagues], (Image) null);
            } else {
                boolean var4 = true;

                for (int var5 = 0; var5 < 3; ++var5) {
                    if (this.field_342[var5] != this.micro.levelLoader.levelNames[var5].length - 1) {
                        var4 = false;
                    }
                }

                if (!var4) {
                    this.addTextRender(this.gameMenuFinished, "You have completed all tracks at this level.");
                }
            }
        }

        if (!this.field_356) {
            this.gameMenuFinished.addMenuElement(this.field_334);
        }

        this.field_333.setText("Restart: " + this.micro.levelLoader.getName(this.field_354, this.field_355));
        this.gameMenuFinished.addMenuElement(this.field_333);
        this.gameMenuFinished.addMenuElement(this.settingStringPlayMenu);
        this.method_1(this.gameMenuFinished, false);
    }

    // $FF: renamed from: h () void
    public void repaint() {
        this.micro.gameCanvas.repaint();
    }

    // $FF: renamed from: void () int
    public int getCanvasHeight() {
        return this.micro.gameCanvas.getHeight();
    }

    // $FF: renamed from: c () int
    public int getCanvasWidth() {
        return this.micro.gameCanvas.getWidth();
    }

    // $FF: renamed from: char (int) void
    public void method_201(int var1) {
        this.field_377 = false;
        switch (var1) {
            case 0:
                this.method_1(this.gameMenuMain, false);
                this.micro.gamePhysics.enableGenerateInputAI();
                this.field_357 = true;
                break;
            case 1:
                this.field_354 = this.settingStringLevel.getCurrentOptionPos();
                this.field_355 = this.settingsStringTrack.getCurrentOptionPos();
                this.field_333.setText("Restart: " + this.micro.levelLoader.getName(this.field_354, this.field_355));
                this.field_357 = false;
                this.method_1(this.gameMenuIngame, false);
                break;
            case 2:
                this.field_362 = System.currentTimeMillis();
                this.gameMenuFinished.clearVector();
                this.field_354 = this.settingStringLevel.getCurrentOptionPos();
                this.field_355 = this.settingsStringTrack.getCurrentOptionPos();
                this.recordManager.method_8(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos());
                int var2 = this.recordManager.getPosOfNewRecord(this.settingsStringLeague.getCurrentOptionPos(), this.field_337);
                this.field_340 = this.method_219(this.field_337);
                if (var2 >= 0 && var2 <= 2) {
                    TextRender var3;
                    (var3 = new TextRender("", this.micro)).setDx(GameCanvas.spriteSizeX[5] + 1);
                    switch (var2) {
                        case 0:
                            var3.setText("First place!");
                            var3.setDrawSprite(true, 5);
                            break;
                        case 1:
                            var3.setText("Second place!");
                            var3.setDrawSprite(true, 6);
                            break;
                        case 2:
                            var3.setText("Third place!");
                            var3.setDrawSprite(true, 7);
                    }

                    this.gameMenuFinished.addMenuElement(var3);
                    TextRender var4;
                    (var4 = new TextRender("" + this.field_340, this.micro)).setDx(GameCanvas.spriteSizeX[5] + 1);
                    this.gameMenuFinished.addMenuElement(var4);
                    this.gameMenuFinished.addMenuElement(this.field_335);
                    this.gameMenuFinished.addMenuElement(this.field_336);
                    this.method_1(this.gameMenuFinished, false);
                    this.field_377 = false;
                    System.gc();
                } else {
                    this.method_197();
                }
                break;
            default:
                this.method_1(this.gameMenuMain, false);
        }

        long currentTimeMillis = System.currentTimeMillis();
        this.micro.gameCanvas.isDrawingTime = false;
        long var6 = 0L;
        byte var8 = 50;
        this.micro.gamePhysics.method_53();
        this.micro.gameToMenu();

        Micro micro;
        while (Micro.isInGameMenu && Micro.field_249 && this.currentGameMenu != null) {
            if (Micro.isPaused) {
                while (Micro.isPaused) {
                    try {
                        Thread.sleep(100L);
                    } catch (InterruptedException var18) {
                    }
                }
            }

            long var20;
            if (this.micro.gamePhysics.isGenerateInputAI()) {
                int var9;
                if ((var9 = this.micro.gamePhysics.updatePhysics()) != 0 && var9 != 4) {
                    try {
                        this.micro.gamePhysics.resetSmth(true);
                    } catch (NullPointerException var17) {
                    }
                }

                this.micro.gamePhysics.method_53();
                this.repaint();
                if ((var20 = System.currentTimeMillis()) - var6 < (long) var8) {
                    try {
                        synchronized (this.field_359) {
                            this.field_359.wait((long) var8 - (var20 - var6) < 1L ? 1L : (long) var8 - (var20 - var6));
                        }
                    } catch (InterruptedException var16) {
                    }

                    var6 = System.currentTimeMillis();
                } else {
                    var6 = var20;
                }
            } else {
                var8 = 50;
                if ((var20 = System.currentTimeMillis()) - var6 < (long) var8) {
                    try {
                        Object var21;
                        synchronized (var21 = new Object()) {
                            var21.wait((long) var8 - (var20 - var6) < 1L ? 1L : (long) var8 - (var20 - var6));
                        }
                    } catch (InterruptedException var14) {
                    }

                    var6 = System.currentTimeMillis();
                } else {
                    var6 = var20;
                }

                micro = this.micro;
                if (Micro.isInGameMenu) {
                    this.repaint();
                }
            }
        }

        micro = this.micro;
        micro.timeMs += System.currentTimeMillis() - currentTimeMillis;
        this.micro.gameCanvas.isDrawingTime = true;
        if (this.currentGameMenu == null) {
            Micro.field_249 = false;
        }

    }

    // $FF: renamed from: if (javax.microedition.lcdui.Graphics) void
    protected synchronized void method_202(Graphics var1) {
        if (this.currentGameMenu != null && !this.field_377) {
            this.micro.gameCanvas.render_160(var1);
            this.fillCanvasWithImage(var1);
            this.currentGameMenu.render_76(var1);
        }
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics) void
    private void fillCanvasWithImage(Graphics graphics) {
        for (int y = 0; y < this.getCanvasHeight(); y += this.rasterImage.getHeight()) {
            for (int x = 0; x < this.getCanvasWidth(); x += this.rasterImage.getWidth()) {
                graphics.drawImage(this.rasterImage, x, y, 20);
            }
        }

    }

    // $FF: renamed from: try (int) void
    protected void processNonFireKeyCode(int keyCode) {
        if (this.micro.gameCanvas.getGameAction(keyCode) != 8) {
            // if not fire
            this.processKeyCode(keyCode);
        }
    }

    // $FF: renamed from: case (int) void
    protected void processKeyCode(int keyCode) {
        if (this.currentGameMenu != null) {
            switch (this.micro.gameCanvas.getGameAction(keyCode)) {
                case 1: // UP
                    this.currentGameMenu.processGameActionUp();
                    return;
                case 2: // LEFT
                    this.currentGameMenu.processGameActionUpd(3);
                    if (this.currentGameMenu == this.gameMenuHighscore) {
                        --this.field_360;
                        if (this.field_360 < 0) {
                            this.field_360 = 0;
                        }

                        this.method_207(this.field_360);
                    }
                case 3:
                case 4:
                case 7:
                default:
                    break;
                case 5: // RIGHT
                    this.currentGameMenu.processGameActionUpd(2);
                    if (this.currentGameMenu == this.gameMenuHighscore) {
                        ++this.field_360;
                        if (this.field_360 > this.settingsStringLeague.getMaxAvailableOptionPos()) {
                            this.field_360 = this.settingsStringLeague.getMaxAvailableOptionPos();
                        }

                        this.method_207(this.field_360);
                        return;
                    }
                    break;
                case 6: // DOWN
                    this.currentGameMenu.processGameActionDown();
                    return;
                case 8: // FIRE
                    this.currentGameMenu.processGameActionUpd(1);
                    return;
            }
        }

    }

    // $FF: renamed from: a (javax.microedition.lcdui.Command, javax.microedition.lcdui.Displayable) void
    public void method_206(Command var1, Displayable var2) {
        if (var1 == this.commandOk) {
            if (this.currentGameMenu != null) {
                this.currentGameMenu.processGameActionUpd(1);
                return;
            }
        } else if (var1 == this.commandBack && this.currentGameMenu != null) {
            if (this.currentGameMenu == this.gameMenuIngame) {
                this.micro.menuToGame();
                return;
            }

            this.method_1(this.currentGameMenu.getGameMenu(), true);
        }

    }

    // $FF: renamed from: a () e
    public GameMenu getGameMenu() {
        return this.currentGameMenu;
    }

    // $FF: renamed from: a (e, boolean) void
    public void method_1(GameMenu gm, boolean var2) {
        this.micro.gameCanvas.removeCommand(this.commandBack);
        if (gm != this.gameMenuMain && gm != this.gameMenuFinished && gm != null) {
            this.micro.gameCanvas.addCommand(this.commandBack);
        }

        if (gm == this.gameMenuHighscore) {
            this.field_360 = this.settingsStringLeague.getCurrentOptionPos();
            this.method_207(this.field_360);
        } else if (gm == this.gameMenuFinished) {
            this.field_341 = this.gameMenuEnterName.getStrArr();
            this.field_336.setText("Name - " + new String(this.field_341));
        } else if (gm == this.gameMenuPlay) {
            this.settingsStringTrack.setOptionsList(this.micro.levelLoader.levelNames[this.settingStringLevel.getCurrentOptionPos()]);
            if (this.currentGameMenu == this.field_299) {
                this.field_345[this.settingStringLevel.getCurrentOptionPos()] = this.settingsStringTrack.getCurrentOptionPos();
            }

            this.settingsStringTrack.setAvailableOptions(this.field_342[this.settingStringLevel.getCurrentOptionPos()]);
            this.settingsStringTrack.setCurentOptionPos(this.field_345[this.settingStringLevel.getCurrentOptionPos()]);
        }

        if (gm == this.gameMenuMain || gm == this.gameMenuPlay) {
            this.micro.gamePhysics.enableGenerateInputAI();
        }

        this.currentGameMenu = gm;
        if (this.currentGameMenu != null && !var2) {
            this.currentGameMenu.method_70();
        }

        this.field_377 = false;
    }

    // $FF: renamed from: new (int) void
    public void method_207(int var1) {
        this.gameMenuHighscore.clearVector();
        this.recordManager.method_8(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos());
        this.gameMenuHighscore.addMenuElement(new TextRender(this.micro.levelLoader.getName(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos()), this.micro));
        this.gameMenuHighscore.addMenuElement(new TextRender("LEAGUE: " + this.settingsStringLeague.getOptionsList()[var1], this.micro));
        String[] var2 = this.recordManager.getRecordDescription(var1);

        for (int var3 = 0; var3 < var2.length; ++var3) {
            if (var2[var3] != null) {
                TextRender var4;
                (var4 = new TextRender("" + (var3 + 1) + "." + var2[var3], this.micro)).setDx(GameCanvas.spriteSizeX[5] + 1);
                if (var3 == 0) {
                    var4.setDrawSprite(true, 5);
                } else if (var3 == 1) {
                    var4.setDrawSprite(true, 6);
                } else if (var3 == 2) {
                    var4.setDrawSprite(true, 7);
                }

                this.gameMenuHighscore.addMenuElement(var4);
            }
        }

        this.recordManager.closeRecordStore();
        if (var2[0] == null) {
            this.gameMenuHighscore.addMenuElement(new TextRender("No Highscores", this.micro));
        }

        this.gameMenuHighscore.addMenuElement(this.settingStringBack);
        System.gc();
    }

    // $FF: renamed from: if () void
    public synchronized void saveSmthToRecordStoreAndCloseIt() {
        if (this.isRecordStoreOpened) {
            this.method_208();

            try {
                this.recordStore.closeRecordStore();
                this.isRecordStoreOpened = false;
            } catch (RecordStoreException var1) {
            }
        }

        this.currentGameMenu = null;
    }

    // $FF: renamed from: f () void
    public void method_208() {
        this.copyThreeBytesFromArr(16, this.field_341);
        if (this.isHasPointerEvents) {
            this.setValue(13, (byte) this.settingStringSoftwareJoystick.getCurrentOptionPos());
        }

        this.setValue(0, (byte) this.perspectiveSetting.getCurrentOptionPos());
        this.setValue(1, (byte) this.shadowsSetting.getCurrentOptionPos());
        this.setValue(2, (byte) this.driverSpriteSetting.getCurrentOptionPos());
        this.setValue(3, (byte) this.bikeSpriteSetting.getCurrentOptionPos());
        this.setValue(14, (byte) this.inputSetting.getCurrentOptionPos());
        this.setValue(4, (byte) this.lookAheadSetting.getCurrentOptionPos());
        this.setValue(5, (byte) this.settingsStringLeague.getMaxAvailableOptionPos());
        this.setValue(6, (byte) this.settingStringLevel.getMaxAvailableOptionPos());
        this.setValue(10, (byte) this.settingStringLevel.getCurrentOptionPos());
        this.setValue(11, (byte) this.settingsStringTrack.getCurrentOptionPos());
        this.setValue(12, (byte) this.settingsStringLeague.getCurrentOptionPos());

        for (int i = 0; i < 3; ++i) {
            this.setValue(7 + i, this.field_342[i]);
        }

        if (this.recorcStoreRecordId == -1) {
            try {
                this.recorcStoreRecordId = this.recordStore.addRecord(this.field_278, 0, 19);
            } catch (RecordStoreNotOpenException var2) {
            } catch (RecordStoreException var3) {
            }
        } else {
            try {
                this.recordStore.setRecord(this.recorcStoreRecordId, this.field_278, 0, 19);
            } catch (RecordStoreNotOpenException var4) {
            } catch (RecordStoreException var5) {
            }
        }
    }

    public void run() {
        if (this.alert != null) {
            Display.getDisplay(this.micro).setCurrent(this.alert);
        }
    }

    // $FF: renamed from: a (java.lang.String, java.lang.String, javax.microedition.lcdui.Image) void
    public void showAlert(String title, String alertText, Image image) {
        this.alert = new Alert(title, alertText, image, AlertType.INFO);
        this.alert.setTimeout(4000);
        (new Thread(this)).start();
    }

    // $FF: renamed from: a (j) void
    public void processMenu(IGameMenuElement menuElement) {
        if (menuElement == this.taskStart) {
            if (this.settingStringLevel.getCurrentOptionPos() <= this.settingStringLevel.getMaxAvailableOptionPos() && this.settingsStringTrack.getCurrentOptionPos() <= this.settingsStringTrack.getMaxAvailableOptionPos() && this.settingsStringLeague.getCurrentOptionPos() <= this.settingsStringLeague.getMaxAvailableOptionPos()) {
                this.micro.gamePhysics.disableGenerateInputAI();
                this.micro.levelLoader.method_88(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos());
                this.micro.gamePhysics.setMotoLeague(this.settingsStringLeague.getCurrentOptionPos());
                this.field_357 = true;
                this.micro.menuToGame();
            } else {
                this.showAlert("GDTR", "Complete more tracks to unlock this track/league combo.", (Image) null);
            }
        } else if (menuElement == this.settingStringSoftwareJoystick) {
            this.micro.gameCanvas.method_124(this.settingStringSoftwareJoystick.getCurrentOptionPos() == 0);
        } else if (menuElement == this.perspectiveSetting) {
            this.micro.gamePhysics.method_26(this.perspectiveSetting.getCurrentOptionPos() == 0);
            LevelLoader var10000 = this.micro.levelLoader;
            LevelLoader.isEnabledPerspective = this.perspectiveSetting.getCurrentOptionPos() == 0;
        } else if (menuElement == this.shadowsSetting) {
            LevelLoader.isEnabledShadows = this.shadowsSetting.getCurrentOptionPos() == 0;
        } else {
            if (menuElement == this.driverSpriteSetting) {
                if (this.driverSpriteSetting.method_114()) {
                    this.driverSpriteSetting.setCurentOptionPos(this.driverSpriteSetting.getCurrentOptionPos() + 1);
                    return;
                }
            } else if (menuElement == this.bikeSpriteSetting) {
                if (this.bikeSpriteSetting.method_114()) {
                    this.bikeSpriteSetting.setCurentOptionPos(this.bikeSpriteSetting.getCurrentOptionPos() + 1);
                    return;
                }
            } else {
                if (menuElement == this.inputSetting) {
                    if (this.inputSetting.method_114()) {
                        this.inputSetting.setCurentOptionPos(this.inputSetting.getCurrentOptionPos() + 1);
                    }

                    this.micro.gameCanvas.method_163(this.inputSetting.getCurrentOptionPos());
                    return;
                }

                if (menuElement == this.lookAheadSetting) {
                    this.micro.gamePhysics.setEnableLookAhead(this.lookAheadSetting.getCurrentOptionPos() == 0);
                    return;
                }

                if (menuElement == this.field_314) {
                    if (this.currentGameMenu == this.gameMenuConfirmClear) {
                        this.recordManager.deleteRecordStores();
                        this.showAlert("Cleared", "Highscores have been cleared", (Image) null);
                    } else if (this.currentGameMenu == this.gameMenuConfirmReset) {
                        this.exit();
                        this.showAlert("Reset", "Master reset. Application will be closed.", (Image) null);
                    }

                    this.method_1(this.currentGameMenu.getGameMenu(), false);
                    return;
                }

                if (menuElement == this.field_315) {
                    this.method_1(this.currentGameMenu.getGameMenu(), false);
                    return;
                }

                if (menuElement == this.settingStringBack) {
                    this.method_1(this.currentGameMenu.getGameMenu(), true);
                    return;
                }

                if (menuElement == this.settingStringPlayMenu) {
                    this.settingStringLevel.setCurentOptionPos(this.field_354);
                    this.settingsStringTrack.setAvailableOptions(this.field_342[this.field_354]);
                    this.settingsStringTrack.setCurentOptionPos(this.field_355);
                    this.method_1(this.currentGameMenu.getGameMenu(), false);
                    return;
                }

                if (menuElement == this.settingStringGoToMain) {
                    this.method_1(this.gameMenuMain, false);
                    return;
                }

                if (menuElement == this.settingStringExitGame) {
                    this.method_1(this.currentGameMenu.getGameMenu(), false);
                    return;
                }

                if (menuElement == this.field_333) {
                    if (this.settingsStringLeague.getCurrentOptionPos() <= this.settingsStringLeague.getMaxAvailableOptionPos()) {
                        this.settingStringLevel.setCurentOptionPos(this.field_354);
                        this.settingsStringTrack.setAvailableOptions(this.field_342[this.field_354]);
                        this.settingsStringTrack.setCurentOptionPos(this.field_355);
                        this.micro.gamePhysics.setMotoLeague(this.settingsStringLeague.getCurrentOptionPos());
                        this.field_357 = true;
                        this.micro.menuToGame();
                        return;
                    }
                } else {
                    if (menuElement == this.field_334) {
                        if (!this.field_356) {
                            this.settingsStringTrack.menuElemMethod(2);
                        }

                        this.micro.levelLoader.method_88(this.settingStringLevel.getCurrentOptionPos(), this.settingsStringTrack.getCurrentOptionPos());
                        this.micro.gamePhysics.setMotoLeague(this.settingsStringLeague.getCurrentOptionPos());
                        this.method_208();
                        this.field_357 = true;
                        this.micro.menuToGame();
                        return;
                    }

                    if (menuElement == this.settingStringContinue) {
                        this.repaint();
                        this.micro.menuToGame();
                        return;
                    }

                    if (menuElement == this.field_336) {
                        this.gameMenuEnterName.method_70();
                        this.method_1(this.gameMenuEnterName, false);
                        return;
                    }

                    if (menuElement == this.field_335) {
                        this.method_197();
                        return;
                    }

                    if (menuElement == this.settingsStringTrack) {
                        if (this.settingsStringTrack.method_114()) {
                            this.settingsStringTrack.setAvailableOptions(this.field_342[this.settingStringLevel.getCurrentOptionPos()]);
                            this.settingsStringTrack.init();
                            this.field_299 = this.settingsStringTrack.getGameMenu();
                            this.method_1(this.field_299, false);
                            this.field_299.method_83(this.settingsStringTrack.getCurrentOptionPos());
                        }

                        this.field_345[this.settingStringLevel.getCurrentOptionPos()] = this.settingsStringTrack.getCurrentOptionPos();
                        return;
                    }

                    if (menuElement == this.settingStringLevel) {
                        if (this.settingStringLevel.method_114()) {
                            this.gameMenuStringLevel = this.settingStringLevel.getGameMenu();
                            this.method_1(this.gameMenuStringLevel, false);
                            this.gameMenuStringLevel.method_83(this.settingStringLevel.getCurrentOptionPos());
                        }

                        this.settingsStringTrack.setOptionsList(this.micro.levelLoader.levelNames[this.settingStringLevel.getCurrentOptionPos()]);
                        this.settingsStringTrack.setAvailableOptions(this.field_342[this.settingStringLevel.getCurrentOptionPos()]);
                        this.settingsStringTrack.setCurentOptionPos(this.field_345[this.settingStringLevel.getCurrentOptionPos()]);
                        this.settingsStringTrack.init();
                        return;
                    }

                    if (menuElement == this.settingsStringLeague && this.settingsStringLeague.method_114()) {
                        this.gameMenuLeague = this.settingsStringLeague.getGameMenu();
                        this.settingsStringLeague.setParentGameMenu(this.currentGameMenu);
                        this.method_1(this.gameMenuLeague, false);
                        this.gameMenuLeague.method_83(this.settingsStringLeague.getCurrentOptionPos());
                    }
                }
            }
        }
    }

    // $FF: renamed from: j () int
    public int method_210() {
        int var1 = 0;
        if (this.driverSpriteSetting.getCurrentOptionPos() == 0) {
            var1 |= 2;
        }

        if (this.bikeSpriteSetting.getCurrentOptionPos() == 0) {
            var1 |= 1;
        }

        return var1;
    }

    // $FF: renamed from: int (int) void
    public void method_211(int var1) {
        this.bikeSpriteSetting.setCurentOptionPos(1);
        this.driverSpriteSetting.setCurentOptionPos(1);
        if ((var1 & 1) > 0) {
            this.bikeSpriteSetting.setCurentOptionPos(0);
        }

        if ((var1 & 2) > 0) {
            this.driverSpriteSetting.setCurentOptionPos(0);
        }
    }

    // $FF: renamed from: o () int
    public int method_212() {
        return this.settingStringLevel.getCurrentOptionPos();
    }

    // $FF: renamed from: n () int
    public int method_213() {
        return this.settingsStringTrack.getCurrentOptionPos();
    }

    // $FF: renamed from: l () int
    public int method_214() {
        return this.settingsStringLeague.getCurrentOptionPos();
    }

    // $FF: renamed from: a (long) void
    public void method_215(long var1) {
        this.field_337 = var1;
    }

    // $FF: renamed from: if (int, byte) byte[]
    private byte[] method_216(int var1, byte var2) {
        switch (var1) {
            case 16:
                byte[] var3 = new byte[3];

                for (int var4 = 0; var4 < 3; ++var4) {
                    var3[var4] = this.field_278[16 + var4];
                }

                if (var3[0] == -127) {
                    var3[0] = var2;
                }

                return var3;
            default:
                return null;
        }
    }

    // $FF: renamed from: a (int, byte) byte
    private byte method_217(int var1, byte var2) {
        return this.field_278[var1] == -127 ? var2 : this.field_278[var1];
    }

    // $FF: renamed from: a (int, byte[]) void
    private void copyThreeBytesFromArr(int var1, byte[] var2) {
        if (this.isRecordStoreOpened && var1 == 16) {
            for (int i = 0; i < 3; ++i) {
                this.field_278[16 + i] = var2[i];
            }
        }
    }

    // $FF: renamed from: if (long) java.lang.String
    private String method_219(long var1) {
        this.field_338 = (int) (var1 / 100L);
        this.field_339 = (int) (var1 % 100L);
        String var3;
        if (this.field_338 / 60 < 10) {
            var3 = " 0" + this.field_338 / 60;
        } else {
            var3 = " " + this.field_338 / 60;
        }

        if (this.field_338 % 60 < 10) {
            var3 = var3 + ":0" + this.field_338 % 60;
        } else {
            var3 = var3 + ":" + this.field_338 % 60;
        }

        if (this.field_339 < 10) {
            var3 = var3 + ".0" + this.field_339;
        } else {
            var3 = var3 + "." + this.field_339;
        }

        return var3;
    }

    // $FF: renamed from: do (int, byte) void
    private void setValue(int pos, byte value) {
        if (this.isRecordStoreOpened) {
            this.field_278[pos] = value;
        }

    }

    // $FF: renamed from: m () void
    private void exit() {
        this.perspectiveSetting.setCurentOptionPos(0);
        this.shadowsSetting.setCurentOptionPos(0);
        this.driverSpriteSetting.setCurentOptionPos(0);
        this.bikeSpriteSetting.setCurentOptionPos(0);
        this.lookAheadSetting.setCurentOptionPos(0);
        this.settingsStringLeague.setCurentOptionPos(0);
        this.settingsStringLeague.setAvailableOptions(0);
        this.settingStringLevel.setCurentOptionPos(0);
        this.settingStringLevel.setAvailableOptions(1);
        this.settingsStringTrack.setCurentOptionPos(0);
        if (this.isHasPointerEvents) {
            this.settingStringSoftwareJoystick.setCurentOptionPos(0);
        }

        this.field_341[0] = 65;
        this.field_341[1] = 65;
        this.field_341[2] = 65;
        this.inputSetting.setCurentOptionPos(0);
        this.field_342[0] = 0;
        this.field_342[1] = 0;
        this.field_342[2] = -1;
        this.availableLeagues = 0;
        this.method_208();
        this.recordManager.deleteRecordStores();
        this.micro.notifyDestroyed();
    }

    // $FF: renamed from: d () void
    public void removeOkAndBackCommands() {
        this.micro.gameCanvas.removeCommand(this.commandOk);
        this.micro.gameCanvas.removeCommand(this.commandBack);
    }

    // $FF: renamed from: i () void
    public void addOkAndBackCommands() {
        if (this.currentGameMenu != this.gameMenuMain && this.currentGameMenu != this.gameMenuFinished && this.currentGameMenu != null) {
            this.micro.gameCanvas.addCommand(this.commandBack);
        }

        this.micro.gameCanvas.addCommand(this.commandOk);
    }

    // $FF: renamed from: bb (int) int
    private int getCountOfRecordStoresWithPrefix(int prefixNumber) {
        String[] storeNames = RecordStore.listRecordStores();
        if (this.recordManager != null && storeNames != null) {
            int count = 0;

            for (int i = 0; i < storeNames.length; ++i) {
                if (storeNames[i].startsWith("" + prefixNumber)) {
                    ++count;
                }
            }

            return count;
        } else {
            return 0;
        }
    }
}
