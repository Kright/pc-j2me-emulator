import javax.microedition.lcdui.Graphics;

// $FF: renamed from: g
public class SettingsStringRender implements IGameMenuElement, IMenuManager {
    // $FF: renamed from: t java.lang.String[]
    private String[] optionsList;
    // $FF: renamed from: q int
    protected int currentOptionPos;
    // $FF: renamed from: r int
    private int maxAvailableOption;
    // $FF: renamed from: v java.lang.String
    protected String text;
    // $FF: renamed from: w c
    private IMenuManager menuManager;
    // $FF: renamed from: k e
    private GameMenu currentGameMenu = null;
    // $FF: renamed from: l e
    private GameMenu parentGameMenu = null;
    // $FF: renamed from: s boolean
    private boolean field_146;
    // $FF: renamed from: o boolean
    private boolean field_147 = false;
    // $FF: renamed from: p java.lang.String
    private String selectedOptionName;
    // $FF: renamed from: m Micro
    private Micro micro = null;
    // $FF: renamed from: n g[]
    private SettingsStringRender[] settingsStringRenders = null;
    // $FF: renamed from: x boolean
    private boolean hasSprite;
    // $FF: renamed from: u boolean
    private boolean isDrawSprite8;
    // $FF: renamed from: j boolean
    private boolean useColon;

    public SettingsStringRender(String text, int isDisabled, IMenuManager menuManager, String[] optionsList, boolean var5, Micro micro, GameMenu gameMenu, boolean useColon) {
        this.micro = micro;
        if (useColon) {
            this.useColon = true;
            this.text = text;
            this.menuManager = menuManager;
            this.isDrawSprite8 = true;
            this.hasSprite = false;
        } else {
            this.useColon = false;
            this.text = text + ":";
            this.currentOptionPos = isDisabled;
            this.menuManager = menuManager;
            this.optionsList = optionsList;
            if (this.optionsList == null) {
                this.optionsList = new String[]{""};
            }

            this.maxAvailableOption = this.optionsList.length - 1;
            this.field_146 = var5;
            this.setCurentOptionPos(isDisabled);
            if (var5) {
                if (isDisabled == 1) {
                    this.selectedOptionName = "Off";
                } else {
                    this.selectedOptionName = "On";
                }
            } else {
                this.parentGameMenu = gameMenu;
                this.selectCurrentOptionName();
                this.init();
            }
        }
    }

    // $FF: renamed from: a (boolean, boolean) void
    public void setFlags(boolean hasSprite, boolean isDrawSprite8) {
        this.hasSprite = hasSprite;
        this.isDrawSprite8 = isDrawSprite8;
    }

    // $FF: renamed from: a (java.lang.String[]) void
    public void setOptionsList(String[] var1) {
        this.optionsList = var1;
        if (this.currentOptionPos > this.optionsList.length - 1) {
            this.currentOptionPos = this.optionsList.length - 1;
        }

        if (this.maxAvailableOption > this.optionsList.length - 1) {
            this.maxAvailableOption = this.optionsList.length - 1;
        }

        this.selectCurrentOptionName();
        this.init();
    }

    // $FF: renamed from: byte () void
    public void init() {
        this.currentGameMenu = new GameMenu(this.text, this.micro, this.parentGameMenu, (byte[]) null);
        this.settingsStringRenders = new SettingsStringRender[this.optionsList.length];

        for (int var1 = 0; var1 < this.settingsStringRenders.length; ++var1) {
            if (var1 > this.maxAvailableOption) {
                this.settingsStringRenders[var1] = new SettingsStringRender(this.optionsList[var1], 0, this, (String[]) null, false, this.micro, this.parentGameMenu, true);
                this.settingsStringRenders[var1].setFlags(true, true);
            } else {
                this.settingsStringRenders[var1] = new SettingsStringRender(this.optionsList[var1], 0, this, (String[]) null, false, this.micro, this.parentGameMenu, true);
            }

            this.currentGameMenu.addMenuElement(this.settingsStringRenders[var1]);
        }

    }

    // $FF: renamed from: if (e) void
    public void setParentGameMenu(GameMenu parentGameMenu) {
        this.parentGameMenu = parentGameMenu;
    }

    // $FF: renamed from: a (java.lang.String) void
    public void setText(String text) {
        if (this.useColon) {
            this.text = text;
        } else {
            this.text = text + ":";
        }
    }

    // $FF: renamed from: do () boolean
    public boolean isNotTextRender() {
        return true;
    }

    // $FF: renamed from: a (int) void
    public void menuElemMethod(int var1) {
        if (this.useColon) {
            if (var1 == 1) {
                this.menuManager.processMenu(this);
                return;
            }
        } else {
            switch (var1) {
                case 1:
                    if (this.field_146) {
                        ++this.currentOptionPos;
                        if (this.currentOptionPos > 1) {
                            this.currentOptionPos = 0;
                        }

                        if (this.currentOptionPos == 1) {
                            this.selectedOptionName = "Off";
                        } else {
                            this.selectedOptionName = "On";
                        }

                        this.menuManager.processMenu(this);
                        return;
                    }

                    this.field_147 = true;
                    this.menuManager.processMenu(this);
                    return;
                case 2:
                    if (this.field_146) {
                        if (this.currentOptionPos == 1) {
                            this.currentOptionPos = 0;
                            this.selectedOptionName = "On";
                            this.menuManager.processMenu(this);
                        }

                        return;
                    }

                    ++this.currentOptionPos;
                    if (this.currentOptionPos > this.optionsList.length - 1) {
                        this.currentOptionPos = this.optionsList.length - 1;
                    } else {
                        this.menuManager.processMenu(this);
                    }

                    this.selectCurrentOptionName();
                    return;
                case 3:
                    if (this.field_146) {
                        if (this.currentOptionPos == 0) {
                            this.currentOptionPos = 1;
                            this.selectedOptionName = "Off";
                            this.menuManager.processMenu(this);
                        }

                        return;
                    }

                    --this.currentOptionPos;
                    if (this.currentOptionPos < 0) {
                        this.currentOptionPos = 0;
                    } else {
                        this.selectCurrentOptionName();
                        this.menuManager.processMenu(this);
                    }

                    this.selectCurrentOptionName();
            }
        }

    }

    // $FF: renamed from: j () void
    private void selectCurrentOptionName() {
        this.selectedOptionName = this.optionsList[this.currentOptionPos];
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics, int, int) void
    public void render(Graphics graphics, int y, int x) {
        if (this.useColon) {
            if (!this.hasSprite) {
                graphics.drawString(this.text, x, y, 20);
            } else {
                graphics.drawString(this.text, x + GameCanvas.spriteSizeX[8] + 3, y, 20);
                if (this.isDrawSprite8) {
                    this.micro.gameCanvas.drawSprite(graphics, 8, x, y - GameCanvas.spriteSizeY[8] / 2 + graphics.getFont().getHeight() / 2);
                } else {
                    this.micro.gameCanvas.drawSprite(graphics, 9, x, y - GameCanvas.spriteSizeY[9] / 2 + graphics.getFont().getHeight() / 2);
                }
            }
        } else {
            graphics.drawString(this.text, x, y, 20);
            int shiftedX = x + graphics.getFont().stringWidth(this.text);
            if (this.currentOptionPos > this.maxAvailableOption && !this.field_146) {
                this.micro.gameCanvas.drawSprite(graphics, 8, shiftedX + 1, y - GameCanvas.spriteSizeY[8] / 2 + graphics.getFont().getHeight() / 2);
                shiftedX += GameCanvas.spriteSizeX[9] + 1;
            }

            shiftedX += 2;
            graphics.drawString(this.selectedOptionName, shiftedX, y, 20);
        }
    }

    // $FF: renamed from: do (int) void
    public void setAvailableOptions(int maxAvailableOption) {
        this.maxAvailableOption = maxAvailableOption;
        if (this.maxAvailableOption > this.optionsList.length - 1) {
            this.maxAvailableOption = this.optionsList.length - 1;
        }

        if (this.currentGameMenu != null) {
            for (int i = 0; i < this.settingsStringRenders.length; ++i) {
                if (i > maxAvailableOption) {
                    this.settingsStringRenders[i].setFlags(true, true);
                } else {
                    this.settingsStringRenders[i].setFlags(false, false);
                }
            }
        }

    }

    // $FF: renamed from: null () int
    public int getMaxAvailableOptionPos() {
        return this.maxAvailableOption;
    }

    // $FF: renamed from: else () int
    public int getMaxOptionPos() {
        return this.optionsList.length - 1;
    }

    // $FF: renamed from: case () java.lang.String[]
    public String[] getOptionsList() {
        return this.optionsList;
    }

    // $FF: renamed from: for (int) void
    public void setCurentOptionPos(int pos) {
        this.currentOptionPos = pos;
        if (this.currentOptionPos > this.optionsList.length - 1) {
            this.currentOptionPos = 0;
        }

        if (this.currentOptionPos < 0) {
            this.currentOptionPos = this.optionsList.length - 1;
        }

        this.selectCurrentOptionName();
    }

    // $FF: renamed from: goto () int
    public int getCurrentOptionPos() {
        return this.currentOptionPos;
    }

    // $FF: renamed from: a () e
    public GameMenu getGameMenu() {
        return this.currentGameMenu;
    }

    // $FF: renamed from: a (e, boolean) void
    public void method_1(GameMenu var1, boolean var2) {
    }

    // $FF: renamed from: if () void
    public void saveSmthToRecordStoreAndCloseIt() {
    }

    // $FF: renamed from: a (j) void
    public void processMenu(IGameMenuElement var1) {
        for (int var2 = 0; var2 < this.settingsStringRenders.length; ++var2) {
            if (var1 == this.settingsStringRenders[var2]) {
                this.currentOptionPos = var2;
                this.selectCurrentOptionName();
                break;
            }
        }

        this.menuManager.method_1(this.parentGameMenu, true);
        this.menuManager.processMenu(this);
    }

    // $FF: renamed from: try () g[]
    public SettingsStringRender[] getSettingsStringRenders() {
        return this.settingsStringRenders;
    }

    // $FF: renamed from: char () boolean
    public boolean method_114() {
        if (this.field_147) {
            this.field_147 = false;
            return true;
        } else {
            return this.field_147;
        }
    }
}
