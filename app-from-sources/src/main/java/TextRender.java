import java.util.Vector;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

// $FF: renamed from: h
public class TextRender implements IGameMenuElement {
    // $FF: renamed from: b java.lang.String
    private String text;
    // $FF: renamed from: c javax.microedition.lcdui.Font
    private static Font defaultFont;
    // $FF: renamed from: d javax.microedition.lcdui.Font
    private Font font;
    // $FF: renamed from: e int
    private int dx = 0;
    // $FF: renamed from: f int
    private static int fieldMaxWidth = 100;
    // $FF: renamed from: g int
    private static int fieldMaxHeightUnused = 100;
    // $FF: renamed from: a boolean
    private boolean isDrawSprite;
    // $FF: renamed from: i int
    private int spriteNo;
    // $FF: renamed from: j Micro
    private Micro micro;

    public TextRender(String text, Micro var2) {
        this.text = text;
        this.micro = var2;
        this.isDrawSprite = false;
        this.spriteNo = 0;
        this.font = null;
    }

    // $FF: renamed from: for () int
    public static int getBaselinePosition() {
        return defaultFont.getBaselinePosition();
    }

    // $FF: renamed from: if (javax.microedition.lcdui.Font) void
    public void setFont(Font value) {
        this.font = value;
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Font) void
    public static void setDefaultFont(Font value) {
        defaultFont = value;
    }

    // $FF: renamed from: a (int, int) void
    public static void setMaxArea(int w, int h) {
        fieldMaxWidth = w;
        fieldMaxHeightUnused = h;
    }

    // $FF: renamed from: a (java.lang.String) void
    public void setText(String text) {
        this.text = text;
    }

    // $FF: renamed from: do () boolean
    public boolean isNotTextRender() {
        return false;
    }

    // $FF: renamed from: a (int) void
    public void menuElemMethod(int var1) {
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics, int, int) void
    public void render(Graphics graphics, int y, int x) {
        Font preservedFont = graphics.getFont();
        graphics.setFont(defaultFont);
        if (this.font != null) {
            graphics.setFont(this.font);
        }

        graphics.drawString(this.text, x + this.dx, y, 20);
        if (this.isDrawSprite) {
            this.micro.gameCanvas.drawSprite(graphics, this.spriteNo, x, y);
        }

        graphics.setFont(preservedFont);
    }

    // $FF: renamed from: a (java.lang.String, Micro) h[]
    public static TextRender[] makeMultilineTextRenders(String text, Micro micro) {
        int startPos = 0;
        int endPos = 0;
        byte var4 = 25;

        Vector vector;
        for (vector = new Vector(); endPos < text.length(); startPos = ++endPos - 1) {
            int var6;
            if ((var6 = text.indexOf(" ", startPos)) == -1) {
                endPos = var6 = text.length();
            }

            while (endPos < text.length() && defaultFont.substringWidth(text, startPos, var6 - startPos) < fieldMaxWidth - var4) {
                endPos = var6 + 1;
                if ((var6 = text.indexOf(" ", var6 + 1)) == -1) {
                    if (defaultFont.substringWidth(text, startPos, text.length() - 1 - startPos) <= fieldMaxWidth - var4) {
                        endPos = text.length();
                    }
                    break;
                }
            }

            vector.addElement(new TextRender(text.substring(startPos, endPos), micro));
        }

        TextRender[] result = new TextRender[vector.size()];
        vector.copyInto(result);
        return result;
    }

    // $FF: renamed from: if (int) void
    public void setDx(int var1) {
        this.dx = var1;
    }

    // $FF: renamed from: a (boolean, int) void
    public void setDrawSprite(boolean isDrawSprite, int spriteNo) {
        this.isDrawSprite = isDrawSprite;
        this.spriteNo = spriteNo;
    }
}
