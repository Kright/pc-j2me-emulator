import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreNotOpenException;

// $FF: renamed from: a
public class RecordManager {
    // $FF: renamed from: char int
    public static final int unused = 3;
    // $FF: renamed from: c long[][]
    private long[][] recordTimeMs = new long[4][3];
    // 4: league, 100, 175, 225, 350,
    // 3: three best times

    // $FF: renamed from: d byte[][][]
    private byte[][][] recordName = new byte[4][3][3];
    // $FF: renamed from: e javax.microedition.rms.RecordStore
    private RecordStore recordStore = null;
    // $FF: renamed from: f int
    private int packedRecordInfoRecordId = -1;
    // $FF: renamed from: a byte[]
    private byte[] packedRecordInfo = new byte[96];
    // $FF: renamed from: h java.lang.String
    private String str = null;

    // $FF: renamed from: a (int, int) void
    public void method_8(int var1, int var2) {
        this.resetRecordsTime();

        try {
            this.str = "" + var1 + var2;
            this.recordStore = RecordStore.openRecordStore(this.str, true);
        } catch (RecordStoreException var9) {
            return;
        }

        this.packedRecordInfoRecordId = -1;

        RecordEnumeration recordEnum;
        try {
            recordEnum = this.recordStore.enumerateRecords((RecordFilter) null, (RecordComparator) null, false);
        } catch (RecordStoreNotOpenException var8) {
            return;
        }

        if (recordEnum.numRecords() > 0) {
            byte[] var4;
            try {
                var4 = recordEnum.nextRecord();
                recordEnum.reset();
                this.packedRecordInfoRecordId = recordEnum.nextRecordId();
            } catch (RecordStoreNotOpenException var5) {
                return;
            } catch (InvalidRecordIDException var6) {
                return;
            } catch (RecordStoreException var7) {
                return;
            }

            this.loadRecordInfo(var4);
            recordEnum.destroy();
        }
    }

    // $FF: renamed from: a (byte[], int) long
    private long load5BytesAsLong(byte[] var1, int offset) {
        long result = 0L;
        long mult = 1L;

        for (int var7 = offset; var7 < 5 + offset; ++var7) {
            int var8 = (var1[var7] + 256) % 256;
            result += mult * (long) var8;
            mult *= 256L;
        }

        return result;
    }

    // $FF: renamed from: a (byte[], int, long) void
    private void pushLongAs5Bytes(byte[] var1, int var2, long var3) {
        for (int var5 = var2; var5 < 5 + var2; ++var5) {
            var1[var5] = (byte) ((int) (var3 % 256L));
            var3 /= 256L;
        }
    }

    // $FF: renamed from: a (byte[]) void
    private void loadRecordInfo(byte[] var1) {
        int offset = 0;

        int league;
        int pos;
        for (league = 0; league < 4; ++league) {
            for (pos = 0; pos < 3; ++pos) {
                this.recordTimeMs[league][pos] = this.load5BytesAsLong(var1, offset);
                offset += 5;
            }
        }

        for (league = 0; league < 4; ++league) {
            for (pos = 0; pos < 3; ++pos) {
                for (int i = 0; i < 3; ++i) {
                    this.recordName[league][pos][i] = var1[offset++];
                }
            }
        }
    }

    // $FF: renamed from: g (byte[]) void
    private void getLevelInfo(byte[] var1) {
        int shift = 0;

        int league;
        int recordNo;
        for (league = 0; league < 4; ++league) {
            for (recordNo = 0; recordNo < 3; ++recordNo) {
                this.pushLongAs5Bytes(var1, shift, this.recordTimeMs[league][recordNo]);
                shift += 5;
            }
        }

        for (league = 0; league < 4; ++league) {
            for (recordNo = 0; recordNo < 3; ++recordNo) {
                for (int var5 = 0; var5 < 3; ++var5) {
                    var1[shift++] = this.recordName[league][recordNo][var5];
                }
            }
        }
    }

    // $FF: renamed from: h () void
    private void resetRecordsTime() {
        for (int league = 0; league < 4; ++league) {
            for (int pos = 0; pos < 3; ++pos) {
                this.recordTimeMs[league][pos] = 0L;
            }
        }
    }

    // $FF: renamed from: a (int) java.lang.String[]
    public String[] getRecordDescription(int var1) {
        String[] var2 = new String[3];

        for (int var3 = 0; var3 < 3; ++var3) {
            if (this.recordTimeMs[var1][var3] != 0L) {
                int var4 = (int) this.recordTimeMs[var1][var3] / 100;
                int var5 = (int) this.recordTimeMs[var1][var3] % 100;
                var2[var3] = "" + new String(this.recordName[var1][var3]) + " ";
                if (var4 / 60 < 10) {
                    var2[var3] = var2[var3] + " 0" + var4 / 60;
                } else {
                    var2[var3] = var2[var3] + " " + var4 / 60;
                }

                if (var4 % 60 < 10) {
                    var2[var3] = var2[var3] + ":0" + var4 % 60;
                } else {
                    var2[var3] = var2[var3] + ":" + var4 % 60;
                }

                if (var5 < 10) {
                    var2[var3] = var2[var3] + ".0" + var5;
                } else {
                    var2[var3] = var2[var3] + "." + var5;
                }
            } else {
                var2[var3] = null;
            }
        }

        return var2;
    }

    // $FF: renamed from: for () void
    public void writeRecordInfo() {
        this.getLevelInfo(this.packedRecordInfo);
        if (this.packedRecordInfoRecordId == -1) {
            try {
                this.packedRecordInfoRecordId = this.recordStore.addRecord(this.packedRecordInfo, 0, 96);
            } catch (RecordStoreNotOpenException var1) {
            } catch (RecordStoreException var2) {
            }
        } else {
            try {
                this.recordStore.setRecord(this.packedRecordInfoRecordId, this.packedRecordInfo, 0, 96);
            } catch (RecordStoreNotOpenException var3) {
            } catch (RecordStoreException var4) {
            }
        }
    }

    // $FF: renamed from: a (int, long) int
    public int getPosOfNewRecord(int league, long timeMs) {
        for (int i = 0; i < 3; ++i) {
            if (this.recordTimeMs[league][i] > timeMs || this.recordTimeMs[league][i] == 0L) {
                return i;
            }
        }

        return 3;
    }

    // $FF: renamed from: a (int, byte[], long) void
    public void method_17(int league, byte[] values, long timeMs) {
        int newRecordPos;
        if ((newRecordPos = this.getPosOfNewRecord(league, timeMs)) != 3) {
            if (timeMs > 16777000L) {
                timeMs = 16777000L;  // 3 bytes, not five, wtf?
            }

            this.addNewRecord(league, newRecordPos);
            this.recordTimeMs[league][newRecordPos] = timeMs;

            for (int i = 0; i < 3; ++i) {
                this.recordName[league][newRecordPos][i] = values[i];
            }
        }
    }

    // $FF: renamed from: m (int, int) void
    private void addNewRecord(int gameLevel, int position) {
        for (int i = 2; i > position; --i) {
            this.recordTimeMs[gameLevel][i] = this.recordTimeMs[gameLevel][i - 1];
            System.arraycopy(this.recordName[gameLevel][i - 1], 0, this.recordName[gameLevel][i], 0, 3);
        }
    }

    // $FF: renamed from: if () void
    public void deleteRecordStores() {
        String[] names = RecordStore.listRecordStores();

        for (int i = 0; i < names.length; ++i) {
            if (!names[i].equals("GDTRStates")) {
                try {
                    RecordStore var10000 = this.recordStore;
                    RecordStore.deleteRecordStore(names[i]);
                } catch (RecordStoreException var3) {
                }
            }
        }
    }

    // $FF: renamed from: a () void
    public void closeRecordStore() {
        if (this.recordStore != null) {
            try {
                this.recordStore.closeRecordStore();
                return;
            } catch (RecordStoreException var1) {
            }
        }
    }
}
