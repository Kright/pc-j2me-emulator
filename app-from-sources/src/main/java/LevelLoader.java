import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

// $FF: renamed from: f
public class LevelLoader {
    // $FF: renamed from: b int
    public static final int field_114 = 0;
    // $FF: renamed from: for int
    public static final int field_115 = 1;
    // $FF: renamed from: int int
    public static final int field_116 = 2;
    // $FF: renamed from: do int
    public static final int field_117 = 0;
    // $FF: renamed from: if int
    public static final int field_118 = 1;
    // $FF: renamed from: void boolean
    public static boolean isEnabledPerspective = true;
    // $FF: renamed from: case boolean
    public static boolean isEnabledShadows = true;
    // $FF: renamed from: s int[][]
    private int[][] field_121 = (int[][]) null;
    // $FF: renamed from: g l
    public GameLevel gameLevel = null;
    // $FF: renamed from: h int[]
    private int[] field_123 = new int[3];
    // $FF: renamed from: v int[]
    private int[] field_124 = new int[3];
    // $FF: renamed from: null int
    public int field_125 = 0;
    // $FF: renamed from: f int
    public int field_126 = -1;
    // $FF: renamed from: new java.lang.String[][]
    public String[][] levelNames = new String[3][];
    // $FF: renamed from: c int[][]
    private static int[][] levelOffsetInFile = new int[3][];
    // $FF: renamed from: j int
    public int field_129;
    // $FF: renamed from: i int
    public int field_130;
    // $FF: renamed from: long int
    public int field_131;
    // $FF: renamed from: da int
    private int field_132 = 0;
    // $FF: renamed from: ea int
    private static int field_133 = 0;
    // $FF: renamed from: fa int
    private static int field_134 = 0;
    // $FF: renamed from: a int
    private static int field_135 = 0;
    // $FF: renamed from: k int
    private static int field_136 = 0;
    // $FF: renamed from: e int
    public int field_137;
    // $FF: renamed from: d int
    public int field_138;

    public LevelLoader() {
        for (int var1 = 0; var1 < 3; ++var1) {
            this.field_123[var1] = (int) ((long) (GamePhysics.const175_1_half[var1] + 19660 >> 1) * (long) (GamePhysics.const175_1_half[var1] + 19660 >> 1) >> 16);
            this.field_124[var1] = (int) ((long) (GamePhysics.const175_1_half[var1] - 19660 >> 1) * (long) (GamePhysics.const175_1_half[var1] - 19660 >> 1) >> 16);
        }

        try {
            this.loadLevels();
        } catch (IOException var2) {
        }

        this.method_87();
    }

    // $FF: renamed from: a (java.lang.String) java.io.InputStream
    public InputStream getResourceAsStream(String var1) {
        InputStream var2;
        if (var1.charAt(0) == '/') {
            var2 = this.getClass().getResourceAsStream(var1);
        } else {
            var2 = this.getClass().getResourceAsStream("/" + var1);
        }

        return var2;
    }

    // $FF: renamed from: d () void
    private void loadLevels() throws IOException {
        InputStream s = this.getResourceAsStream("levels.mrg");
        DataInputStream dis = new DataInputStream(s);
        byte[] var3 = new byte[40];
        int[] var4 = new int[3];

        for (int league = 0; league < 3; ++league) {
            var4[league] = dis.readInt();
            levelOffsetInFile[league] = new int[var4[league]];
            this.levelNames[league] = new String[var4[league]];

            for (int levelNp = 0; levelNp < var4[league]; ++levelNp) {
                int var7 = dis.readInt();
                levelOffsetInFile[league][levelNp] = var7;

                for (int var8 = 0; var8 < 40; ++var8) {
                    var3[var8] = dis.readByte();
                    if (var3[var8] == 0) {
                        this.levelNames[league][levelNp] = (new String(var3, 0, var8)).replace('_', ' ');
                        break;
                    }
                }
            }
        }

    }

    // $FF: renamed from: a (int, int) java.lang.String
    public String getName(int league, int level) {
        return league < this.levelNames.length && level < this.levelNames[league].length ? this.levelNames[league][level] : "---";
    }

    // $FF: renamed from: if () void
    public void method_87() {
        this.method_88(this.field_125, this.field_126 + 1);
    }

    // $FF: renamed from: do (int, int) int
    public int method_88(int var1, int var2) {
        this.field_125 = var1;
        this.field_126 = var2;
        if (this.field_126 >= this.levelNames[this.field_125].length) {
            this.field_126 = 0;
        }

        this.method_89(this.field_125 + 1, this.field_126 + 1);
        return this.field_126;
    }

    // $FF: renamed from: h (int, int) void
    private void method_89(int var1, int var2) {
        InputStream var4 = this.getResourceAsStream("/levels.mrg");
        DataInputStream dis = new DataInputStream(var4);

        try {
            for (int i = levelOffsetInFile[var1 - 1][var2 - 1]; i > 0; i -= dis.skipBytes(i)) {
            }

            if (this.gameLevel == null) {
                this.gameLevel = new GameLevel();
            }

            this.gameLevel.load(dis);
            dis.close();
            this.method_96(this.gameLevel);
        } catch (IOException var6) {
        }
    }

    // $FF: renamed from: if (int) void
    public void method_90(int var1) {
        this.field_129 = this.gameLevel.startPosX << 1;
        this.field_130 = this.gameLevel.startPosY << 1;
    }

    // $FF: renamed from: do () int
    public int method_91() {
        return this.gameLevel.pointPositions[this.gameLevel.finishFlagPoint][0] << 1;
    }

    // $FF: renamed from: int () int
    public int method_92() {
        return this.gameLevel.pointPositions[this.gameLevel.startFlagPoint][0] << 1;
    }

    // $FF: renamed from: new () int
    public int method_93() {
        return this.gameLevel.startPosX << 1;
    }

    // $FF: renamed from: a () int
    public int method_94() {
        return this.gameLevel.startPosY << 1;
    }

    // $FF: renamed from: a (int) int
    public int method_95(int var1) {
        return this.gameLevel.method_181(var1 >> 1);
    }

    // $FF: renamed from: a (l) void
    public void method_96(GameLevel gameLevel) {
        this.field_131 = Integer.MIN_VALUE;
        this.gameLevel = gameLevel;
        int var2 = this.gameLevel.pointsCount;
        if (this.field_121 == null || this.field_132 < var2) {
            this.field_121 = (int[][]) null;
            System.gc();
            this.field_132 = var2 < 100 ? 100 : var2;
            this.field_121 = new int[this.field_132][2];
        }

        field_133 = 0;
        field_134 = 0;
        field_135 = gameLevel.pointPositions[field_133][0];
        field_136 = gameLevel.pointPositions[field_134][0];

        for (int var3 = 0; var3 < var2; ++var3) {
            int var4 = gameLevel.pointPositions[(var3 + 1) % var2][0] - gameLevel.pointPositions[var3][0];
            int var5 = gameLevel.pointPositions[(var3 + 1) % var2][1] - gameLevel.pointPositions[var3][1];
            if (var3 != 0 && var3 != var2 - 1) {
                this.field_131 = this.field_131 < gameLevel.pointPositions[var3][0] ? gameLevel.pointPositions[var3][0] : this.field_131;
            }

            int var6 = -var5;
            int var8 = GamePhysics.getSmthLikeMaxAbs(var6, var4);
            this.field_121[var3][0] = (int) (((long) var6 << 32) / (long) var8 >> 16);
            this.field_121[var3][1] = (int) (((long) var4 << 32) / (long) var8 >> 16);
            if (this.gameLevel.startFlagPoint == 0 && gameLevel.pointPositions[var3][0] > this.gameLevel.startPosX) {
                this.gameLevel.startFlagPoint = var3 + 1;
            }

            if (this.gameLevel.finishFlagPoint == 0 && gameLevel.pointPositions[var3][0] > this.gameLevel.finishPosX) {
                this.gameLevel.finishFlagPoint = var3;
            }
        }

        field_133 = 0;
        field_134 = 0;
        field_135 = 0;
        field_136 = 0;
    }

    // $FF: renamed from: if (int, int) void
    public void setMinMaxX(int minX, int maxX) {
        this.gameLevel.setMinMaxX(minX, maxX);
    }

    // $FF: renamed from: a (i, int, int) void
    public void renderLevel3D(GameCanvas gameCanvas, int xF16, int yF16) {
        if (gameCanvas != null) {
            gameCanvas.setColor(0, 170, 0);
            xF16 >>= 1;
            yF16 >>= 1;
            this.gameLevel.renderLevel3D(gameCanvas, xF16, yF16);
        }
    }

    // $FF: renamed from: a (i) void
    public void renderTrackNearestLine(GameCanvas canvas) {
        canvas.setColor(0, 255, 0);
        this.gameLevel.renderTrackNearestGreenLine(canvas);
    }

    // $FF: renamed from: a (int, int, int) void
    public void method_100(int var1, int var2, int var3) {
        this.gameLevel.method_184(var1 + 98304 >> 1, var2 - 98304 >> 1, var3 >> 1);
        var2 >>= 1;
        var1 >>= 1;
        field_134 = field_134 < this.gameLevel.pointsCount - 1 ? field_134 : this.gameLevel.pointsCount - 1;
        field_133 = field_133 < 0 ? 0 : field_133;
        if (var2 > field_136) {
            while (field_134 < this.gameLevel.pointsCount - 1 && var2 > this.gameLevel.pointPositions[++field_134][0]) {
            }
        } else if (var1 < field_135) {
            while (field_133 > 0 && var1 < this.gameLevel.pointPositions[--field_133][0]) {
            }
        } else {
            while (field_133 < this.gameLevel.pointsCount && var1 > this.gameLevel.pointPositions[++field_133][0]) {
            }

            if (field_133 > 0) {
                --field_133;
            }

            while (field_134 > 0 && var2 < this.gameLevel.pointPositions[--field_134][0]) {
            }

            field_134 = field_134 + 1 < this.gameLevel.pointsCount - 1 ? field_134 + 1 : this.gameLevel.pointsCount - 1;
        }

        field_135 = this.gameLevel.pointPositions[field_133][0];
        field_136 = this.gameLevel.pointPositions[field_134][0];
    }

    // $FF: renamed from: a (n, int) int
    public int method_101(TimerOrMotoPartOrMenuElem var1, int var2) {
        int var16 = 0;
        byte var17 = 2;
        int var18 = var1.xF16 >> 1;
        int var19 = var1.yF16 >> 1;
        if (isEnabledPerspective) {
            var19 -= 65536;
        }

        int var20 = 0;
        int var21 = 0;

        for (int var22 = field_133; var22 < field_134; ++var22) {
            int var4 = this.gameLevel.pointPositions[var22][0];
            int var5 = this.gameLevel.pointPositions[var22][1];
            int var6 = this.gameLevel.pointPositions[var22 + 1][0];
            int var7;
            if ((var7 = this.gameLevel.pointPositions[var22 + 1][1]) < var5) {
                ;
            }

            if (var18 - this.field_123[var2] <= var6 && var18 + this.field_123[var2] >= var4) {
                int var8 = var4 - var6;
                int var9 = var5 - var7;
                int var10 = (int) ((long) var8 * (long) var8 >> 16) + (int) ((long) var9 * (long) var9 >> 16);
                int var11 = (int) ((long) (var18 - var4) * (long) (-var8) >> 16) + (int) ((long) (var19 - var5) * (long) (-var9) >> 16);
                int var12;
                if ((var10 < 0 ? -var10 : var10) >= 3) {
                    var12 = (int) (((long) var11 << 32) / (long) var10 >> 16);
                } else {
                    var12 = (var11 > 0 ? 1 : -1) * (var10 > 0 ? 1 : -1) * Integer.MAX_VALUE;
                }

                if (var12 < 0) {
                    var12 = 0;
                }

                if (var12 > 65536) {
                    var12 = 65536;
                }

                int var13 = var4 + (int) ((long) var12 * (long) (-var8) >> 16);
                int var14 = var5 + (int) ((long) var12 * (long) (-var9) >> 16);
                var8 = var18 - var13;
                var9 = var19 - var14;
                byte var3;
                long var23;
                if ((var23 = ((long) var8 * (long) var8 >> 16) + ((long) var9 * (long) var9 >> 16)) < (long) this.field_123[var2]) {
                    if (var23 >= (long) this.field_124[var2]) {
                        var3 = 1;
                    } else {
                        var3 = 0;
                    }
                } else {
                    var3 = 2;
                }

                if (var3 == 0 && (int) ((long) this.field_121[var22][0] * (long) var1.field_382 >> 16) + (int) ((long) this.field_121[var22][1] * (long) var1.field_383 >> 16) < 0) {
                    this.field_137 = this.field_121[var22][0];
                    this.field_138 = this.field_121[var22][1];
                    return 0;
                }

                if (var3 == 1 && (int) ((long) this.field_121[var22][0] * (long) var1.field_382 >> 16) + (int) ((long) this.field_121[var22][1] * (long) var1.field_383 >> 16) < 0) {
                    ++var16;
                    var17 = 1;
                    if (var16 == 1) {
                        var20 = this.field_121[var22][0];
                        var21 = this.field_121[var22][1];
                    } else {
                        var20 += this.field_121[var22][0];
                        var21 += this.field_121[var22][1];
                    }
                }
            }
        }

        if (var17 == 1) {
            if ((int) ((long) var20 * (long) var1.field_382 >> 16) + (int) ((long) var21 * (long) var1.field_383 >> 16) >= 0) {
                return 2;
            }

            this.field_137 = var20;
            this.field_138 = var21;
        }

        return var17;
    }
}
