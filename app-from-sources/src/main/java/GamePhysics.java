// $FF: renamed from: b
public class GamePhysics {
    // $FF: renamed from: Y int
    public static int field_7;
    // $FF: renamed from: void int
    public static int field_8;
    // $FF: renamed from: g int
    public static int field_9;
    // $FF: renamed from: f int
    public static int field_10;
    // $FF: renamed from: e int
    public static int field_11;
    // $FF: renamed from: ae int
    public static int motoParam1;
    // $FF: renamed from: ad int
    public static int motoParam2;
    // $FF: renamed from: y int
    public static int field_14;
    // $FF: renamed from: q int
    public static int motoParam10;
    // $FF: renamed from: x int
    public static int field_16;
    // $FF: renamed from: for int[]
    public static int[] const175_1_half = new int[]{114688, 65536, 32768};
    // $FF: renamed from: P int
    public static int motoParam3;
    // $FF: renamed from: j int
    public static int field_19;
    // $FF: renamed from: Q int
    public static int motoParam4;
    // $FF: renamed from: char int
    public static int motoParam5;
    // $FF: renamed from: ab int
    public static int motoParam6;
    // $FF: renamed from: W int
    public static int motoParam7;
    // $FF: renamed from: A int
    public static int motoParam8;
    // $FF: renamed from: long int
    public static int motoParam9;
    // $FF: renamed from: va int
    private int index01 = 0;
    // $FF: renamed from: wa int
    private int index10 = 1;
    // $FF: renamed from: xa int
    private int field_28 = -1;
    // $FF: renamed from: H k[]
    public class_10[] field_29;
    // $FF: renamed from: i n[]
    private TimerOrMotoPartOrMenuElem[] field_30;
    // $FF: renamed from: c int
    private int field_31 = 0;
    // $FF: renamed from: l f
    private LevelLoader levelLoader;
    // $FF: renamed from: E int
    private int field_33 = 0;
    // $FF: renamed from: C int
    private int field_34 = 0;
    // $FF: renamed from: I boolean
    private boolean field_35 = false;
    // $FF: renamed from: m boolean
    private boolean field_36 = false;
    // $FF: renamed from: T int
    private int field_37 = 32768;
    // $FF: renamed from: p int
    private final int field_38 = 3276;
    // $FF: renamed from: k int
    private int field_39 = 0;
    // $FF: renamed from: v boolean
    private boolean isGenerateInputAI = false;
    // $FF: renamed from: b boolean
    public boolean field_41 = false;
    // $FF: renamed from: af boolean
    private boolean field_42 = false;
    // $FF: renamed from: aa n[]

    /**
     * 1 - forward wheel
     * 2 - back wheel
     * 3 - handlebar
     */
    TimerOrMotoPartOrMenuElem[] motoComponents = new TimerOrMotoPartOrMenuElem[6];
    // $FF: renamed from: t int
    private int field_44;
    // $FF: renamed from: z int
    public int field_45;
    // $FF: renamed from: else boolean
    public boolean field_46;
    // $FF: renamed from: U boolean
    public boolean isRenderMotoWithSprites;
    // $FF: renamed from: ag int
    public static final int field_48 = 1;
    // $FF: renamed from: byte int
    public static final int field_49 = 0;
    // $FF: renamed from: ah int
    public static final int field_50 = 1;
    // $FF: renamed from: L int
    public static final int field_51 = 2;
    // $FF: renamed from: u int
    public static final int field_52 = 3;
    // $FF: renamed from: h int
    public static int curentMotoLeague = 0;
    // $FF: renamed from: d boolean
    private boolean isInputAcceleration;
    // $FF: renamed from: F boolean
    private boolean isInputBreak;
    // $FF: renamed from: X boolean
    private boolean isInputBack;
    // $FF: renamed from: w boolean
    private boolean isInputForward;
    // $FF: renamed from: if boolean
    private boolean isInputUp;
    // $FF: renamed from: s boolean
    private boolean isInputDown;
    // $FF: renamed from: O boolean
    private boolean isInputLeft;
    // $FF: renamed from: r boolean
    private boolean isInputRight;
    // $FF: renamed from: a int
    public static final int field_62 = 0;
    // $FF: renamed from: V int
    public static final int field_63 = 1;
    // $FF: renamed from: B int
    public static final int field_64 = 2;
    // $FF: renamed from: ac int
    public static final int field_65 = 3;
    // $FF: renamed from: new int
    public static final int field_66 = 4;
    // $FF: renamed from: Z int
    public static final int field_67 = 5;
    // $FF: renamed from: R boolean
    private boolean field_68;
    // $FF: renamed from: N boolean
    public boolean field_69;
    // $FF: renamed from: do boolean
    private boolean isEnableLookAhead;
    // $FF: renamed from: o int
    private int camShiftX;
    // $FF: renamed from: n int
    private int camShiftY;
    // $FF: renamed from: G int
    private int field_73;
    // $FF: renamed from: K int[][]
    private final int[][] hardcodedArr1;
    // $FF: renamed from: uc int[][]
    private final int[][] hardcodedArr2;
    // $FF: renamed from: S int[][]
    private final int[][] hardcodedArr3;
    // $FF: renamed from: wc int[][]
    private final int[][] hardcodedArr4;
    // $FF: renamed from: D int[][]
    private final int[][] hardcodedArr5;
    // $FF: renamed from: M int[][]
    private final int[][] hardcodedArr6;
    // $FF: renamed from: J int[][]
    private int[][] field_80;

    public GamePhysics(LevelLoader levelLoader) {
        for (int var2 = 0; var2 < 6; ++var2) {
            this.motoComponents[var2] = new TimerOrMotoPartOrMenuElem();
        }

        this.field_44 = 0;
        this.field_45 = 0;
        this.field_46 = false;
        this.isRenderMotoWithSprites = false;
        this.isInputAcceleration = false;
        this.isInputBreak = false;
        this.isInputBack = false;
        this.isInputForward = false;
        this.isInputUp = false;
        this.isInputDown = false;
        this.isInputLeft = false;
        this.isInputRight = false;
        this.field_68 = false;
        this.field_69 = false;
        this.isEnableLookAhead = true;
        this.camShiftX = 0;
        this.camShiftY = 0;
        this.field_73 = 655360;
        this.hardcodedArr1 = new int[][]{{183500, -52428}, {262144, -163840}, {406323, -65536}, {445644, -39321}, {235929, 39321}, {16384, -144179}, {13107, -78643}, {288358, 81920}};
        this.hardcodedArr2 = new int[][]{{190054, -111411}, {308019, -235929}, {334233, -114688}, {393216, -58982}, {262144, 98304}, {65536, -124518}, {13107, -78643}, {288358, 81920}};
        this.hardcodedArr3 = new int[][]{{157286, 13107}, {294912, -13107}, {367001, 91750}, {406323, 190054}, {347340, 72089}, {39321, -98304}, {13107, -52428}, {294912, 81920}};
        this.hardcodedArr4 = new int[][]{{183500, -39321}, {262144, -131072}, {393216, -65536}, {458752, -39321}, {294912, 6553}, {16384, -144179}, {13107, -78643}, {288358, 85196}};
        this.hardcodedArr5 = new int[][]{{190054, -91750}, {255590, -235929}, {334233, -114688}, {393216, -42598}, {301465, 6553}, {65536, -78643}, {13107, -78643}, {288358, 85196}};
        this.hardcodedArr6 = new int[][]{{157286, 13107}, {294912, -13107}, {367001, 104857}, {406323, 176947}, {347340, 72089}, {39321, -98304}, {13107, -52428}, {288358, 85196}};
        this.field_80 = new int[][]{{45875}, {32768}, {52428}};
        this.levelLoader = levelLoader;
        this.resetSmth(true);
        this.isGenerateInputAI = false;
        this.method_53();
        this.field_35 = false;
    }

    // $FF: renamed from: byte () int
    public int method_21() {
        if (this.field_46 && this.isRenderMotoWithSprites) {
            return 3;
        } else if (this.isRenderMotoWithSprites) {
            return 1;
        } else {
            return this.field_46 ? 2 : 0;
        }
    }

    // $FF: renamed from: do (int) void
    public void method_22(int var1) {
        this.field_46 = false;
        this.isRenderMotoWithSprites = false;
        if ((var1 & 2) != 0) {
            this.field_46 = true;
        }

        if ((var1 & 1) != 0) {
            this.isRenderMotoWithSprites = true;
        }
    }

    // $FF: renamed from: byte (int) void
    public void setMode(int mode) {
        this.field_45 = mode;
        switch (mode) {
            case 1:
            default:
                field_7 = 1310;
                field_8 = 1638400;
                this.setMotoLeague(1);
                this.resetSmth(true);
        }
    }

    // $FF: renamed from: int (int) void
    public void setMotoLeague(int league) {
        curentMotoLeague = league;
        field_9 = 45875;
        field_10 = 13107;
        field_11 = 39321;
        field_14 = 1310720;
        field_16 = 262144;
        field_19 = 6553;
        switch (league) {
            case 0:
            default:
                motoParam1 = 19660;
                motoParam2 = 19660;
                motoParam3 = 1114112;
                motoParam4 = 52428800;
                motoParam5 = 3276800;
                motoParam6 = 327;
                motoParam7 = 0;
                motoParam8 = 32768;
                motoParam9 = 327680;
                motoParam10 = 19660800;
                break;
            case 1:
                motoParam1 = 32768;
                motoParam2 = 32768;
                motoParam3 = 1114112;
                motoParam4 = 65536000;
                motoParam5 = 3276800;
                motoParam6 = 6553;
                motoParam7 = 26214;
                motoParam8 = 26214;
                motoParam9 = 327680;
                motoParam10 = 19660800;
                break;
            case 2:
                motoParam1 = 32768;
                motoParam2 = 32768;
                motoParam3 = 1310720;
                motoParam4 = 75366400;
                motoParam5 = 3473408;
                motoParam6 = 6553;
                motoParam7 = 26214;
                motoParam8 = 39321;
                motoParam9 = 327680;
                motoParam10 = 21626880;
                break;
            case 3:
                motoParam1 = 32768;
                motoParam2 = 32768;
                motoParam3 = 1441792;
                motoParam4 = 78643200;
                motoParam5 = 3538944;
                motoParam6 = 6553;
                motoParam7 = 26214;
                motoParam8 = 65536;
                motoParam9 = 1310720;
                motoParam10 = 21626880;
        }

        this.resetSmth(true);
    }

    // $FF: renamed from: do (boolean) void
    public void resetSmth(boolean unused) {
        this.field_44 = 0;
        this.method_27(this.levelLoader.method_93(), this.levelLoader.method_94());
        this.field_31 = 0;
        this.field_39 = 0;
        this.field_35 = false;
        this.field_36 = false;
        this.field_68 = false;
        this.field_69 = false;
        this.isGenerateInputAI = false;
        this.field_41 = false;
        this.field_42 = false;
        this.levelLoader.gameLevel.method_183(this.field_29[2].motoComponents[5].xF16 + 98304 - const175_1_half[0], this.field_29[1].motoComponents[5].xF16 - 98304 + const175_1_half[0]);
    }

    // $FF: renamed from: a (boolean) void
    public void method_26(boolean var1) {
        int var2 = (var1 ? 65536 : -65536) << 1;

        for (int var3 = 0; var3 < 6; ++var3) {
            for (int var4 = 0; var4 < 6; ++var4) {
                TimerOrMotoPartOrMenuElem[] var10000 = this.field_29[var3].motoComponents;
                var10000[var4].yF16 += var2;
            }
        }
    }

    // $FF: renamed from: i (int, int) void
    private void method_27(int var1, int var2) {
        if (this.field_29 == null) {
            this.field_29 = new class_10[6];
        }

        if (this.field_30 == null) {
            this.field_30 = new TimerOrMotoPartOrMenuElem[10];
        }

        int var4 = 0;
        byte var5 = 0;
        int var6 = 0;
        int var7 = 0;

        int i;
        for (i = 0; i < 6; ++i) {
            short var8 = 0;
            switch (i) {
                case 0:
                    var5 = 1;
                    var4 = 360448;
                    var6 = 0;
                    var7 = 0;
                    break;
                case 1:
                    var5 = 0;
                    var4 = 98304;
                    var6 = 229376;
                    var7 = 0;
                    break;
                case 2:
                    var5 = 0;
                    var4 = 360448;
                    var6 = -229376;
                    var7 = 0;
                    var8 = 21626;
                    break;
                case 3:
                    var5 = 1;
                    var4 = 229376;
                    var6 = 131072;
                    var7 = 196608;
                    break;
                case 4:
                    var5 = 1;
                    var4 = 229376;
                    var6 = -131072;
                    var7 = 196608;
                    break;
                case 5:
                    var5 = 2;
                    var4 = 294912;
                    var6 = 0;
                    var7 = 327680;
            }

            if (this.field_29[i] == null) {
                this.field_29[i] = new class_10();
            }

            this.field_29[i].reset();
            this.field_29[i].field_257 = const175_1_half[var5];
            this.field_29[i].field_258 = var5;
            this.field_29[i].field_259 = (int) ((long) ((int) (281474976710656L / (long) var4 >> 16)) * (long) field_14 >> 16);
            this.field_29[i].motoComponents[this.index01].xF16 = var1 + var6;
            this.field_29[i].motoComponents[this.index01].yF16 = var2 + var7;
            this.field_29[i].motoComponents[5].xF16 = var1 + var6;
            this.field_29[i].motoComponents[5].yF16 = var2 + var7;
            this.field_29[i].field_260 = var8;
        }

        for (i = 0; i < 10; ++i) {
            if (this.field_30[i] == null) {
                this.field_30[i] = new TimerOrMotoPartOrMenuElem();
            }

            this.field_30[i].setToZeros();
            this.field_30[i].xF16 = motoParam10;
            this.field_30[i].angleF16 = field_16;
        }

        this.field_30[0].yF16 = 229376;
        this.field_30[1].yF16 = 229376;
        this.field_30[2].yF16 = 236293;
        this.field_30[3].yF16 = 236293;
        this.field_30[4].yF16 = 262144;
        this.field_30[5].yF16 = 219814;
        this.field_30[6].yF16 = 219814;
        this.field_30[7].yF16 = 185363;
        this.field_30[8].yF16 = 185363;
        this.field_30[9].yF16 = 327680;
        this.field_30[5].angleF16 = (int) ((long) field_16 * 45875L >> 16);
        this.field_30[6].xF16 = (int) (6553L * (long) motoParam10 >> 16);
        this.field_30[5].xF16 = (int) (6553L * (long) motoParam10 >> 16);
        this.field_30[9].xF16 = (int) (72089L * (long) motoParam10 >> 16);
        this.field_30[8].xF16 = (int) (72089L * (long) motoParam10 >> 16);
        this.field_30[7].xF16 = (int) (72089L * (long) motoParam10 >> 16);
    }

    // $FF: renamed from: if (int, int) void
    public void setRenderMinMaxX(int minX, int maxX) {
        this.levelLoader.setMinMaxX(minX, maxX);
    }

    // $FF: renamed from: null () void
    public void processPointerReleased() {
        this.isInputUp = this.isInputDown = this.isInputRight = this.isInputLeft = false;
    }

    // $FF: renamed from: a (int, int) void
    public void method_30(int var1, int var2) {
        if (!this.isGenerateInputAI) {
            this.isInputUp = this.isInputDown = this.isInputRight = this.isInputLeft = false;
            if (var1 > 0) {
                this.isInputUp = true;
            } else if (var1 < 0) {
                this.isInputDown = true;
            }

            if (var2 > 0) {
                this.isInputRight = true;
                return;
            }

            if (var2 < 0) {
                this.isInputLeft = true;
            }
        }

    }

    // $FF: renamed from: case () void
    public synchronized void enableGenerateInputAI() {
        this.resetSmth(true);
        this.isGenerateInputAI = true;
    }

    // $FF: renamed from: a () void
    public synchronized void disableGenerateInputAI() {
        this.isGenerateInputAI = false;
    }

    // $FF: renamed from: goto () boolean
    public boolean isGenerateInputAI() {
        return this.isGenerateInputAI;
    }

    // $FF: renamed from: p () void
    private void setInputFromAI() {
        int var1 = this.field_29[1].motoComponents[this.index01].xF16 - this.field_29[2].motoComponents[this.index01].xF16;
        int var2 = this.field_29[1].motoComponents[this.index01].yF16 - this.field_29[2].motoComponents[this.index01].yF16;
        int var3 = getSmthLikeMaxAbs(var1, var2);
        int var10000 = (int) (((long) var1 << 32) / (long) var3 >> 16);
        var2 = (int) (((long) var2 << 32) / (long) var3 >> 16);
        this.isInputBreak = false;
        if (var2 < 0) {
            this.isInputBack = true;
            this.isInputForward = false;
        } else if (var2 > 0) {
            this.isInputForward = true;
            this.isInputBack = false;
        }

        boolean var4;
        if ((!(var4 = (this.field_29[2].motoComponents[this.index01].yF16 - this.field_29[0].motoComponents[this.index01].yF16 > 0 ? 1 : -1) * (this.field_29[2].motoComponents[this.index01].field_382 - this.field_29[0].motoComponents[this.index01].field_382 > 0 ? 1 : -1) > 0) || !this.isInputForward) && (var4 || !this.isInputBack)) {
            this.isInputAcceleration = false;
        } else {
            this.isInputAcceleration = true;
        }
    }

    // $FF: renamed from: q () void
    private void method_35() {
        if (!this.field_35) {
            int var1 = this.field_29[1].motoComponents[this.index01].xF16 - this.field_29[2].motoComponents[this.index01].xF16;
            int var2 = this.field_29[1].motoComponents[this.index01].yF16 - this.field_29[2].motoComponents[this.index01].yF16;
            int var3 = getSmthLikeMaxAbs(var1, var2);
            var1 = (int) (((long) var1 << 32) / (long) var3 >> 16);
            var2 = (int) (((long) var2 << 32) / (long) var3 >> 16);
            if (this.isInputAcceleration && this.field_31 >= -motoParam4) {
                this.field_31 -= motoParam5;
            }

            if (this.isInputBreak) {
                this.field_31 = 0;
                this.field_29[1].motoComponents[this.index01].field_384 = (int) ((long) this.field_29[1].motoComponents[this.index01].field_384 * (long) (65536 - motoParam6) >> 16);
                this.field_29[2].motoComponents[this.index01].field_384 = (int) ((long) this.field_29[2].motoComponents[this.index01].field_384 * (long) (65536 - motoParam6) >> 16);
                if (this.field_29[1].motoComponents[this.index01].field_384 < 6553) {
                    this.field_29[1].motoComponents[this.index01].field_384 = 0;
                }

                if (this.field_29[2].motoComponents[this.index01].field_384 < 6553) {
                    this.field_29[2].motoComponents[this.index01].field_384 = 0;
                }
            }

            this.field_29[0].field_259 = (int) (11915L * (long) field_14 >> 16);
            this.field_29[0].field_259 = (int) (11915L * (long) field_14 >> 16);
            this.field_29[4].field_259 = (int) (18724L * (long) field_14 >> 16);
            this.field_29[3].field_259 = (int) (18724L * (long) field_14 >> 16);
            this.field_29[1].field_259 = (int) (43690L * (long) field_14 >> 16);
            this.field_29[2].field_259 = (int) (11915L * (long) field_14 >> 16);
            this.field_29[5].field_259 = (int) (14563L * (long) field_14 >> 16);
            if (this.isInputBack) {
                this.field_29[0].field_259 = (int) (18724L * (long) field_14 >> 16);
                this.field_29[4].field_259 = (int) (14563L * (long) field_14 >> 16);
                this.field_29[3].field_259 = (int) (18724L * (long) field_14 >> 16);
                this.field_29[1].field_259 = (int) (43690L * (long) field_14 >> 16);
                this.field_29[2].field_259 = (int) (10082L * (long) field_14 >> 16);
            } else if (this.isInputForward) {
                this.field_29[0].field_259 = (int) (18724L * (long) field_14 >> 16);
                this.field_29[4].field_259 = (int) (18724L * (long) field_14 >> 16);
                this.field_29[3].field_259 = (int) (14563L * (long) field_14 >> 16);
                this.field_29[1].field_259 = (int) (26214L * (long) field_14 >> 16);
                this.field_29[2].field_259 = (int) (11915L * (long) field_14 >> 16);
            }

            if (this.isInputBack || this.isInputForward) {
                int var4 = -var2;
                TimerOrMotoPartOrMenuElem var10000;
                int var6;
                int var7;
                int var8;
                int var9;
                int var10;
                int var11;
                if (this.isInputBack && this.field_39 > -motoParam9) {
                    var6 = 65536;
                    if (this.field_39 < 0) {
                        var6 = (int) (((long) (motoParam9 - (this.field_39 < 0 ? -this.field_39 : this.field_39)) << 32) / (long) motoParam9 >> 16);
                    }

                    var7 = (int) ((long) motoParam8 * (long) var6 >> 16);
                    var8 = (int) ((long) var4 * (long) var7 >> 16);
                    var9 = (int) ((long) var1 * (long) var7 >> 16);
                    var10 = (int) ((long) var1 * (long) var7 >> 16);
                    var11 = (int) ((long) var2 * (long) var7 >> 16);
                    if (this.field_37 > 32768) {
                        this.field_37 = this.field_37 - 1638 < 0 ? 0 : this.field_37 - 1638;
                    } else {
                        this.field_37 = this.field_37 - 3276 < 0 ? 0 : this.field_37 - 3276;
                    }

                    var10000 = this.field_29[4].motoComponents[this.index01];
                    var10000.field_382 -= var8;
                    var10000 = this.field_29[4].motoComponents[this.index01];
                    var10000.field_383 -= var9;
                    var10000 = this.field_29[3].motoComponents[this.index01];
                    var10000.field_382 += var8;
                    var10000 = this.field_29[3].motoComponents[this.index01];
                    var10000.field_383 += var9;
                    var10000 = this.field_29[5].motoComponents[this.index01];
                    var10000.field_382 -= var10;
                    var10000 = this.field_29[5].motoComponents[this.index01];
                    var10000.field_383 -= var11;
                }

                if (this.isInputForward && this.field_39 < motoParam9) {
                    var6 = 65536;
                    if (this.field_39 > 0) {
                        var6 = (int) (((long) (motoParam9 - this.field_39) << 32) / (long) motoParam9 >> 16);
                    }

                    var7 = (int) ((long) motoParam8 * (long) var6 >> 16);
                    var8 = (int) ((long) var4 * (long) var7 >> 16);
                    var9 = (int) ((long) var1 * (long) var7 >> 16);
                    var10 = (int) ((long) var1 * (long) var7 >> 16);
                    var11 = (int) ((long) var2 * (long) var7 >> 16);
                    if (this.field_37 > 32768) {
                        this.field_37 = this.field_37 + 1638 < 65536 ? this.field_37 + 1638 : 65536;
                    } else {
                        this.field_37 = this.field_37 + 3276 < 65536 ? this.field_37 + 3276 : 65536;
                    }

                    var10000 = this.field_29[4].motoComponents[this.index01];
                    var10000.field_382 += var8;
                    var10000 = this.field_29[4].motoComponents[this.index01];
                    var10000.field_383 += var9;
                    var10000 = this.field_29[3].motoComponents[this.index01];
                    var10000.field_382 -= var8;
                    var10000 = this.field_29[3].motoComponents[this.index01];
                    var10000.field_383 -= var9;
                    var10000 = this.field_29[5].motoComponents[this.index01];
                    var10000.field_382 += var10;
                    var10000 = this.field_29[5].motoComponents[this.index01];
                    var10000.field_383 += var11;
                }

                return;
            }

            if (this.field_37 < 26214) {
                this.field_37 += 3276;
                return;
            }

            if (this.field_37 > 39321) {
                this.field_37 -= 3276;
                return;
            }

            this.field_37 = 32768;
        }

    }

    // $FF: renamed from: do () int
    public synchronized int updatePhysics() {
        this.isInputAcceleration = this.isInputUp;
        this.isInputBreak = this.isInputDown;
        this.isInputBack = this.isInputLeft;
        this.isInputForward = this.isInputRight;
        if (this.isGenerateInputAI) {
            this.setInputFromAI();
        }

        GameCanvas.method_151();
        this.method_35();
        int var1;
        if ((var1 = this.method_39(field_7)) != 5 && !this.field_36) {
            if (this.field_35) {
                return 3;
            } else if (this.isTrackStarted()) {
                this.field_69 = false;
                return 4;
            } else {
                return var1;
            }
        } else {
            return 5;
        }
    }

    // $FF: renamed from: new () boolean
    public boolean isTrackStarted() {
        return this.field_29[1].motoComponents[this.index01].xF16 < this.levelLoader.method_92();
    }

    // $FF: renamed from: long () boolean
    public boolean method_38() {
        return this.field_29[1].motoComponents[this.index10].xF16 > this.levelLoader.method_91() || this.field_29[2].motoComponents[this.index10].xF16 > this.levelLoader.method_91();
    }

    // $FF: renamed from: u (int) int
    private int method_39(int var1) {
        boolean var2 = this.field_68;
        int var3 = 0;
        int var4 = var1;

        label77:
        do {
            int var5;
            while (var3 < var1) {
                this.method_45(var4 - var3);
                if (!var2 && this.method_38()) {
                    var5 = 3;
                } else {
                    var5 = this.method_46(this.index10);
                }

                if (!var2 && this.field_68) {
                    if (var5 != 3) {
                        return 2;
                    }

                    return 1;
                }

                if (var5 == 0) {
                    continue label77;
                }

                if (var5 == 3) {
                    this.field_68 = true;
                    var4 = var3 + var4 >> 1;
                } else {
                    int var6;
                    if (var5 == 1) {
                        do {
                            this.method_47(this.index10);
                            if ((var6 = this.method_46(this.index10)) == 0) {
                                return 5;
                            }
                        } while (var6 != 2);
                    }

                    var3 = var4;
                    var4 = var1;
                    this.index01 = this.index01 == 1 ? 0 : 1;
                    this.index10 = this.index10 == 1 ? 0 : 1;
                }
            }

            if ((var5 = (int) ((long) (this.field_29[1].motoComponents[this.index01].xF16 - this.field_29[2].motoComponents[this.index01].xF16) * (long) (this.field_29[1].motoComponents[this.index01].xF16 - this.field_29[2].motoComponents[this.index01].xF16) >> 16) + (int) ((long) (this.field_29[1].motoComponents[this.index01].yF16 - this.field_29[2].motoComponents[this.index01].yF16) * (long) (this.field_29[1].motoComponents[this.index01].yF16 - this.field_29[2].motoComponents[this.index01].yF16) >> 16)) < 983040) {
                this.field_35 = true;
            }

            if (var5 > 4587520) {
                this.field_35 = true;
            }

            return 0;
        } while (((var4 = var3 + var4 >> 1) - var3 < 0 ? -(var4 - var3) : var4 - var3) >= 65);

        return 5;
    }

    // $FF: renamed from: a (int) void
    private void method_40(int var1) {
        TimerOrMotoPartOrMenuElem var3;
        int var4;
        for (var4 = 0; var4 < 6; ++var4) {
            class_10 var2;
            (var3 = (var2 = this.field_29[var4]).motoComponents[var1]).field_385 = 0;
            var3.field_386 = 0;
            var3.field_387 = 0;
            var3.field_386 -= (int) (((long) field_8 << 32) / (long) var2.field_259 >> 16);
        }

        if (!this.field_35) {
            this.method_42(this.field_29[0], this.field_30[1], this.field_29[2], var1, 65536);
            this.method_42(this.field_29[0], this.field_30[0], this.field_29[1], var1, 65536);
            this.method_42(this.field_29[2], this.field_30[6], this.field_29[4], var1, 131072);
            this.method_42(this.field_29[1], this.field_30[5], this.field_29[3], var1, 131072);
        }

        this.method_42(this.field_29[0], this.field_30[2], this.field_29[3], var1, 65536);
        this.method_42(this.field_29[0], this.field_30[3], this.field_29[4], var1, 65536);
        this.method_42(this.field_29[3], this.field_30[4], this.field_29[4], var1, 65536);
        this.method_42(this.field_29[5], this.field_30[8], this.field_29[3], var1, 65536);
        this.method_42(this.field_29[5], this.field_30[7], this.field_29[4], var1, 65536);
        this.method_42(this.field_29[5], this.field_30[9], this.field_29[0], var1, 65536);
        var3 = this.field_29[2].motoComponents[var1];
        this.field_31 = (int) ((long) this.field_31 * (long) (65536 - field_19) >> 16);
        var3.field_387 = this.field_31;
        if (var3.field_384 > motoParam3) {
            var3.field_384 = motoParam3;
        }

        if (var3.field_384 < -motoParam3) {
            var3.field_384 = -motoParam3;
        }

        var4 = 0;
        int var5 = 0;

        int var6;
        for (var6 = 0; var6 < 6; ++var6) {
            var4 += this.field_29[var6].motoComponents[var1].field_382;
            var5 += this.field_29[var6].motoComponents[var1].field_383;
        }

        var4 = (int) (((long) var4 << 32) / 393216L >> 16);
        var5 = (int) (((long) var5 << 32) / 393216L >> 16);
        int var10 = 0;

        int var11;
        for (var11 = 0; var11 < 6; ++var11) {
            var6 = this.field_29[var11].motoComponents[var1].field_382 - var4;
            int var7 = this.field_29[var11].motoComponents[var1].field_383 - var5;
            if ((var10 = getSmthLikeMaxAbs(var6, var7)) > 1966080) {
                int var8 = (int) (((long) var6 << 32) / (long) var10 >> 16);
                int var9 = (int) (((long) var7 << 32) / (long) var10 >> 16);
                TimerOrMotoPartOrMenuElem[] var10000 = this.field_29[var11].motoComponents;
                var10000[var1].field_382 -= var8;
                var10000 = this.field_29[var11].motoComponents;
                var10000[var1].field_383 -= var9;
            }
        }

        var11 = this.field_29[2].motoComponents[var1].yF16 - this.field_29[0].motoComponents[var1].yF16 >= 0 ? 1 : -1;
        int var12 = this.field_29[2].motoComponents[var1].field_382 - this.field_29[0].motoComponents[var1].field_382 >= 0 ? 1 : -1;
        if (var11 * var12 > 0) {
            this.field_39 = var10;
        } else {
            this.field_39 = -var10;
        }
    }

    // $FF: renamed from: do (int, int) int
    public static int getSmthLikeMaxAbs(int xF16, int yF16) {
        int absXF16 = xF16 < 0 ? -xF16 : xF16;
        int absYF16;
        int maxAbs;
        int minAbs;
        if ((absYF16 = yF16 < 0 ? -yF16 : yF16) >= absXF16) {
            maxAbs = absYF16;
            minAbs = absXF16;
        } else {
            maxAbs = absXF16;
            minAbs = absYF16;
        }

        return (int) (64448L * (long) maxAbs >> 16) + (int) (28224L * (long) minAbs >> 16);
    }

    // $FF: renamed from: a (k, n, k, int, int) void
    private void method_42(class_10 var1, TimerOrMotoPartOrMenuElem var2, class_10 var3, int var4, int var5) {
        TimerOrMotoPartOrMenuElem var6 = var1.motoComponents[var4];
        TimerOrMotoPartOrMenuElem var7 = var3.motoComponents[var4];
        int var8 = var6.xF16 - var7.xF16;
        int var9 = var6.yF16 - var7.yF16;
        int var10;
        if (((var10 = getSmthLikeMaxAbs(var8, var9)) < 0 ? -var10 : var10) >= 3) {
            var8 = (int) (((long) var8 << 32) / (long) var10 >> 16);
            var9 = (int) (((long) var9 << 32) / (long) var10 >> 16);
            int var11 = var10 - var2.yF16;
            int var12 = (int) ((long) var8 * (long) ((int) ((long) var11 * (long) var2.xF16 >> 16)) >> 16);
            int var13 = (int) ((long) var9 * (long) ((int) ((long) var11 * (long) var2.xF16 >> 16)) >> 16);
            int var14 = var6.field_382 - var7.field_382;
            int var15 = var6.field_383 - var7.field_383;
            int var16 = (int) ((long) ((int) ((long) var8 * (long) var14 >> 16) + (int) ((long) var9 * (long) var15 >> 16)) * (long) var2.angleF16 >> 16);
            var12 += (int) ((long) var8 * (long) var16 >> 16);
            var13 += (int) ((long) var9 * (long) var16 >> 16);
            var12 = (int) ((long) var12 * (long) var5 >> 16);
            var13 = (int) ((long) var13 * (long) var5 >> 16);
            var6.field_385 -= var12;
            var6.field_386 -= var13;
            var7.field_385 += var12;
            var7.field_386 += var13;
        }

    }

    // $FF: renamed from: a (int, int, int) void
    private void method_43(int var1, int var2, int var3) {
        for (int var7 = 0; var7 < 6; ++var7) {
            TimerOrMotoPartOrMenuElem var4 = this.field_29[var7].motoComponents[var1];
            TimerOrMotoPartOrMenuElem var5;
            (var5 = this.field_29[var7].motoComponents[var2]).xF16 = (int) ((long) var4.field_382 * (long) var3 >> 16);
            var5.yF16 = (int) ((long) var4.field_383 * (long) var3 >> 16);
            int var6 = (int) ((long) var3 * (long) this.field_29[var7].field_259 >> 16);
            var5.field_382 = (int) ((long) var4.field_385 * (long) var6 >> 16);
            var5.field_383 = (int) ((long) var4.field_386 * (long) var6 >> 16);
        }
    }

    // $FF: renamed from: z (int, int, int) void
    private void method_44(int var1, int var2, int var3) {
        for (int var7 = 0; var7 < 6; ++var7) {
            TimerOrMotoPartOrMenuElem var4 = this.field_29[var7].motoComponents[var1];
            TimerOrMotoPartOrMenuElem var5 = this.field_29[var7].motoComponents[var2];
            TimerOrMotoPartOrMenuElem var6 = this.field_29[var7].motoComponents[var3];
            var4.xF16 = var5.xF16 + (var6.xF16 >> 1);
            var4.yF16 = var5.yF16 + (var6.yF16 >> 1);
            var4.field_382 = var5.field_382 + (var6.field_382 >> 1);
            var4.field_383 = var5.field_383 + (var6.field_383 >> 1);
        }
    }

    // $FF: renamed from: aa (int) void
    private void method_45(int var1) {
        this.method_40(this.index01);
        this.method_43(this.index01, 2, var1);
        this.method_44(4, this.index01, 2);
        this.method_40(4);
        this.method_43(4, 3, var1 >> 1);
        this.method_44(4, this.index01, 3);
        this.method_44(this.index10, this.index01, 2);
        this.method_44(this.index10, this.index10, 3);

        for (int var4 = 1; var4 <= 2; ++var4) {
            TimerOrMotoPartOrMenuElem var2 = this.field_29[var4].motoComponents[this.index01];
            TimerOrMotoPartOrMenuElem var3;
            (var3 = this.field_29[var4].motoComponents[this.index10]).angleF16 = var2.angleF16 + (int) ((long) var1 * (long) var2.field_384 >> 16);
            var3.field_384 = var2.field_384 + (int) ((long) var1 * (long) ((int) ((long) this.field_29[var4].field_260 * (long) var2.field_387 >> 16)) >> 16);
        }
    }

    // $FF: renamed from: ba (int) int
    private int method_46(int var1) {
        byte var2 = 2;
        int var4 = (var4 = this.field_29[1].motoComponents[var1].xF16 < this.field_29[2].motoComponents[var1].xF16 ? this.field_29[2].motoComponents[var1].xF16 : this.field_29[1].motoComponents[var1].xF16) < this.field_29[5].motoComponents[var1].xF16 ? this.field_29[5].motoComponents[var1].xF16 : var4;
        int var5 = (var5 = this.field_29[1].motoComponents[var1].xF16 < this.field_29[2].motoComponents[var1].xF16 ? this.field_29[1].motoComponents[var1].xF16 : this.field_29[2].motoComponents[var1].xF16) < this.field_29[5].motoComponents[var1].xF16 ? var5 : this.field_29[5].motoComponents[var1].xF16;
        this.levelLoader.method_100(var5 - const175_1_half[0], var4 + const175_1_half[0], this.field_29[5].motoComponents[var1].yF16);
        int var6 = this.field_29[1].motoComponents[var1].xF16 - this.field_29[2].motoComponents[var1].xF16;
        int var7 = this.field_29[1].motoComponents[var1].yF16 - this.field_29[2].motoComponents[var1].yF16;
        int var8 = getSmthLikeMaxAbs(var6, var7);
        var6 = (int) (((long) var6 << 32) / (long) var8 >> 16);
        int var9 = -((int) (((long) var7 << 32) / (long) var8 >> 16));
        int var10 = var6;

        for (int var11 = 0; var11 < 6; ++var11) {
            if (var11 != 4 && var11 != 3) {
                TimerOrMotoPartOrMenuElem var3 = this.field_29[var11].motoComponents[var1];
                if (var11 == 0) {
                    var3.xF16 += (int) ((long) var9 * 65536L >> 16);
                    var3.yF16 += (int) ((long) var10 * 65536L >> 16);
                }

                int var12 = this.levelLoader.method_101(var3, this.field_29[var11].field_258);
                if (var11 == 0) {
                    var3.xF16 -= (int) ((long) var9 * 65536L >> 16);
                    var3.yF16 -= (int) ((long) var10 * 65536L >> 16);
                }

                this.field_33 = this.levelLoader.field_137;
                this.field_34 = this.levelLoader.field_138;
                if (var11 == 5 && var12 != 2) {
                    this.field_36 = true;
                }

                if (var11 == 1 && var12 != 2) {
                    this.field_69 = true;
                }

                if (var12 == 1) {
                    this.field_28 = var11;
                    var2 = 1;
                } else if (var12 == 0) {
                    this.field_28 = var11;
                    var2 = 0;
                    break;
                }
            }
        }

        return var2;
    }

    // $FF: renamed from: ca (int) void
    private void method_47(int var1) {
        class_10 var2;
        TimerOrMotoPartOrMenuElem var3;
        TimerOrMotoPartOrMenuElem var10000 = var3 = (var2 = this.field_29[this.field_28]).motoComponents[var1];
        var10000.xF16 += (int) ((long) this.field_33 * 3276L >> 16);
        var3.yF16 += (int) ((long) this.field_34 * 3276L >> 16);
        int var4;
        int var5;
        int var6;
        int var7;
        int var8;
        if (this.isInputBreak && (this.field_28 == 2 || this.field_28 == 1) && var3.field_384 < 6553) {
            var4 = field_9 - motoParam7;
            var5 = 13107;
            var6 = 39321;
            var7 = 26214 - motoParam7;
            var8 = 26214 - motoParam7;
        } else {
            var4 = field_9;
            var5 = field_10;
            var6 = field_11;
            var7 = motoParam1;
            var8 = motoParam2;
        }

        int var9 = getSmthLikeMaxAbs(this.field_33, this.field_34);
        this.field_33 = (int) (((long) this.field_33 << 32) / (long) var9 >> 16);
        this.field_34 = (int) (((long) this.field_34 << 32) / (long) var9 >> 16);
        int var10 = var3.field_382;
        int var11 = var3.field_383;
        int var12 = -((int) ((long) var10 * (long) this.field_33 >> 16) + (int) ((long) var11 * (long) this.field_34 >> 16));
        int var13 = -((int) ((long) var10 * (long) (-this.field_34) >> 16) + (int) ((long) var11 * (long) this.field_33 >> 16));
        int var14 = (int) ((long) var4 * (long) var3.field_384 >> 16) - (int) ((long) var5 * (long) ((int) (((long) var13 << 32) / (long) var2.field_257 >> 16)) >> 16);
        int var15 = (int) ((long) var7 * (long) var13 >> 16) - (int) ((long) var6 * (long) ((int) ((long) var3.field_384 * (long) var2.field_257 >> 16)) >> 16);
        int var16 = -((int) ((long) var8 * (long) var12 >> 16));
        int var17 = (int) ((long) (-var15) * (long) (-this.field_34) >> 16);
        int var18 = (int) ((long) (-var15) * (long) this.field_33 >> 16);
        int var19 = (int) ((long) (-var16) * (long) this.field_33 >> 16);
        int var20 = (int) ((long) (-var16) * (long) this.field_34 >> 16);
        var3.field_384 = var14;
        var3.field_382 = var17 + var19;
        var3.field_383 = var18 + var20;
    }

    // $FF: renamed from: if (boolean) void
    public void setEnableLookAhead(boolean value) {
        this.isEnableLookAhead = value;
    }

    // $FF: renamed from: case (int) void
    public void setMinimalScreenWH(int minWH) {
        this.field_73 = (int) (((long) ((int) (655360L * (long) (minWH << 16) >> 16)) << 32) / 8388608L >> 16);
    }

    // $FF: renamed from: else () int
    public int getCamPosX() {
        if (this.isEnableLookAhead) {
            this.camShiftX = (int) (((long) this.motoComponents[0].field_382 << 32) / 1572864L >> 16) + (int) ((long) this.camShiftX * 57344L >> 16);
        } else {
            this.camShiftX = 0;
        }

        // camShiftX = clamp(camShiftX, -field_73, field_73);
        this.camShiftX = this.camShiftX < this.field_73 ? this.camShiftX : this.field_73;
        this.camShiftX = this.camShiftX < -this.field_73 ? -this.field_73 : this.camShiftX;
        return this.motoComponents[0].xF16 + this.camShiftX << 2 >> 16;
    }

    // $FF: renamed from: if () int
    public int getCamPosY() {
        if (this.isEnableLookAhead) {
            this.camShiftY = (int) (((long) this.motoComponents[0].field_383 << 32) / 1572864L >> 16) + (int) ((long) this.camShiftY * 57344L >> 16);
        } else {
            this.camShiftY = 0;
        }

        this.camShiftY = this.camShiftY < this.field_73 ? this.camShiftY : this.field_73;
        this.camShiftY = this.camShiftY < -this.field_73 ? -this.field_73 : this.camShiftY;
        return this.motoComponents[0].yF16 + this.camShiftY << 2 >> 16;
    }

    // $FF: renamed from: try () int
    public int method_52() {
        int var1 = this.motoComponents[1].xF16 < this.motoComponents[2].xF16 ? this.motoComponents[2].xF16 : this.motoComponents[1].xF16;
        return this.field_35 ? this.levelLoader.method_95(this.motoComponents[0].xF16) : this.levelLoader.method_95(var1);
    }

    // $FF: renamed from: char () void
    public void method_53() {
        synchronized (this.field_29) {
            for (int var2 = 0; var2 < 6; ++var2) {
                this.field_29[var2].motoComponents[5].xF16 = this.field_29[var2].motoComponents[this.index01].xF16;
                this.field_29[var2].motoComponents[5].yF16 = this.field_29[var2].motoComponents[this.index01].yF16;
                this.field_29[var2].motoComponents[5].angleF16 = this.field_29[var2].motoComponents[this.index01].angleF16;
            }

            this.field_29[0].motoComponents[5].field_382 = this.field_29[0].motoComponents[this.index01].field_382;
            this.field_29[0].motoComponents[5].field_383 = this.field_29[0].motoComponents[this.index01].field_383;
            this.field_29[2].motoComponents[5].field_384 = this.field_29[2].motoComponents[this.index01].field_384;
        }
    }

    // $FF: renamed from: void () void
    public void setMotoComponents() {
        synchronized (this.field_29) {
            for (int i = 0; i < 6; ++i) {
                this.motoComponents[i].xF16 = this.field_29[i].motoComponents[5].xF16;
                this.motoComponents[i].yF16 = this.field_29[i].motoComponents[5].yF16;
                this.motoComponents[i].angleF16 = this.field_29[i].motoComponents[5].angleF16;
            }

            this.motoComponents[0].field_382 = this.field_29[0].motoComponents[5].field_382;
            this.motoComponents[0].field_383 = this.field_29[0].motoComponents[5].field_383;
            this.motoComponents[2].field_384 = this.field_29[2].motoComponents[5].field_384;
        }
    }

    // $FF: renamed from: a (i, int, int) void
    private void renderEngine(GameCanvas gameCanvas, int var2, int var3) {
        int engineAngle4F16 = MathF16.atan2(this.motoComponents[0].xF16 - this.motoComponents[3].xF16, this.motoComponents[0].yF16 - this.motoComponents[3].yF16);
        int fenderAngle4F16 = MathF16.atan2(this.motoComponents[0].xF16 - this.motoComponents[4].xF16, this.motoComponents[0].yF16 - this.motoComponents[4].yF16);
        int engineXF16 = (this.motoComponents[0].xF16 >> 1) + (this.motoComponents[3].xF16 >> 1);
        int engineYF16 = (this.motoComponents[0].yF16 >> 1) + (this.motoComponents[3].yF16 >> 1);
        int fenderXF16 = (this.motoComponents[0].xF16 >> 1) + (this.motoComponents[4].xF16 >> 1);
        int fenderYF16 = (this.motoComponents[0].yF16 >> 1) + (this.motoComponents[4].yF16 >> 1);
        int var10 = -var3;
        engineXF16 += (int) ((long) var10 * 65536L >> 16) - (int) ((long) var2 * 32768L >> 16);
        engineYF16 += (int) ((long) var2 * 65536L >> 16) - (int) ((long) var3 * 32768L >> 16);
        fenderXF16 += (int) ((long) var10 * 65536L >> 16) - (int) ((long) var2 * 117964L >> 16);
        fenderYF16 += (int) ((long) var2 * 65536L >> 16) - (int) ((long) var3 * 131072L >> 16);
        gameCanvas.renderFender(fenderXF16 << 2 >> 16, fenderYF16 << 2 >> 16, fenderAngle4F16);
        gameCanvas.renderEngine(engineXF16 << 2 >> 16, engineYF16 << 2 >> 16, engineAngle4F16);
    }

    // $FF: renamed from: la (i) void
    private void renderMotoFork(GameCanvas canvas) {
        canvas.setColor(128, 128, 128);
        canvas.drawLineF16(this.motoComponents[3].xF16, this.motoComponents[3].yF16, this.motoComponents[1].xF16, this.motoComponents[1].yF16);
    }

    // $FF: renamed from: a (i) void
    private void renderWheelTires(GameCanvas canvas) {
        byte backWheelIsThin = 1;
        byte forwardWheelIsThin = 1;
        switch (curentMotoLeague) {
            case 1:
                backWheelIsThin = 0;
                break;
            case 2:
            case 3:
                forwardWheelIsThin = 0;
                backWheelIsThin = 0;
        }

        // back wheel
        canvas.drawWheelTires(this.motoComponents[2].xF16 << 2 >> 16, this.motoComponents[2].yF16 << 2 >> 16, backWheelIsThin);
        // forward wheel
        canvas.drawWheelTires(this.motoComponents[1].xF16 << 2 >> 16, this.motoComponents[1].yF16 << 2 >> 16, forwardWheelIsThin);
    }

    // $FF: renamed from: do (i) void
    private void renderWheelSpokes(GameCanvas gameCanvas) {
        int var2;
        int xxxF16 = (int) ((long) (var2 = this.field_29[1].field_257) * 58982L >> 16);
        int yyyF16 = (int) ((long) var2 * 45875L >> 16);
        gameCanvas.setColor(0, 0, 0);
        if (Micro.isInGameMenu) {
            gameCanvas.drawCircle(this.motoComponents[1].xF16 << 2 >> 16, this.motoComponents[1].yF16 << 2 >> 16, var2 + var2 << 2 >> 16);
            gameCanvas.drawCircle(this.motoComponents[1].xF16 << 2 >> 16, this.motoComponents[1].yF16 << 2 >> 16, xxxF16 + xxxF16 << 2 >> 16);
            gameCanvas.drawCircle(this.motoComponents[2].xF16 << 2 >> 16, this.motoComponents[2].yF16 << 2 >> 16, var2 + var2 << 2 >> 16);
            gameCanvas.drawCircle(this.motoComponents[2].xF16 << 2 >> 16, this.motoComponents[2].yF16 << 2 >> 16, yyyF16 + yyyF16 << 2 >> 16);
        }

        byte var6 = 0;
        int angle;
        int cosF16 = MathF16.cosF16(angle = this.motoComponents[1].angleF16);
        int sinF16 = MathF16.sinF16(angle);
        int dxF16 = (int) ((long) cosF16 * (long) xxxF16 >> 16) + (int) ((long) (-sinF16) * (long) var6 >> 16);
        int dyF16 = (int) ((long) sinF16 * (long) xxxF16 >> 16) + (int) ((long) cosF16 * (long) var6 >> 16);
        angle = 82354;
        cosF16 = MathF16.cosF16(82354);
        sinF16 = MathF16.sinF16(angle);

        int var10;
        int i;
        for (i = 0; i < 5; ++i) {
            // forward wheel spokes
            gameCanvas.drawLineF16(this.motoComponents[1].xF16, this.motoComponents[1].yF16, this.motoComponents[1].xF16 + dxF16, this.motoComponents[1].yF16 + dyF16);
            var10 = dxF16;
            dxF16 = (int) ((long) cosF16 * (long) dxF16 >> 16) + (int) ((long) (-sinF16) * (long) dyF16 >> 16);
            dyF16 = (int) ((long) sinF16 * (long) var10 >> 16) + (int) ((long) cosF16 * (long) dyF16 >> 16);
        }

        var6 = 0;
        cosF16 = MathF16.cosF16(angle = this.motoComponents[2].angleF16);
        sinF16 = MathF16.sinF16(angle);
        dxF16 = (int) ((long) cosF16 * (long) xxxF16 >> 16) + (int) ((long) (-sinF16) * (long) var6 >> 16);
        dyF16 = (int) ((long) sinF16 * (long) xxxF16 >> 16) + (int) ((long) cosF16 * (long) var6 >> 16);
        angle = 82354;
        cosF16 = MathF16.cosF16(82354);
        sinF16 = MathF16.sinF16(angle);

        for (i = 0; i < 5; ++i) {
            // back wheel spokes
            gameCanvas.drawLineF16(this.motoComponents[2].xF16, this.motoComponents[2].yF16, this.motoComponents[2].xF16 + dxF16, this.motoComponents[2].yF16 + dyF16);
            var10 = dxF16;
            dxF16 = (int) ((long) cosF16 * (long) dxF16 >> 16) + (int) ((long) (-sinF16) * (long) dyF16 >> 16);
            dyF16 = (int) ((long) sinF16 * (long) var10 >> 16) + (int) ((long) cosF16 * (long) dyF16 >> 16);
        }

        if (curentMotoLeague > 0) {
            gameCanvas.setColor(255, 0, 0);
            if (curentMotoLeague > 2) {
                gameCanvas.setColor(100, 100, 255);
            }

            gameCanvas.drawCircle(this.motoComponents[2].xF16 << 2 >> 16, this.motoComponents[2].yF16 << 2 >> 16, 4);
            gameCanvas.drawCircle(this.motoComponents[1].xF16 << 2 >> 16, this.motoComponents[1].yF16 << 2 >> 16, 4);
        }
    }

    // $FF: renamed from: if (i, int, int, int, int) void
    private void renderSmth(GameCanvas gameCanvas, int var2, int var3, int var4, int var5) {
        byte var6 = 0;
        int var7 = 65536;
        int var8 = this.motoComponents[0].xF16;
        int var9 = this.motoComponents[0].yF16;
        int x6F16 = 0;
        int y6F16 = 0;
        int xF16 = 0;
        int yF16 = 0;
        int var14 = 0;
        int var15 = 0;
        int x2F16 = 0;
        int y2F16 = 0;
        int x3F16 = 0;
        int y3F16 = 0;
        int x4F16 = 0;
        int y4F16 = 0;
        int circleXF16 = 0;
        int circleYF16 = 0;
        int x5F16 = 0;
        int y5F16 = 0;
        int[][] var27 = (int[][]) null;
        int[][] var28 = (int[][]) null;
        int[][] var29 = (int[][]) null;
        if (this.field_46) {
            if (this.field_37 < 32768) {
                var28 = this.hardcodedArr2;
                var29 = this.hardcodedArr1;
                var7 = (int) ((long) this.field_37 * 131072L >> 16);
            } else if (this.field_37 > 32768) {
                var6 = 1;
                var28 = this.hardcodedArr1;
                var29 = this.hardcodedArr3;
                var7 = (int) ((long) (this.field_37 - '耀') * 131072L >> 16);
            } else {
                var27 = this.hardcodedArr1;
            }
        } else if (this.field_37 < 32768) {
            var28 = this.hardcodedArr5;
            var29 = this.hardcodedArr4;
            var7 = (int) ((long) this.field_37 * 131072L >> 16);
        } else if (this.field_37 > 32768) {
            var6 = 1;
            var28 = this.hardcodedArr4;
            var29 = this.hardcodedArr6;
            var7 = (int) ((long) (this.field_37 - '耀') * 131072L >> 16);
        } else {
            var27 = this.hardcodedArr4;
        }

        int var30;
        for (var30 = 0; var30 < this.hardcodedArr1.length; ++var30) {
            int var31;
            int var32;
            if (var28 != null) {
                var32 = (int) ((long) var28[var30][0] * (long) (65536 - var7) >> 16) + (int) ((long) var29[var30][0] * (long) var7 >> 16);
                var31 = (int) ((long) var28[var30][1] * (long) (65536 - var7) >> 16) + (int) ((long) var29[var30][1] * (long) var7 >> 16);
            } else {
                var32 = var27[var30][0];
                var31 = var27[var30][1];
            }

            int xxF16 = var8 + (int) ((long) var4 * (long) var32 >> 16) + (int) ((long) var2 * (long) var31 >> 16);
            int yyF16 = var9 + (int) ((long) var5 * (long) var32 >> 16) + (int) ((long) var3 * (long) var31 >> 16);
            switch (var30) {
                case 0:
                    x2F16 = xxF16;
                    y2F16 = yyF16;
                    break;
                case 1:
                    x3F16 = xxF16;
                    y3F16 = yyF16;
                    break;
                case 2:
                    x4F16 = xxF16;
                    y4F16 = yyF16;
                    break;
                case 3:
                    circleXF16 = xxF16;
                    circleYF16 = yyF16;
                    break;
                case 4:
                    x5F16 = xxF16;
                    y5F16 = yyF16;
                    break;
                case 5:
                    xF16 = xxF16;
                    yF16 = yyF16;
                    break;
                case 6:
                    var14 = xxF16;
                    var15 = yyF16;
                    break;
                case 7:
                    x6F16 = xxF16;
                    y6F16 = yyF16;
            }
        }

        int var26 = (int) ((long) this.field_80[var6][0] * (long) (65536 - var7) >> 16) + (int) ((long) this.field_80[var6 + 1][0] * (long) var7 >> 16);
        if (this.field_46) {
            gameCanvas.renderBodyPart(xF16 << 2, yF16 << 2, x2F16 << 2, y2F16 << 2, 1);
            gameCanvas.renderBodyPart(x2F16 << 2, y2F16 << 2, x3F16 << 2, y3F16 << 2, 1);
            gameCanvas.renderBodyPart(x3F16 << 2, y3F16 << 2, x4F16 << 2, y4F16 << 2, 2, var26);
            gameCanvas.renderBodyPart(x4F16 << 2, y4F16 << 2, x5F16 << 2, y5F16 << 2, 0);
            var30 = MathF16.atan2(var2, var3);
            if (this.field_37 > 32768) {
                var30 += 20588;
            }

            gameCanvas.method_146(circleXF16 << 2 >> 16, circleYF16 << 2 >> 16, var30);
        } else {
            gameCanvas.setColor(0, 0, 0);
            gameCanvas.drawLineF16(xF16, yF16, x2F16, y2F16);
            gameCanvas.drawLineF16(x2F16, y2F16, x3F16, y3F16);
            gameCanvas.setColor(0, 0, 128);
            gameCanvas.drawLineF16(x3F16, y3F16, x4F16, y4F16);
            gameCanvas.drawLineF16(x4F16, y4F16, x5F16, y5F16);
            gameCanvas.drawLineF16(x5F16, y5F16, x6F16, y6F16);
            var30 = 65536;
            gameCanvas.setColor(156, 0, 0);
            gameCanvas.drawCircle(circleXF16 << 2 >> 16, circleYF16 << 2 >> 16, var30 + var30 << 2 >> 16);
        }

        gameCanvas.setColor(0, 0, 0);
        gameCanvas.drawForthSpriteByCenter(x6F16 << 2 >> 16, y6F16 << 2 >> 16);
        gameCanvas.drawForthSpriteByCenter(var14 << 2 >> 16, var15 << 2 >> 16);
    }

    // $FF: renamed from: a (i, int, int, int, int) void
    private void renderMotoAsLines(GameCanvas gameCanvas, int var2, int var3, int var4, int var5) {
        int var7 = this.motoComponents[2].xF16;
        int var8 = this.motoComponents[2].yF16;
        int var9 = var7 + (int) ((long) var4 * (long) '耀' >> 16);
        int var10 = var8 + (int) ((long) var5 * (long) '耀' >> 16);
        int var11 = var7 - (int) ((long) var4 * (long) '耀' >> 16);
        int var12 = var8 - (int) ((long) var5 * (long) '耀' >> 16);
        int var13 = this.motoComponents[0].xF16 + (int) ((long) var2 * 32768L >> 16);
        int var14 = this.motoComponents[0].yF16 + (int) ((long) var3 * 32768L >> 16);
        int var15 = var13 - (int) ((long) var2 * 131072L >> 16);
        int var16 = var14 - (int) ((long) var3 * 131072L >> 16);
        int var17 = var15 + (int) ((long) var4 * 65536L >> 16);
        int var18 = var16 + (int) ((long) var5 * 65536L >> 16);
        int var19 = var15 + (int) ((long) var2 * 49152L >> 16) + (int) ((long) var4 * 49152L >> 16);
        int var20 = var16 + (int) ((long) var3 * 49152L >> 16) + (int) ((long) var5 * 49152L >> 16);
        int var21 = var15 + (int) ((long) var4 * 32768L >> 16);
        int var22 = var16 + (int) ((long) var5 * 32768L >> 16);
        int var23 = this.motoComponents[1].xF16;
        int var24 = this.motoComponents[1].yF16;
        int var25 = this.motoComponents[4].xF16 - (int) ((long) var2 * 49152L >> 16);
        int var26 = this.motoComponents[4].yF16 - (int) ((long) var3 * 49152L >> 16);
        int var27 = var25 - (int) ((long) var4 * 32768L >> 16);
        int var28 = var26 - (int) ((long) var5 * 32768L >> 16);
        int var29 = var25 - (int) ((long) var2 * 131072L >> 16) + (int) ((long) var4 * 16384L >> 16);
        int var30 = var26 - (int) ((long) var3 * 131072L >> 16) + (int) ((long) var5 * 16384L >> 16);
        int var31 = this.motoComponents[3].xF16;
        int var32 = this.motoComponents[3].yF16;
        int var33 = var31 + (int) ((long) var4 * 32768L >> 16);
        int var34 = var32 + (int) ((long) var5 * 32768L >> 16);
        int var35 = var31 + (int) ((long) var4 * 114688L >> 16) - (int) ((long) var2 * 32768L >> 16);
        int var36 = var32 + (int) ((long) var5 * 114688L >> 16) - (int) ((long) var3 * 32768L >> 16);
        gameCanvas.setColor(50, 50, 50);
        gameCanvas.drawCircle(var21 << 2 >> 16, var22 << 2 >> 16, '耀' + '耀' << 2 >> 16);
        if (!this.field_35) {
            gameCanvas.drawLineF16(var9, var10, var17, var18);
            gameCanvas.drawLineF16(var11, var12, var15, var16);
        }

        gameCanvas.drawLineF16(var13, var14, var15, var16);
        gameCanvas.drawLineF16(var13, var14, var31, var32);
        gameCanvas.drawLineF16(var19, var20, var33, var34);
        gameCanvas.drawLineF16(var33, var34, var35, var36);
        if (!this.field_35) {
            gameCanvas.drawLineF16(var31, var32, var23, var24);
            gameCanvas.drawLineF16(var35, var36, var23, var24);
        }

        gameCanvas.drawLineF16(var17, var18, var27, var28);
        gameCanvas.drawLineF16(var19, var20, var25, var26);
        gameCanvas.drawLineF16(var25, var26, var29, var30);
        gameCanvas.drawLineF16(var27, var28, var29, var30);
    }

    // $FF: renamed from: if (i) void
    public void renderGame(GameCanvas gameCanvas) {
        gameCanvas.clearScreenWithWhite();
        int xxF16 = this.motoComponents[3].xF16 - this.motoComponents[4].xF16;
        int yyF16 = this.motoComponents[3].yF16 - this.motoComponents[4].yF16;
        int maxAbs;
        if ((maxAbs = getSmthLikeMaxAbs(xxF16, yyF16)) != 0) {
            xxF16 = (int) (((long) xxF16 << 32) / (long) maxAbs >> 16);
            yyF16 = (int) (((long) yyF16 << 32) / (long) maxAbs >> 16);
        }

        int var5 = -yyF16;
        if (this.field_35) {
            int var8 = this.motoComponents[4].xF16;
            int var7;
            if ((var7 = this.motoComponents[3].xF16) >= var8) {
                int var9 = var7;
                var7 = var8;
                var8 = var9;
            }

            this.levelLoader.gameLevel.method_183(var7, var8);
        }

        if (LevelLoader.isEnabledPerspective) {
            this.levelLoader.renderLevel3D(gameCanvas, this.motoComponents[0].xF16, this.motoComponents[0].yF16);
        }

        if (this.isRenderMotoWithSprites) {
            this.renderEngine(gameCanvas, xxF16, yyF16);
        }

        if (!Micro.isInGameMenu) {
            this.renderWheelTires(gameCanvas);
        }

        this.renderWheelSpokes(gameCanvas);
        if (this.isRenderMotoWithSprites) {
            gameCanvas.setColor(170, 0, 0);
        } else {
            gameCanvas.setColor(50, 50, 50);
        }

        gameCanvas.method_142(this.motoComponents[1].xF16 << 2 >> 16, this.motoComponents[1].yF16 << 2 >> 16, const175_1_half[0] << 2 >> 16, MathF16.atan2(xxF16, yyF16));
        if (!this.field_35) {
            this.renderMotoFork(gameCanvas);
        }

        this.renderSmth(gameCanvas, xxF16, yyF16, var5, xxF16);
        if (!this.isRenderMotoWithSprites) {
            this.renderMotoAsLines(gameCanvas, xxF16, yyF16, var5, xxF16);
        }

        this.levelLoader.renderTrackNearestLine(gameCanvas);
    }
}
