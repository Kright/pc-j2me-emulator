import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;

// $FF: renamed from: l
public class GameLevel {
    // $FF: renamed from: a int
    private int minX = 0;
    // $FF: renamed from: d int
    private int maxX = 0;
    // $FF: renamed from: e int
    private int field_264 = 0;
    // $FF: renamed from: b int
    private int field_265 = 0;
    // $FF: renamed from: g int
    private int field_266 = 0;
    // $FF: renamed from: null int
    public int startPosX;
    // $FF: renamed from: case int
    public int startPosY;
    // $FF: renamed from: long int
    public int finishPosX;
    // $FF: renamed from: goto int
    public int startFlagPoint = 0;
    // $FF: renamed from: for int
    public int finishFlagPoint = 0;
    // $FF: renamed from: else int
    public int finishPosY;
    // $FF: renamed from: void int
    public int pointsCount;
    // $FF: renamed from: int int
    public int field_274;
    // $FF: renamed from: char int[][]
    public int[][] pointPositions = (int[][]) null;
    // $FF: renamed from: try java.lang.String
    public String field_276 = "levelname";
    // $FF: renamed from: r int
    private int field_277 = 0;

    public GameLevel() {
        init();
    }

    // $FF: renamed from: int () void
    public void init() {
        this.startPosX = 0;
        this.startPosY = 0;
        this.finishPosX = 13107200;
        this.pointsCount = 0;
        this.field_274 = 0;
    }

    // $FF: renamed from: a (int, int, int, int) void
    public void method_174(int var1, int var2, int var3, int var4) {
        this.startPosX = var1 << 16 >> 3;
        this.startPosY = var2 << 16 >> 3;
        this.finishPosX = var3 << 16 >> 3;
        this.finishPosY = var4 << 16 >> 3;
    }

    // $FF: renamed from: if () int
    public int getStartPosX() {
        return this.startPosX << 3 >> 16;
    }

    // $FF: renamed from: do () int
    public int getStartPosY() {
        return this.startPosY << 3 >> 16;
    }

    // $FF: renamed from: a () int
    public int getFinishPosX() {
        return this.finishPosX << 3 >> 16;
    }

    // $FF: renamed from: for () int
    public int getFinishPosY() {
        return this.finishPosY << 3 >> 16;
    }

    // $FF: renamed from: if (int) int
    public int getPointX(int pointNo) {
        return this.pointPositions[pointNo][0] << 3 >> 16;
    }

    // $FF: renamed from: a (int) int
    public int getPointY(int pointNo) {
        return this.pointPositions[pointNo][1] << 3 >> 16;
    }

    // $FF: renamed from: do (int) int
    public int method_181(int var1) {
        int var2 = var1 - this.pointPositions[this.startFlagPoint][0];
        int var3;
        return ((var3 = this.pointPositions[this.finishFlagPoint][0] - this.pointPositions[this.startFlagPoint][0]) < 0 ? -var3 : var3) >= 3 && var2 <= var3 ? (int) (((long) var2 << 32) / (long) var3 >> 16) : 65536;
    }

    // $FF: renamed from: if (int, int) void
    public void setMinMaxX(int minX, int maxX) {
        this.minX = minX << 16 >> 3;
        this.maxX = maxX << 16 >> 3;
    }

    // $FF: renamed from: a (int, int) void
    public void method_183(int var1, int var2) {
        this.field_264 = var1 >> 1;
        this.field_265 = var2 >> 1;
    }

    // $FF: renamed from: a (int, int, int) void
    public void method_184(int var1, int var2, int var3) {
        this.field_264 = var1;
        this.field_265 = var2;
        this.field_266 = var3;
    }

    // $FF: renamed from: if (i, int, int) void
    public void renderShadow(GameCanvas gameCanvas, int var2, int var3) {
        if (var3 <= this.pointsCount - 1) {
            int var4 = this.field_266 - (this.pointPositions[var2][1] + this.pointPositions[var3 + 1][1] >> 1) < 0 ? 0 : this.field_266 - (this.pointPositions[var2][1] + this.pointPositions[var3 + 1][1] >> 1);
            if (this.field_266 <= this.pointPositions[var2][1] || this.field_266 <= this.pointPositions[var3 + 1][1]) {
                var4 = var4 < 327680 ? var4 : 327680;
            }

            this.field_277 = (int) ((long) this.field_277 * 49152L >> 16) + (int) ((long) var4 * 16384L >> 16);
            if (this.field_277 <= 557056) {
                int var5 = (int) (1638400L * (long) this.field_277 >> 16) >> 16;
                gameCanvas.setColor(var5, var5, var5);
                int var6 = this.pointPositions[var2][0] - this.pointPositions[var2 + 1][0];
                int var8 = (int) (((long) (this.pointPositions[var2][1] - this.pointPositions[var2 + 1][1]) << 32) / (long) var6 >> 16);
                int var9 = this.pointPositions[var2][1] - (int) ((long) this.pointPositions[var2][0] * (long) var8 >> 16);
                int var10 = (int) ((long) this.field_264 * (long) var8 >> 16) + var9;
                var6 = this.pointPositions[var3][0] - this.pointPositions[var3 + 1][0];
                var8 = (int) (((long) (this.pointPositions[var3][1] - this.pointPositions[var3 + 1][1]) << 32) / (long) var6 >> 16);
                var9 = this.pointPositions[var3][1] - (int) ((long) this.pointPositions[var3][0] * (long) var8 >> 16);
                int var11 = (int) ((long) this.field_265 * (long) var8 >> 16) + var9;
                if (var2 == var3) {
                    gameCanvas.drawLine(this.field_264 << 3 >> 16, var10 + 65536 << 3 >> 16, this.field_265 << 3 >> 16, var11 + 65536 << 3 >> 16);
                    return;
                }

                gameCanvas.drawLine(this.field_264 << 3 >> 16, var10 + 65536 << 3 >> 16, this.pointPositions[var2 + 1][0] << 3 >> 16, this.pointPositions[var2 + 1][1] + 65536 << 3 >> 16);

                for (int i = var2 + 1; i < var3; ++i) {
                    gameCanvas.drawLine(this.pointPositions[i][0] << 3 >> 16, this.pointPositions[i][1] + 65536 << 3 >> 16, this.pointPositions[i + 1][0] << 3 >> 16, this.pointPositions[i + 1][1] + 65536 << 3 >> 16);
                }

                gameCanvas.drawLine(this.pointPositions[var3][0] << 3 >> 16, this.pointPositions[var3][1] + 65536 << 3 >> 16, this.field_265 << 3 >> 16, var11 + 65536 << 3 >> 16);
            }
        }

    }

    // $FF: renamed from: a (i, int, int) void
    public synchronized void renderLevel3D(GameCanvas gameCanvas, int xF16, int yF16) {
        int var7 = 0;
        int var8 = 0;

        int lineNo;
        for (lineNo = 0; lineNo < this.pointsCount - 1 && this.pointPositions[lineNo][0] <= this.minX; ++lineNo) {
        }

        if (lineNo > 0) {
            --lineNo;
        }

        int var9 = xF16 - this.pointPositions[lineNo][0];
        int var10 = yF16 + 3276800 - this.pointPositions[lineNo][1];
        int var11 = GamePhysics.getSmthLikeMaxAbs(var9, var10);
        var9 = (int) (((long) var9 << 32) / (long) (var11 >> 1 >> 1) >> 16);
        var10 = (int) (((long) var10 << 32) / (long) (var11 >> 1 >> 1) >> 16);
        gameCanvas.setColor(0, 170, 0);

        while (lineNo < this.pointsCount - 1) {
            int var4 = var9;
            int var5 = var10;
            var9 = xF16 - this.pointPositions[lineNo + 1][0];
            var10 = yF16 + 3276800 - this.pointPositions[lineNo + 1][1];
            var11 = GamePhysics.getSmthLikeMaxAbs(var9, var10);
            var9 = (int) (((long) var9 << 32) / (long) (var11 >> 1 >> 1) >> 16);
            var10 = (int) (((long) var10 << 32) / (long) (var11 >> 1 >> 1) >> 16);
            // far line
            gameCanvas.drawLine(this.pointPositions[lineNo][0] + var4 << 3 >> 16, this.pointPositions[lineNo][1] + var5 << 3 >> 16, this.pointPositions[lineNo + 1][0] + var9 << 3 >> 16, this.pointPositions[lineNo + 1][1] + var10 << 3 >> 16);
            // from far to near
            gameCanvas.drawLine(this.pointPositions[lineNo][0] << 3 >> 16, this.pointPositions[lineNo][1] << 3 >> 16, this.pointPositions[lineNo][0] + var4 << 3 >> 16, this.pointPositions[lineNo][1] + var5 << 3 >> 16);
            if (lineNo > 1) {
                if (this.pointPositions[lineNo][0] > this.field_264 && var7 == 0) {
                    var7 = lineNo - 1;
                }

                if (this.pointPositions[lineNo][0] > this.field_265 && var8 == 0) {
                    var8 = lineNo - 1;
                }
            }

            if (this.startFlagPoint == lineNo) {
                // render far start flag
                gameCanvas.renderStartFlag(this.pointPositions[this.startFlagPoint][0] + var4 << 3 >> 16, this.pointPositions[this.startFlagPoint][1] + var5 << 3 >> 16);
                gameCanvas.setColor(0, 170, 0);
            }

            if (this.finishFlagPoint == lineNo) {
                // render far finish flag
                gameCanvas.renderFinishFlag(this.pointPositions[this.finishFlagPoint][0] + var4 << 3 >> 16, this.pointPositions[this.finishFlagPoint][1] + var5 << 3 >> 16);
                gameCanvas.setColor(0, 170, 0);
            }

            if (this.pointPositions[lineNo][0] > this.maxX) {
                break;
            }

            ++lineNo;
        }

        gameCanvas.drawLine(this.pointPositions[this.pointsCount - 1][0] << 3 >> 16, this.pointPositions[this.pointsCount - 1][1] << 3 >> 16, this.pointPositions[this.pointsCount - 1][0] + var9 << 3 >> 16, this.pointPositions[this.pointsCount - 1][1] + var10 << 3 >> 16);
        if (LevelLoader.isEnabledShadows) {
            this.renderShadow(gameCanvas, var7, var8);
        }

    }

    // $FF: renamed from: a (i) void
    public synchronized void renderTrackNearestGreenLine(GameCanvas canvas) {
        int pointNo;
        for (pointNo = 0; pointNo < this.pointsCount - 1 && this.pointPositions[pointNo][0] <= this.minX; ++pointNo) {
        }

        if (pointNo > 0) {
            --pointNo;
        }

        while (pointNo < this.pointsCount - 1) {
            canvas.drawLine(this.pointPositions[pointNo][0] << 3 >> 16, this.pointPositions[pointNo][1] << 3 >> 16, this.pointPositions[pointNo + 1][0] << 3 >> 16, this.pointPositions[pointNo + 1][1] << 3 >> 16);
            if (this.startFlagPoint == pointNo) {
                canvas.renderStartFlag(this.pointPositions[this.startFlagPoint][0] << 3 >> 16, this.pointPositions[this.startFlagPoint][1] << 3 >> 16);
                canvas.setColor(0, 255, 0);
            }

            if (this.finishFlagPoint == pointNo) {
                canvas.renderFinishFlag(this.pointPositions[this.finishFlagPoint][0] << 3 >> 16, this.pointPositions[this.finishFlagPoint][1] << 3 >> 16);
                canvas.setColor(0, 255, 0);
            }

            if (this.pointPositions[pointNo][0] > this.maxX) {
                break;
            }

            ++pointNo;
        }
    }

    // $FF: renamed from: for (int, int) void
    public void addPointSimple(int var1, int var2) {
        this.addPoint(var1 << 16 >> 3, var2 << 16 >> 3);
    }

    // $FF: renamed from: do (int, int) void
    public void addPoint(int x, int y) {
        if (this.pointPositions == null || this.pointPositions.length <= this.pointsCount) {
            int var3 = 100;
            if (this.pointPositions != null) {
                var3 = var3 < this.pointPositions.length + 30 ? this.pointPositions.length + 30 : var3;
            }

            int[][] var4 = new int[var3][2];
            if (this.pointPositions != null) {
                System.arraycopy(this.pointPositions, 0, var4, 0, this.pointPositions.length);
            }

            this.pointPositions = var4;
        }

        if (this.pointsCount == 0 || this.pointPositions[this.pointsCount - 1][0] < x) {
            this.pointPositions[this.pointsCount][0] = x;
            this.pointPositions[this.pointsCount][1] = y;
            ++this.pointsCount;
        }

    }

    // $FF: renamed from: a (java.io.DataInputStream) void
    public synchronized void load(DataInputStream var1) throws IOException {
        this.init();
        if (var1.readByte() == 50) {
            byte[] var3 = new byte[20];
            var1.readFully(var3);
        }

        this.finishFlagPoint = 0;
        this.startFlagPoint = 0;
        this.startPosX = var1.readInt();
        this.startPosY = var1.readInt();
        this.finishPosX = var1.readInt();
        this.finishPosY = var1.readInt();
        short pointsCount = var1.readShort();
        int pointX = var1.readInt();
        int pointY = var1.readInt();
        int offsetX = pointX;
        int offsetY = pointY;
        this.addPointSimple(pointX, pointY);

        for (int i = 1; i < pointsCount; ++i) {
            byte modeOrDx;
            if ((modeOrDx = var1.readByte()) == -1) {
                offsetY = 0;
                offsetX = 0;
                pointX = var1.readInt();
                pointY = var1.readInt();
            } else {
                pointX = modeOrDx;
                pointY = var1.readByte();
            }

            offsetX += pointX;
            offsetY += pointY;
            this.addPointSimple(offsetX, offsetY);
        }

    }

    // $FF: renamed from: a (java.lang.String) java.io.InputStream
    public static InputStream makeInputStream(String name) {
        return (new Object()).getClass().getResourceAsStream("/" + name);
    }
}
