This code isn't associated with Codebrew Software in any fashion. All rights to the original Gravity Defied, it's name, logotype, brand and all that stuff belongs to Codebrew Software.

**How to run**
./gradlew app-original:run


## project structure
* **emulaltor** - proxy classes for emulating java 2 ME classes on PC. Implementation is extremely incompete and was tested on just one game.
* **app-original** - app for running emulator with original jar
* **app-from-souces** - app for running emulator with restored code. Behavour should be identical.
