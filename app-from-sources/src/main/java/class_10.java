// $FF: renamed from: k
public class class_10 {
    // $FF: renamed from: do boolean
    public boolean unusedBool;
    // $FF: renamed from: a int
    public int field_257;
    // $FF: renamed from: int int
    public int field_258;
    // $FF: renamed from: for int
    public int field_259;
    // $FF: renamed from: new int
    public int field_260;
    // $FF: renamed from: if n[]
    public TimerOrMotoPartOrMenuElem[] motoComponents = new TimerOrMotoPartOrMenuElem[6];

    public class_10() {
        for (int var1 = 0; var1 < 6; ++var1) {
            this.motoComponents[var1] = new TimerOrMotoPartOrMenuElem();
        }

        this.reset();
    }

    // $FF: renamed from: a () void
    public void reset() {
        this.field_257 = this.field_259 = this.field_260 = 0;
        this.unusedBool = true;

        for (int i = 0; i < 6; ++i) {
            this.motoComponents[i].setToZeros();
        }
    }
}
