// $FF: renamed from: d
public class MathF16 {
    // $FF: renamed from: case int
    public static final int INT_MAX = Integer.MAX_VALUE;
    // $FF: renamed from: int int
    public static final int INT_MIN = Integer.MIN_VALUE;
    // $FF: renamed from: if int
    public static final int unusedField_83 = 0;
    // $FF: renamed from: a int
    public static final int unusedField_84 = 1;
    // $FF: renamed from: try int
    public static int PiHalfF16 = 102944;
    // $FF: renamed from: new int
    public static int Pi2F16 = 411775;
    // $FF: renamed from: goto int
    public static int PiF16 = 205887;
    // $FF: renamed from: byte int
    public static int Cx10000 = 65536;
    // $FF: renamed from: j int
    private static int C_64 = 64;
    // $FF: renamed from: k int
    private static int C_64F;
    // $FF: renamed from: l int[]
    private static int[] sinTable;
    // $FF: renamed from: m int[]
    private static int[] xzTable;

    // $FF: renamed from: a (int, int) int
    public static int multiplyF16(int a, int b) {
        return (int) ((long) a * (long) b >> 16);
    }

    // $FF: renamed from: do (int, int) int
    public static int divideF16(int var0, int var1) {
        return (int) (((long) var0 << 32) / (long) var1 >> 16);
    }

    // $FF: renamed from: if (int) int
    public static int sinF16(int angle) {
        while (angle >= PiF16) {
            angle -= Pi2F16;
        }

        while (angle <= -PiF16) {
            angle += Pi2F16;
        }

        if (angle >= PiHalfF16) {
            angle = PiF16 - angle;
        }

        if (angle <= -PiHalfF16) {
            angle = -PiF16 - angle;
        }

        int pos;
        if (angle < 0) {
            pos = divideF16(multiplyF16(-angle, C_64F), PiHalfF16);
            return -sinTable[pos >> 16];
        } else {
            pos = divideF16(multiplyF16(angle, C_64F), PiHalfF16);
            return sinTable[pos >> 16];
        }
    }

    // $FF: renamed from: do (int) int
    public static int cosF16(int angle) {
        return sinF16(PiHalfF16 - angle);
    }

    // $FF: renamed from: a (int) int
    public static int xz(int angle) {
        int result = 0;
        boolean isNegative = false;

        try {
            if (angle < 0) {
                isNegative = true;
                angle = -angle;
            }

            int pos;
            if (angle <= Cx10000) {
                if ((pos = multiplyF16(angle, C_64F)) >> 16 == C_64F) {
                    result = PiHalfF16;
                } else {
                    result = xzTable[pos >> 16];
                }
            } else {
                pos = multiplyF16(divideF16(Cx10000, angle), C_64F);
                result = PiHalfF16 - xzTable[pos >> 16];
            }
        } catch (ArrayIndexOutOfBoundsException var4) {
        }

        return isNegative ? -result : result;
    }

    // $FF: renamed from: if (int, int) int
    public static int atan2(int dx, int dy) {
        if ((dy < 0 ? -dy : dy) < 3) {
            return (dx > 0 ? 1 : -1) * PiHalfF16;
        } else {
            int var3 = xz(divideF16(dx, dy));
            if (dx > 0) {
                return dy > 0 ? var3 : PiF16 + var3;
            } else {
                return dy > 0 ? var3 : var3 - PiF16;
            }
        }
    }

    static {
        C_64F = C_64 << 16;
        // [int(0x10000 * math.sin(3.1415 * i / 128)) for i in range(64)]
        sinTable = new int[]{0, 1608, 3215, 4821, 6423, 8022, 9616, 11204, 12785, 14359, 15923, 17479, 19024, 20557, 22078, 23586, 25079, 26557, 28020, 29465, 30893, 32302, 33692, 35061, 36409, 37736, 39039, 40319, 41575, 42806, 44011, 45189, 46340, 47464, 48558, 49624, 50660, 51665, 52639, 53581, 54491, 55368, 56212, 57022, 57797, 58538, 59243, 59913, 60547, 61144, 61705, 62228, 62714, 63162, 63571, 63943, 64276, 64571, 64826, 65043, 65220, 65358, 65457, 65516};
        // xz, maybe atan
        xzTable = new int[]{0, 1023, 2047, 3069, 4090, 5109, 6126, 7139, 8149, 9155, 10157, 11155, 12146, 13133, 14113, 15087, 16054, 17015, 17967, 18912, 19849, 20778, 21698, 22610, 23512, 24405, 25289, 26163, 27027, 27882, 28726, 29561, 30385, 31199, 32003, 32796, 33579, 34352, 35114, 35866, 36608, 37339, 38060, 38771, 39471, 40161, 40841, 41512, 42172, 42822, 43463, 44094, 44716, 45328, 45931, 46524, 47109, 47684, 48251, 48809, 49358, 49899, 50431, 50955};
    }
}
