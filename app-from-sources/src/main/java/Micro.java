import java.io.IOException;
import java.io.InputStream;
import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;

public class Micro extends MIDlet implements Runnable {
    // $FF: renamed from: d i
    public GameCanvas gameCanvas;
    // $FF: renamed from: char f
    public LevelLoader levelLoader;
    // $FF: renamed from: else b
    public GamePhysics gamePhysics;
    // $FF: renamed from: try m
    public MenuManager menuManager;
    // $FF: renamed from: j java.lang.Thread
    private Thread thread = null;
    // $FF: renamed from: case boolean
    public boolean field_242 = false;
    // $FF: renamed from: null int
    public int numPhysicsLoops = 2;
    // $FF: renamed from: for long
    public long timeMs = 0L;
    // $FF: renamed from: goto long
    public long gameTimeMs = 0L;
    // $FF: renamed from: byte long
    public long field_246 = 0L;
    // $FF: renamed from: int boolean
    public boolean isInited = false;
    // $FF: renamed from: if boolean
    public boolean field_248 = false;
    // $FF: renamed from: a boolean
    public static boolean field_249 = false;
    // $FF: renamed from: c boolean
    public static boolean isPaused = true;
    // $FF: renamed from: e int
    public static final int unusedField_251 = 30;
    // $FF: renamed from: b boolean
    public static boolean isInGameMenu;
    // $FF: renamed from: long int
    public static int gameLoadingStateStage = 0;
    // $FF: renamed from: new int
    public static final int unusedField_254 = 10;
    // $FF: renamed from: x byte[]
    private static byte[] singleByteArr = new byte[1];

    public void gameToMenu() {
        this.gameCanvas.removeMenuCommand();
        isInGameMenu = true;
        this.menuManager.addOkAndBackCommands();
    }

    public void menuToGame() {
        this.menuManager.removeOkAndBackCommands();
        isInGameMenu = false;
        this.gameCanvas.addMenuCommand();
    }

    // $FF: renamed from: a () long
    private long goLoadingStep() {
        ++gameLoadingStateStage;
        this.gameCanvas.repaint();
        long startTimeMillis = System.currentTimeMillis();
        switch (gameLoadingStateStage) {
            case 1:
                this.levelLoader = new LevelLoader();
                break;
            case 2:
                this.gamePhysics = new GamePhysics(this.levelLoader);
                this.gameCanvas.init(this.gamePhysics);
                break;
            case 3:
                this.menuManager = new MenuManager(this);
                this.menuManager.initPart(1);
                break;
            case 4:
                this.menuManager.initPart(2);
                break;
            case 5:
                this.menuManager.initPart(3);
                break;
            case 6:
                this.menuManager.initPart(4);
                break;
            case 7:
                this.menuManager.initPart(5);
                break;
            case 8:
                this.menuManager.initPart(6);
                break;
            case 9:
                this.menuManager.initPart(7);
                break;
            case 10:
                this.gameCanvas.setMenuManager(this.menuManager);
                this.gameCanvas.setViewPosition(-50, 150);
                this.setMode(1);
                break;
            default:
                --gameLoadingStateStage;

                try {
                    Thread.sleep(100L);
                } catch (InterruptedException var3) {
                }
        }

        return System.currentTimeMillis() - startTimeMillis;
    }

    public void init() {
        long timeToLoading = 3000L;
        Thread.yield();
        this.gameCanvas = new GameCanvas(this);
        Display.getDisplay(this).setCurrent(this.gameCanvas);
        this.gameCanvas.requestRepaint(1);

        while (!this.gameCanvas.isShown()) {
            this.goLoadingStep();
        }

        long deltaTimeMs;
        while (timeToLoading > 0L) {
            deltaTimeMs = this.goLoadingStep();
            timeToLoading -= deltaTimeMs;
        }

        this.gameCanvas.requestRepaint(2);

        for (timeToLoading = 3000L; timeToLoading > 0L; timeToLoading -= deltaTimeMs) {
            deltaTimeMs = this.goLoadingStep();
        }

        while (gameLoadingStateStage < 10) {
            this.goLoadingStep();
        }

        this.gameCanvas.requestRepaint(0);
        this.isInited = true;
    }

    public static byte[] readBigFile(String var0, int var1) {
        byte[] result = null;

        try {
            InputStream var3 = new Object().getClass().getResourceAsStream("/" + var0);
            result = new byte[var1];
            var3.read(result, 0, var1);
        } catch (IOException var4) {
        }

        return result;
    }

    public static byte readByte(InputStream var0) {
        try {
            var0.read(singleByteArr, 0, 1);
        } catch (IOException var1) {
        }

        return singleByteArr[0];
    }

    public static InputStream readFile(String var0) {
        InputStream var1;
        if (var0.charAt(0) == '/') {
            var1 = var0.getClass().getResourceAsStream(var0);
        } else {
            var1 = var0.getClass().getResourceAsStream("/" + var0);
        }

        return var1;
    }

    public void restart(boolean var1) {
        this.gamePhysics.resetSmth(true);
        this.timeMs = 0L;
        this.gameTimeMs = 0L;
        this.field_246 = 0L;
        if (var1) {
            this.gameCanvas.scheduleGameTimerTask(this.levelLoader.getName(this.menuManager.getCurrentLevel(), this.menuManager.getCurrentTrack()), 3000);
        }

        this.gameCanvas.method_129();
    }

    protected void destroyApp(boolean var1) {
        field_249 = false;
        synchronized (this.gameCanvas) {
            ;
        }

        this.field_242 = true;
        this.menuManager.saveSmthToRecordStoreAndCloseIt();
        this.notifyDestroyed();
    }

    protected void startApp() {
        field_249 = true;
        isPaused = false;
        if (this.thread == null) {
            this.thread = new Thread(this);
            this.thread.start();
        }
    }

    protected void pauseApp() {
        isPaused = true;
        if (!isInGameMenu) {
            this.gameToMenu();
        }

        System.gc();
    }

    public void run() {
        if (!this.isInited) {
//            try {
            this.init();
//            } catch (IOException var13) {
//                throw new RuntimeException();
//            }
        }

        this.gameCanvas.setCommandListener(this.gameCanvas);
        this.restart(false);
        this.menuManager.method_201(0);
        if (this.menuManager.method_196()) {
            this.restart(true);
        }

        long var3 = 0L;

        while (field_249) {
            int var5;
            if (this.gamePhysics.method_21() != this.menuManager.method_210()) {
                var5 = this.gameCanvas.loadSprites(this.menuManager.method_210());
                this.gamePhysics.method_22(var5);
                this.menuManager.method_211(var5);
            }

            boolean var10000;
            boolean var10001;
            try {
                if (isInGameMenu) {
                    this.menuManager.method_201(1);
                    if (this.menuManager.method_196()) {
                        this.restart(true);
                    }
                }

                for (int i = this.numPhysicsLoops; i > 0; --i) {
                    if (this.field_248) {
                        this.gameTimeMs += 20L;
                    }

                    if (this.timeMs == 0L) {
                        this.timeMs = System.currentTimeMillis();
                    }

                    if ((var5 = this.gamePhysics.updatePhysics()) == 3 && this.field_246 == 0L) {
                        this.field_246 = System.currentTimeMillis() + 3000L;
                        this.gameCanvas.scheduleGameTimerTask("Crashed", 3000);
                        this.gameCanvas.repaint();
                        this.gameCanvas.serviceRepaints();
                    }

                    if (this.field_246 != 0L && this.field_246 < System.currentTimeMillis()) {
                        this.restart(true);
                    }

                    if (var5 == 5) {
                        this.gameCanvas.scheduleGameTimerTask("Crashed", 3000);
                        this.gameCanvas.repaint();
                        this.gameCanvas.serviceRepaints();

                        try {
                            long var7 = 1000L;
                            if (this.field_246 > 0L) {
                                var7 = Math.min(this.field_246 - System.currentTimeMillis(), 1000L);
                            }

                            if (var7 > 0L) {
                                Thread.sleep(var7);
                            }
                        } catch (InterruptedException var12) {
                        }

                        this.restart(true);
                    } else if (var5 == 4) {
                        this.timeMs = 0L;
                        this.gameTimeMs = 0L;
                    } else if (var5 == 1 || var5 == 2) {
                        if (var5 == 2) {
                            this.gameTimeMs -= 10L;
                        }

                        this.goalLoop();
                        this.menuManager.method_215(this.gameTimeMs / 10L);
                        this.menuManager.method_201(2);
                        if (this.menuManager.method_196()) {
                            this.restart(true);
                        }

                        if (!field_249) {
                            break;
                        }
                    }

                    this.field_248 = var5 != 4;
                }

                var10000 = field_249;
            } catch (Exception var15) {
                var10001 = false;
                continue;
            }

            if (!var10000) {
                break;
            }

            try {
                this.gamePhysics.method_53();
                long var1;
                if ((var1 = System.currentTimeMillis()) - var3 < 30L) {
                    try {
                        synchronized (this) {
                            this.wait(Math.max(30L - (var1 - var3), 1L));
                        }
                    } catch (InterruptedException var11) {
                    }

                    var3 = System.currentTimeMillis();
                } else {
                    var3 = var1;
                }

                this.gameCanvas.repaint();
            } catch (Exception var14) {
                var10001 = false;
            }
        }

        this.destroyApp(true);
    }

    public void goalLoop() {
        long var4 = 0L;
        if (!this.gamePhysics.field_69) {
            this.gameCanvas.scheduleGameTimerTask("Wheelie!", 1000);
        } else {
            this.gameCanvas.scheduleGameTimerTask("Finished", 1000);
        }

        for (long timeMs = System.currentTimeMillis() + 1000L; timeMs > System.currentTimeMillis(); this.gameCanvas.repaint()) {
            if (isInGameMenu) {
                this.gameCanvas.repaint();
                return;
            }

            for (int i = this.numPhysicsLoops; i > 0; --i) {
                if (this.gamePhysics.updatePhysics() == 5) {
                    try {
                        long deltaTime;
                        if ((deltaTime = timeMs - System.currentTimeMillis()) > 0L) {
                            Thread.sleep(deltaTime);
                        }

                        return;
                    } catch (InterruptedException var12) {
                        return;
                    }
                }
            }

            this.gamePhysics.method_53();
            long var2;
            if ((var2 = System.currentTimeMillis()) - var4 < 30L) {
                try {
                    synchronized (this) {
                        this.wait(Math.max(30L - (var2 - var4), 1L));
                    }
                } catch (InterruptedException var14) {
                }

                var4 = System.currentTimeMillis();
            } else {
                var4 = var2;
            }
        }
    }

    public void setNumPhysicsLoops(int value) {
        this.numPhysicsLoops = value;
    }

    public void setMode(int mode) {
        this.gamePhysics.setMode(mode);
    }
}
