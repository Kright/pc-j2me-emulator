import java.util.TimerTask;
import javax.microedition.lcdui.Graphics;

// $FF: renamed from: n
public class TimerOrMotoPartOrMenuElem extends TimerTask implements IGameMenuElement {
    // $FF: renamed from: char int
    public int xF16;
    // $FF: renamed from: i int
    public int yF16;
    // $FF: renamed from: b int
    public int angleF16;
    // $FF: renamed from: e int
    public int field_382;
    // $FF: renamed from: d int
    public int field_383;
    // $FF: renamed from: goto int
    public int field_384;
    // $FF: renamed from: null int
    public int field_385;
    // $FF: renamed from: long int
    public int field_386;
    // $FF: renamed from: f int
    public int field_387;
    // $FF: renamed from: c int
    int timerNo;
    // $FF: renamed from: j Micro
    Micro micro;
    // $FF: renamed from: g java.lang.String
    private String text;
    // $FF: renamed from: w e
    private GameMenu gameMenu;
    // $FF: renamed from: h c
    private IMenuManager menuManager;

    public TimerOrMotoPartOrMenuElem() {
        this.setToZeros();
    }

    // $FF: renamed from: int () void
    public void setToZeros() {
        this.xF16 = this.yF16 = this.angleF16 = 0;
        this.field_382 = this.field_383 = this.field_384 = 0;
        this.field_385 = this.field_386 = this.field_387 = 0;
    }

    public TimerOrMotoPartOrMenuElem(int timerNo, Micro micro) {
        this.micro = micro;
        this.timerNo = timerNo;
    }

    public void run() {
        this.micro.gameCanvas.method_150(this.timerNo);
    }

    public TimerOrMotoPartOrMenuElem(String text, GameMenu gameMenu, IMenuManager menuManager) {
        this.text = text + ">";
        this.gameMenu = gameMenu;
        this.menuManager = menuManager;
    }

    // $FF: renamed from: a (java.lang.String) void
    public void setText(String text) {
        this.text = text + ">";
    }

    // $FF: renamed from: new () java.lang.String
    public String getText() {
        return this.text;
    }

    // $FF: renamed from: do () boolean
    public boolean isNotTextRender() {
        return true;
    }

    // $FF: renamed from: a (int) void
    public void menuElemMethod(int var1) {
        switch (var1) {
            case 1:
            case 2:
                this.menuManager.processMenu(this);
                this.gameMenu.setGameMenu(this.menuManager.getGameMenu());
                this.menuManager.method_1(this.gameMenu, false);
            case 3:
            default:
        }
    }

    // $FF: renamed from: a (e) void
    public void setGameMenu(GameMenu gameMenu) {
        this.gameMenu = gameMenu;
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics, int, int) void
    public void render(Graphics graphics, int y, int x) {
        graphics.drawString(this.text, x, y, 20);
    }
}
