import javax.microedition.lcdui.Graphics;

// $FF: renamed from: j
public interface IGameMenuElement {
    // $FF: renamed from: a (java.lang.String) void
    void setText(String text);

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics, int, int) void
    void render(Graphics graphics, int y, int x);

    // $FF: renamed from: do () boolean
    boolean isNotTextRender();

    // $FF: renamed from: a (int) void
    void menuElemMethod(int var1);
}
