import java.util.Vector;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

// $FF: renamed from: e
public class GameMenu {
    // $FF: renamed from: try e
    protected GameMenu gameMenu;
    // $FF: renamed from: h java.lang.String
    protected String field_94;
    // $FF: renamed from: if int
    protected int field_95;
    // $FF: renamed from: l java.util.Vector
    private Vector vector;
    // $FF: renamed from: new Micro
    protected Micro micro;
    // $FF: renamed from: g javax.microedition.lcdui.Font
    protected Font font;
    // $FF: renamed from: b javax.microedition.lcdui.Font
    protected Font font2;
    // $FF: renamed from: p javax.microedition.lcdui.Font
    private Font font3;
    // $FF: renamed from: goto int
    protected int field_101;
    // $FF: renamed from: char int
    public int xPos;
    // $FF: renamed from: c int
    protected int field_103;
    // $FF: renamed from: t int
    private int field_104;
    // $FF: renamed from: e int
    private int field_105;
    // $FF: renamed from: d int
    private int field_106;
    // $FF: renamed from: w int
    private int field_107;
    // $FF: renamed from: x int
    private int canvasWidth;
    // $FF: renamed from: y int
    private int canvasHeight;
    // $FF: renamed from: z int
    private int field_110;
    // $FF: renamed from: a boolean
    private boolean field_111;
    // $FF: renamed from: f int
    private int nameCursorPos;
    // $FF: renamed from: ca byte[]
    private byte[] strArr = null;

    public GameMenu(String var1, Micro micro, GameMenu var3, byte[] var4) {
        this.field_94 = var1;
        this.field_95 = -1;
        this.vector = new Vector();
        this.micro = micro;
        this.gameMenu = var3;
        this.canvasWidth = micro.gameCanvas.getWidth();
        this.canvasHeight = micro.gameCanvas.getHeight();
        this.font = Font.getFont(64, 1, 16);
        this.font3 = Font.getFont(64, 0, 8);
        if (this.canvasWidth >= 128) {
            this.font2 = Font.getFont(64, 1, 0);
        } else {
            this.font2 = this.font3;
        }

        TextRender.setDefaultFont(this.font3);
        TextRender.setMaxArea(this.canvasWidth, this.canvasHeight);
        this.field_101 = 1;
        if (this.canvasWidth <= 100) {
            this.xPos = 6;
        } else {
            this.xPos = 9;
        }

        if (this.canvasHeight <= 100) {
            this.field_94 = "";
        }

        this.field_104 = this.xPos + 7;
        this.field_103 = 2;
        this.field_110 = 0;
        if (!this.field_94.equals("")) {
            this.field_107 = (this.canvasHeight - (this.field_101 << 1) - 10 - this.font.getBaselinePosition()) / (this.font2.getBaselinePosition() + this.field_103);
        } else {
            this.field_107 = (this.canvasHeight - (this.field_101 << 1) - 10) / (this.font2.getBaselinePosition() + this.field_103);
        }

        if (var4 != null) {
            this.field_111 = true;
            this.nameCursorPos = 0;
            this.xPos = 8;
            this.strArr = var4;
        } else {
            this.field_111 = false;
        }

        if (this.field_107 > 13) {
            this.field_107 = 13;
        }

    }

    // $FF: renamed from: a (int) void
    public void method_68(int var1) {
        this.field_103 = var1;
    }

    // $FF: renamed from: a (java.lang.String) void
    public void method_69(String var1) {
        this.field_94 = var1;
    }

    // $FF: renamed from: int () void
    public void method_70() {
        if (this.field_111) {
            this.nameCursorPos = 0;
        } else {
            if (this.vector != null) {
                this.field_95 = 0;

                for (int var1 = 0; var1 < this.vector.size() && var1 < this.field_107; ++var1) {
                    if (((IGameMenuElement) this.vector.elementAt(var1)).isNotTextRender()) {
                        this.field_95 = var1;
                        break;
                    }
                }

                this.field_105 = 0;
                this.field_106 = this.vector.size() - 1;
                if (this.field_106 > this.field_107 - 1) {
                    this.field_106 = this.field_107 - 1;
                }
            }
        }
    }

    // $FF: renamed from: case () void
    public void method_71() {
        this.field_95 = this.vector.size() - 1;

        for (int var1 = this.vector.size() - 1; var1 > 0; --var1) {
            if (((IGameMenuElement) this.vector.elementAt(var1)).isNotTextRender()) {
                this.field_95 = var1;
                break;
            }
        }

        this.field_105 = this.vector.size() - this.field_107;
        if (this.field_105 < 0) {
            this.field_105 = 0;
        }

        this.field_106 = this.vector.size() - 1;
        if (this.field_106 > this.field_95 + this.field_107) {
            this.field_106 = this.field_95 + this.field_107;
        }
    }

    // $FF: renamed from: a (j) void
    public void addMenuElement(IGameMenuElement var1) {
        int var2 = this.field_101;
        this.field_107 = 1;
        this.vector.addElement(var1);
        if (!this.field_94.equals("")) {
            var2 = this.font.getBaselinePosition() + 2;
        }

        if (this.canvasHeight < 100) {
            ++var2;
        } else {
            var2 += 4;
        }

        for (int var3 = 0; var3 < this.vector.size() - 1; ++var3) {
            if (((IGameMenuElement) this.vector.elementAt(var3)).isNotTextRender()) {
                var2 += this.font2.getBaselinePosition() + this.field_103;
            } else {
                var2 += (TextRender.getBaselinePosition() < GameCanvas.spriteSizeY[5] ? GameCanvas.spriteSizeY[5] : TextRender.getBaselinePosition()) + this.field_103;
            }

            if (var2 > this.canvasHeight - (this.field_101 << 1) - 10) {
                break;
            }

            ++this.field_107;
        }

        if (this.field_107 > 13) {
            this.field_107 = 13;
        }

        this.method_70();
    }

    // $FF: renamed from: try () void
    public void processGameActionDown() {
        if (this.field_111) {
            if (this.strArr[this.nameCursorPos] == 32) {
                this.strArr[this.nameCursorPos] = 90;
                return;
            }

            --this.strArr[this.nameCursorPos];
            if (this.strArr[this.nameCursorPos] < 65) {
                this.strArr[this.nameCursorPos] = 32;
                return;
            }
        } else if (this.vector.size() != 0) {
            if (!((IGameMenuElement) this.vector.elementAt(this.field_95)).isNotTextRender()) {
                ++this.field_106;
                this.field_95 = this.field_106;
                ++this.field_105;
                return;
            }

            ++this.field_95;
            if (this.field_95 > this.vector.size() - 1) {
                this.method_70();
                return;
            }

            boolean var3 = false;

            int var2;
            for (var2 = this.field_95; var2 <= this.field_106 + 1; ++var2) {
                if (((IGameMenuElement) this.vector.elementAt(var2)).isNotTextRender()) {
                    var3 = true;
                    break;
                }
            }

            if (var3) {
                this.field_95 = var2;
            } else if (this.field_106 < this.vector.size() - 1) {
                ++this.field_106;
                ++this.field_105;
            } else {
                --this.field_95;
            }

            if (this.field_95 > this.field_106) {
                ++this.field_105;
                ++this.field_106;
                if (this.field_106 > this.vector.size() - 1) {
                    this.field_106 = this.vector.size() - 1;
                }

                this.field_95 = this.field_106;
            }
        }
    }

    // $FF: renamed from: new () void
    public void processGameActionUp() {
        if (this.field_111) {
            if (this.strArr[this.nameCursorPos] == 32) {
                this.strArr[this.nameCursorPos] = 65;
                return;
            }

            ++this.strArr[this.nameCursorPos];
            if (this.strArr[this.nameCursorPos] > 90) {
                this.strArr[this.nameCursorPos] = 32;
                return;
            }
        } else if (this.vector.size() != 0) {
            IGameMenuElement var4 = (IGameMenuElement) this.vector.elementAt(this.field_95);
            --this.field_95;
            if (this.field_95 < 0) {
                this.method_71();
                return;
            }

            boolean var3 = false;

            int var2;
            for (var2 = this.field_95; var2 >= this.field_105; --var2) {
                if (((IGameMenuElement) this.vector.elementAt(var2)).isNotTextRender()) {
                    var3 = true;
                    break;
                }
            }

            if (!var3) {
                if (this.field_105 > 0) {
                    --this.field_105;
                    if (this.vector.size() > this.field_107 - 1) {
                        --this.field_106;
                        return;
                    }
                } else {
                    this.method_71();
                }

                return;
            }

            this.field_95 = var2;
            if (this.field_95 < this.field_105) {
                --this.field_105;
                if (this.field_105 < 0) {
                    this.field_95 = 0;
                    this.field_105 = 0;
                }

                if (this.vector.size() > this.field_107 - 1) {
                    --this.field_106;
                }
            }
        }
    }

    // $FF: renamed from: if (int) void
    public void processGameActionUpd(int var1) {
        if (this.field_111) {
            switch (var1) {
                case 1:
                    if (this.nameCursorPos == 2) {
                        this.micro.menuManager.method_1(this.gameMenu, false);
                        return;
                    }

                    ++this.nameCursorPos;
                    return;
                case 2:
                    ++this.nameCursorPos;
                    if (this.nameCursorPos > 2) {
                        this.nameCursorPos = 2;
                        return;
                    }
                    break;
                case 3:
                    --this.nameCursorPos;
                    if (this.nameCursorPos < 0) {
                        this.nameCursorPos = 0;
                    }
            }

        } else {
            if (this.field_95 != -1) {
                for (int var2 = this.field_95; var2 < this.vector.size(); ++var2) {
                    IGameMenuElement var3;
                    if ((var3 = (IGameMenuElement) this.vector.elementAt(var2)) != null && var3.isNotTextRender()) {
                        var3.menuElemMethod(var1);
                        return;
                    }
                }
            }
        }
    }

    // $FF: renamed from: a (javax.microedition.lcdui.Graphics) void
    protected void render_76(Graphics graphics) {
        int var2;
        int i;
        if (this.field_111) {
            graphics.setColor(0, 0, 20);
            graphics.setFont(this.font);
            byte var7 = 1;
            graphics.drawString("Enter Name", this.xPos, var7, 20);
            var2 = var7 + this.font.getHeight() + (this.field_103 << 2);
            graphics.setFont(this.font2);

            for (i = 0; i < 3; ++i) {
                graphics.drawChar((char) this.strArr[i], this.xPos + i * this.font2.charWidth('W') + 1, var2, 17);
                if (i == this.nameCursorPos) {
                    graphics.drawChar('^', this.xPos + i * this.font2.charWidth('W') + 1, var2 + this.font2.getHeight(), 17);
                }
            }

        } else {
            graphics.setColor(0, 0, 0);
            var2 = this.field_101;
            if (!this.field_94.equals("")) {
                graphics.setFont(this.font);
                graphics.drawString(this.field_94, this.xPos, var2, 20);
                var2 += this.font.getBaselinePosition() + 2;
            }

            if (this.field_105 > 0) {
                this.micro.gameCanvas.drawSprite(graphics, 2, this.xPos - 3, var2);
            }

            if (this.canvasHeight < 100) {
                ++var2;
            } else {
                var2 += 4;
            }

            graphics.setFont(this.font2);

            for (i = this.field_105; i < this.field_106 + 1; ++i) {
                IGameMenuElement var4 = (IGameMenuElement) this.vector.elementAt(i);
                graphics.setColor(0, 0, 0);
                var4.render(graphics, var2, this.field_104);
                if (i == this.field_95 && var4.isNotTextRender()) {
                    int var5 = this.xPos - this.micro.gameCanvas.helmetSpriteWidth / 2;
                    int var6 = var2 + this.font2.getBaselinePosition() / 2 - this.micro.gameCanvas.helmetSpriteHeight / 2;
                    graphics.setClip(var5, var6, this.micro.gameCanvas.helmetSpriteWidth, this.micro.gameCanvas.helmetSpriteHeight);
                    graphics.drawImage(this.micro.gameCanvas.helmetImage, var5 - this.micro.gameCanvas.helmetSpriteWidth * (this.field_110 % 6), var6 - this.micro.gameCanvas.helmetSpriteHeight * (this.field_110 / 6), 20);
                    graphics.setClip(0, 0, this.canvasWidth, this.canvasHeight);
                    ++this.field_110;
                    if (this.field_110 > 30) {
                        this.field_110 = 0;
                    }
                }

                if (var4.isNotTextRender()) {
                    var2 += this.font2.getBaselinePosition() + this.field_103;
                } else {
                    var2 += (TextRender.getBaselinePosition() < GameCanvas.spriteSizeY[5] ? GameCanvas.spriteSizeY[5] : TextRender.getBaselinePosition()) + this.field_103;
                }
            }

            if (this.vector.size() > this.field_106 && this.field_106 != this.vector.size() - 1) {
                if (GameCanvas.spriteSizeY[3] + var2 > this.canvasHeight) {
                    this.micro.gameCanvas.drawSprite(graphics, 3, this.xPos - 3, this.canvasHeight - GameCanvas.spriteSizeY[3]);
                    return;
                }

                this.micro.gameCanvas.drawSprite(graphics, 3, this.xPos - 3, var2 - 2);
            }
        }
    }

    // $FF: renamed from: a (e) void
    public void setGameMenu(GameMenu gameMenu) {
        this.gameMenu = gameMenu;
    }

    // $FF: renamed from: a () e
    public GameMenu getGameMenu() {
        return this.gameMenu;
    }

    // $FF: renamed from: do () int
    public int method_79() {
        return this.field_95;
    }

    // $FF: renamed from: for () void
    public void clearVector() {
        this.vector.setSize(0);
        this.field_105 = 0;
        this.field_106 = 0;
        this.field_95 = -1;
    }

    // $FF: renamed from: byte () java.lang.String
    public String makeString() {
        return new String(this.strArr);
    }

    // $FF: renamed from: if () byte[]
    public byte[] getStrArr() {
        return this.strArr;
    }

    // $FF: renamed from: do (int) void
    public void method_83(int var1) {
        this.method_70();

        while (this.field_95 < var1) {
            ++this.field_95;
            if (this.field_95 > this.field_106) {
                ++this.field_105;
                ++this.field_106;
            }
        }
    }
}
